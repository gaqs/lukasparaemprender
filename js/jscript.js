$(document).ready(function () {
  bsCustomFileInput.init()
});

function editSubmit() {
  var agree = confirm("¿Está seguro que desea guardar loca cambios?");
  if (agree) return true ;
  else return false ;
}



(function() {
  'use strict';
  window.addEventListener('load', function() {
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        }
        form.classList.add('was-validated');
      }, false);
    });
  }, false);
})();

$('textarea').on("propertychange keyup input paste ready",
    function () {
        var limit = $(this).data("limit");
        var remainingChars = limit - $(this).val().length;
        if (remainingChars <= 0) {
            $(this).val($(this).val().substring(0, limit));
        }
        var rest = remainingChars<=0?0:remainingChars;
        var asd = $(this).prev('span').text(rest);
    });

    function checkRut(rut) {
        var valor = rut.value.replace(/\./g,'');
        valor = valor.replace('-','');
        cuerpo = valor.slice(0,-1);
        dv = valor.slice(-1).toUpperCase();
        rut.value = cuerpo + '-'+ dv
        if(cuerpo.length < 7) { return false; }
        suma = 0;
        multiplo = 2;
        for(i=1;i<=cuerpo.length;i++) {
            index = multiplo * valor.charAt(cuerpo.length - i);
            suma = suma + index;
            if(multiplo < 7) { multiplo = multiplo + 1; } else { multiplo = 2; }
        }
        dvEsperado = 11 - (suma % 11);
        dv = (dv == 'K')?10:dv;
        dv = (dv == 0)?11:dv;
        if(dvEsperado != dv) {  return false; }
        rut.setCustomValidity('');
    }
