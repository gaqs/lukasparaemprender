-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 15-08-2019 a las 00:55:09
-- Versión del servidor: 10.4.6-MariaDB
-- Versión de PHP: 7.3.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `lukaspar_lukasparaemprender`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inn_mentor`
--

CREATE TABLE `inn_mentor` (
  `id` int(20) NOT NULL,
  `rut` varchar(100) NOT NULL,
  `nombres` varchar(100) NOT NULL,
  `apellidos` varchar(100) NOT NULL,
  `telefono` varchar(20) NOT NULL,
  `email` varchar(100) NOT NULL,
  `descripcion` text NOT NULL,
  `insert_date` datetime NOT NULL,
  `edit_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `inn_mentor`
--

INSERT INTO `inn_mentor` (`id`, `rut`, `nombres`, `apellidos`, `telefono`, `email`, `descripcion`, `insert_date`, `edit_date`) VALUES
(4, '44444444-4', 'jaqicu', 'Mcbride', '+1 (977) 993-9386', 'pirulusiva@mailinator.com', 'Ad aut repudiandae q', '2019-08-14 12:00:34', '2019-08-14 12:00:34'),
(5, 'Ut enim occaecat rat', 'riqyme', 'Wheeler', '+1 (501) 896-9917', 'borovomis@mailinator.net', 'Corporis enim repudi', '2019-08-14 17:08:45', '2019-08-14 17:08:45'),
(6, 'Excepturi molestiae ', 'xijekahe', 'Byers', '+1 (192) 655-2938', 'sixebax@mailinator.net', 'Accusamus distinctio', '2019-08-14 17:10:18', '2019-08-14 17:10:18');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `inn_mentor`
--
ALTER TABLE `inn_mentor`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `inn_mentor`
--
ALTER TABLE `inn_mentor`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
