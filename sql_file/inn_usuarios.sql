-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 15-08-2019 a las 00:55:20
-- Versión del servidor: 10.4.6-MariaDB
-- Versión de PHP: 7.3.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `lukaspar_lukasparaemprender`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inn_usuarios`
--

CREATE TABLE `inn_usuarios` (
  `id` int(20) NOT NULL,
  `id_grupo` int(20) NOT NULL,
  `administrador` int(2) NOT NULL,
  `rut` varchar(100) NOT NULL,
  `nombres` varchar(100) NOT NULL,
  `apellidos` varchar(100) NOT NULL,
  `direccion` varchar(100) NOT NULL,
  `telefono` varchar(20) NOT NULL,
  `email` varchar(100) NOT NULL,
  `rol` int(2) NOT NULL,
  `descripcion` text NOT NULL,
  `contrasena` varchar(100) NOT NULL,
  `path_alumno` varchar(100) NOT NULL,
  `path_cedula` varchar(100) NOT NULL,
  `insert_date` datetime NOT NULL,
  `edit_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `inn_usuarios`
--

INSERT INTO `inn_usuarios` (`id`, `id_grupo`, `administrador`, `rut`, `nombres`, `apellidos`, `direccion`, `telefono`, `email`, `rol`, `descripcion`, `contrasena`, `path_alumno`, `path_cedula`, `insert_date`, `edit_date`) VALUES
(2, 4, 1, '1111111-1', 'lobodijuju', 'Velez', 'Ut anim facere qui i', '+1 (259) 469-3823', 'gaqs.02@gmail.com', 1, 'Qui dicta incididunt', '3d3321e48e544a4f183ce7bedd181e3e76de0e8f7462509c9aad2d66daaef439', './uploaded_files/1111111-1/alumno_5d543023c507a.jpg', './uploaded_files/1111111-1/cedula_5d543023c5814.jpg', '2019-08-14 12:00:35', '2019-08-14 12:00:35'),
(3, 4, 0, '222222222-2', 'jozicoh', 'Kelley', 'Omnis cumque eum non', '+1 (857) 176-6453', 'nebi@mailinator.net', 2, 'Quia sequi dolores e', 'a25e888598944b398c2a1e4ec11bb8d31bd21dba71b4cb9fde2b23ed5a8c802f', './uploaded_files/222222222-2/alumno_5d543023ce797.jpg', './uploaded_files/222222222-2/cedula_5d543023ceefd.jpg', '2019-08-14 12:00:35', '2019-08-14 12:00:35'),
(4, 4, 0, '33333333-3', 'cotawucene', 'Battle', 'Autem est aut tempor', '+1 (288) 201-8904', 'xige@mailinator.net', 3, 'Id aut voluptate sed', '981fbddd65e37a062485211973607a4d6be3ea21e8928267d3be3a5edff0d115', './uploaded_files/33333333-3/alumno_5d543023def40.jpg', './uploaded_files/33333333-3/cedula_5d543023df7ba.jpg', '2019-08-14 12:00:35', '2019-08-14 12:00:35');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `inn_usuarios`
--
ALTER TABLE `inn_usuarios`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `inn_usuarios`
--
ALTER TABLE `inn_usuarios`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
