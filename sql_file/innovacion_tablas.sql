-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 15-08-2019 a las 00:53:15
-- Versión del servidor: 10.4.6-MariaDB
-- Versión de PHP: 7.3.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `lukaspar_lukasparaemprender`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inn_encuesta`
--

CREATE TABLE `inn_encuesta` (
  `id` int(20) NOT NULL,
  `id_grupo` int(20) NOT NULL,
  `desafio` int(2) NOT NULL,
  `profundizar` text NOT NULL,
  `beneficiarios` text NOT NULL,
  `propuesta_solucion` text NOT NULL,
  `comunidad_implicada` text NOT NULL,
  `prototipo` text NOT NULL,
  `sustentabilidad` text NOT NULL,
  `testear` text NOT NULL,
  `presupuesto` text NOT NULL,
  `path_patrocinio` varchar(100) NOT NULL,
  `path_mentor_academico` varchar(100) NOT NULL,
  `path_reunion` varchar(100) NOT NULL,
  `path_instrumentos` varchar(100) NOT NULL,
  `insert_date` datetime NOT NULL,
  `edit_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inn_mentor`
--

CREATE TABLE `inn_mentor` (
  `id` int(20) NOT NULL,
  `rut` varchar(100) NOT NULL,
  `nombres` varchar(100) NOT NULL,
  `apellidos` varchar(100) NOT NULL,
  `telefono` varchar(20) NOT NULL,
  `email` varchar(100) NOT NULL,
  `descripcion` text NOT NULL,
  `insert_date` datetime NOT NULL,
  `edit_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inn_usuarios`
--

CREATE TABLE `inn_usuarios` (
  `id` int(20) NOT NULL,
  `id_grupo` int(20) NOT NULL,
  `administrador` int(2) NOT NULL,
  `rut` varchar(100) NOT NULL,
  `nombres` varchar(100) NOT NULL,
  `apellidos` varchar(100) NOT NULL,
  `direccion` varchar(100) NOT NULL,
  `telefono` varchar(20) NOT NULL,
  `email` varchar(100) NOT NULL,
  `rol` int(2) NOT NULL,
  `descripcion` text NOT NULL,
  `contrasena` varchar(100) NOT NULL,
  `path_alumno` varchar(100) NOT NULL,
  `path_cedula` varchar(100) NOT NULL,
  `insert_date` datetime NOT NULL,
  `edit_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `inn_encuesta`
--
ALTER TABLE `inn_encuesta`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `inn_mentor`
--
ALTER TABLE `inn_mentor`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `inn_usuarios`
--
ALTER TABLE `inn_usuarios`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `inn_encuesta`
--
ALTER TABLE `inn_encuesta`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `inn_mentor`
--
ALTER TABLE `inn_mentor`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `inn_usuarios`
--
ALTER TABLE `inn_usuarios`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
