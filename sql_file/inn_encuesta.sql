-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 15-08-2019 a las 00:54:58
-- Versión del servidor: 10.4.6-MariaDB
-- Versión de PHP: 7.3.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `lukaspar_lukasparaemprender`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inn_encuesta`
--

CREATE TABLE `inn_encuesta` (
  `id` int(20) NOT NULL,
  `id_grupo` int(20) NOT NULL,
  `desafio` int(2) NOT NULL,
  `profundizar` text NOT NULL,
  `beneficiarios` text NOT NULL,
  `propuesta_solucion` text NOT NULL,
  `comunidad_implicada` text NOT NULL,
  `prototipo` text NOT NULL,
  `sustentabilidad` text NOT NULL,
  `testear` text NOT NULL,
  `presupuesto` text NOT NULL,
  `path_patrocinio` varchar(100) NOT NULL,
  `path_mentor_academico` varchar(100) NOT NULL,
  `path_reunion` varchar(100) NOT NULL,
  `path_instrumentos` varchar(100) NOT NULL,
  `insert_date` datetime NOT NULL,
  `edit_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `inn_encuesta`
--

INSERT INTO `inn_encuesta` (`id`, `id_grupo`, `desafio`, `profundizar`, `beneficiarios`, `propuesta_solucion`, `comunidad_implicada`, `prototipo`, `sustentabilidad`, `testear`, `presupuesto`, `path_patrocinio`, `path_mentor_academico`, `path_reunion`, `path_instrumentos`, `insert_date`, `edit_date`) VALUES
(2, 4, 0, 'Mollit vel aut eum i', 'Sint veniam est in', 'Accusantium mollit e', 'Ut odit tempore aut', 'Omnis voluptas cupid', 'Temporibus id accusa', 'Laudantium exercita', '', './uploaded_files/44444444-4/patrocinio_5d543023b6140.jpg', './uploaded_files/44444444-4/mentor_academico_5d543023b6af7.jpg', './uploaded_files/44444444-4/reunion_5d543023b718f.jpg', './uploaded_files/44444444-4/instrumentos_5d543023b797d.jpg', '2019-08-14 12:00:35', '2019-08-14 12:00:35'),
(3, 5, 0, 'Recusandae Quae in ', 'Pariatur Dolores ip', 'Atque et est ipsam a', 'Repellendus Id cum ', 'Sint quo fugit qui ', 'Commodo qui in asper', 'Suscipit id ipsum ', '', './uploaded_files/Ut enim occaecat rat/patrocinio_5d54785d641e8.', './uploaded_files/Ut enim occaecat rat/mentor_academico_5d54785d64230.', './uploaded_files/Ut enim occaecat rat/reunion_5d54785d64237.', './uploaded_files/Ut enim occaecat rat/instrumentos_5d54785d6423b.', '2019-08-14 17:08:45', '2019-08-14 17:08:45'),
(4, 6, 0, 'Voluptas at voluptat', 'Dolor nulla et fugia', 'Iusto dolores corrup', 'Alias ut voluptas co', 'Et eligendi incidunt', 'Et magnam aut ut quo', 'Earum ipsum id iusto', '', './uploaded_files/Excepturi molestiae /patrocinio_5d5478ba56766.', './uploaded_files/Excepturi molestiae /mentor_academico_5d5478ba5676e.', './uploaded_files/Excepturi molestiae /reunion_5d5478ba56771.', './uploaded_files/Excepturi molestiae /instrumentos_5d5478ba56773.', '2019-08-14 17:10:18', '2019-08-14 17:10:18');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `inn_encuesta`
--
ALTER TABLE `inn_encuesta`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `inn_encuesta`
--
ALTER TABLE `inn_encuesta`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
