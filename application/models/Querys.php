<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Querys extends CI_Model {

	public function addUser($user_rut,$user_name,$user_surname,$user_job,$user_address,$user_phone,$user_email,$user_pass){
    $query = $this->db->query("INSERT INTO usuario (rut,nombres,appellidos,ocupacion,direccion,telefono,email,contrasena)
                              VALUES ('$user_rut','$user_name','$user_surname','$user_job','$user_address',$user_phone,'$user_email','$user_pass') ");
		if( $query ){
			$result = 'exito';
		    return $result;
		}
		else{
		    return $this->db->error();
		}
  }

  public function addBussiness(){
    $query = $this->db->query("INSERT INTO TELEFONO02 (FONO_TELEFONO02, ID_TELECENTRO) VALUES ('$phone', $id)");
    		if( $query ){
    			$result = 'exito';
    		    return $result;
    		}
    		else{
    		    return $this->db->error();
    		}
  }

	public function selectInfoUser(){

			$query = $this->db->query("SELECT * FROM usuarios WHERE flag = 1");

			if( $query ){
				$result = $query->result_array();
			    return $result;
			}
			else{
			    return $this->db->error();
			}

		}
		public function selectEspecificUser($id){

			$query = $this->db->query("SELECT tipo_usuario FROM usuarios WHERE id = $id ");

			if( $query ){
				$result = $query->result_array();
			    return $result;
			}
			else{
			    return $this->db->error();
			}

		}

	public function checkAdmin($email,$pass){
			$query = $this->db->query("SELECT * FROM administradores WHERE correo = '$email' AND contrasena = '$pass' ");

			if( $query ){
				$result = $query->result_array();
			    return $result;
			}
			else{
			    return $this->db->error();
			}
		}
	public function selectUserById($id){
		$query = $this->db->query("SELECT * FROM usuarios u, encuesta_emprendimiento e WHERE u.id = $id AND u.id = e.id_usuario");

		if( $query ){
			$result = $query->result_array();
				return $result;
		}
		else{
				return $this->db->error();
		}
	}

	/*-------------Innovacion ---------------*/

	public function selectAllInnUser(){

			$query = $this->db->query("SELECT * FROM inn_usuarios WHERE administrador = 1");

			if( $query ){
				$result = $query->result_array();
			    return $result;
			}
			else{
			    return $this->db->error();
			}

		}

	public function selectUserByGroup($id){
		$query = $this->db->query("SELECT * FROM inn_usuarios WHERE id_grupo = '$id' ");

		if( $query ){
			$result = $query->result_array();
				return $result;
		}
		else{
				return $this->db->error();
		}
	}

	public function selectUserByGroupAndRol($id, $rol){
		$query = $this->db->query("SELECT * FROM inn_usuarios WHERE id_grupo = '$id' AND rol = $rol");

		if( $query ){
			$result = $query->result_array();
				return $result;
		}
		else{
				return $this->db->error();
		}

	}

	public function selectAllByGroup($id){
		$query = $this->db->query("SELECT * FROM inn_mentor m, inn_encuesta e WHERE m.id_mentor = e.id_grupo AND m.id_mentor = '$id' ");

		if( $query ){
			$result = $query->result_array();
				return $result;
		}
		else{
				return $this->db->error();
		}
	}
	/* ----------------------------------------- */

	public function checkUser($email,$pass){
		$query = $this->db->query("SELECT * FROM usuarios WHERE email = '$email' AND contrasena = '$pass' AND flag = 1");

		if( $query ){
			$result = $query->result_array();
				return $result;
		}
		else{
				return $this->db->error();
		}
	}
	public function checkInnUser($email,$pass){
		$query = $this->db->query("SELECT * FROM inn_usuarios WHERE email = '$email' AND contrasena = '$pass' AND administrador = 1");

		if( $query ){
			$result = $query->result_array();
				return $result;
		}
		else{
				return $this->db->error();
		}
	}

	public function selectUserByIdEmpresa($id){
		$query = $this->db->query("SELECT * FROM usuarios u, encuesta_empresa e, empresa em  WHERE u.id = $id AND e.id_usuario = $id AND em.id_usuario = $id");

		if( $query ){
			$result = $query->result_array();
				return $result;
		}
		else{
				return $this->db->error();
		}


	}

	public function selectFileVal_Emprendimiento($id){
		$query = $this->db->query("SELECT path_cedula, val_path_cedula, path_residencia, val_path_residencia FROM encuesta_emprendimiento WHERE id_usuario = $id");

		if( $query ){
			$result = $query->result_array();
				return $result;
		}
		else{
				return $this->db->error();
		}
	}

	public function selectAllUsers(){
		$query = $this->db->query("SELECT * FROM usuarios u, encuesta_emprendimiento e WHERE u.id = e.id_usuario AND u.tipo_usuario = '0' AND u.flag = 1");

		if( $query ){
			$result = $query->result_array();
				return $result;
		}
		else{
				return $this->db->error();
		}
	}

	public function selectAllEmpresa(){
		$query = $this->db->query("SELECT * FROM usuarios u, encuesta_empresa e WHERE u.id = e.id_usuario AND u.tipo_usuario = '1' AND u.flag = 1");

		if( $query ){
			$result = $query->result_array();
				return $result;
		}
		else{
				return $this->db->error();
		}
	}

	public function selectAllNotas(){
		$query = $this->db->query("SELECT * FROM notas_encuesta");

		if( $query ){
			$result = $query->result_array();
				return $result;
		}
		else{
				return $this->db->error();
		}
	}

	public function validateAndSelectUser($hash){
		$query = $this->db->query("SELECT u.id, u.rut, u.tipo_usuario, h.hash_email, h.date_email, h.validado FROM usuarios u, hash_validation h WHERE u.id = h.id_user AND u.flag = 1 AND h.hash_email = '$hash' ");

		if( $query ){
			$result = $query->result_array();
				return $result;
		}
		else{
				return $this->db->error();
		}

	}

	public function selectUserIdByHash($old_pass, $pass_hash){
		$query = $this->db->query("SELECT id_user FROM hash_validation where pass = '$old_pass' AND hash_pass = '$pass_hash'");

		if( $query ){
			$result = $query->result_array();
			return $result;
		}else{
			return $this->db->error();
		}
	}

} //end class Query
