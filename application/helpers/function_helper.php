<?php
defined('BASEPATH') OR exit('No direct script access allowed');


function hash_generate($chars){
  $data = '1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ`-=~!@#$%^&*()_+,./<>?;:[]{}\|';
  return substr(str_shuffle($data), 0, $chars);
}

function pass_generate($chars){
  $data = '1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ';
  return substr(str_shuffle($data), 0, $chars);
}

function encrypted($string){
  $a = md5($string);
  $b = hash('sha256',$a);

  return $b;
}

function reArrayFiles($file_post) {
    $file_ary = array();
    $file_count = count($file_post['name']);
    $file_keys = array_keys($file_post);

    for ($i=0; $i<$file_count; $i++) {
        foreach ($file_keys as $key) {
            $file_ary[$i][$key] = $file_post[$key][$i];
        }
    }
    return $file_ary;
}

function fileManager($file_path, $type, $file_array){
  $tmp_name = $file_array['tmp_name'];
  $name     = $file_array['name'];
  $ext      = pathinfo($name, PATHINFO_EXTENSION);
  $path     = $file_path . $type.'_' . uniqid() .'.'.$ext;
  move_uploaded_file($tmp_name, $path);
  return $path;
}

function EditfileManager($file_path, $type, $file_array){
    error_reporting(E_ERROR | E_PARSE);
    $file = explode('/', $file_path);
    $tmp_name = $file_array['tmp_name'];
    $name     = $file_array['name'];
    $ext      = pathinfo($name, PATHINFO_EXTENSION);
    $path     = './'.$file[1].'/'.$file[2].'/'.$type.'_'. uniqid() .'.'.$ext;
    move_uploaded_file($tmp_name, $path);
    //cambiar valor en base de datos de path!
    return $path;
}

function delete_directory($dirname) {
         if (is_dir($dirname))
           $dir_handle = opendir($dirname);
     if (!$dir_handle)
          return false;
     while($file = readdir($dir_handle)) {
           if ($file != "." && $file != "..") {
                if (!is_dir($dirname."/".$file))
                     unlink($dirname."/".$file);
                else
                     delete_directory($dirname.'/'.$file);
           }
     }
     closedir($dir_handle);
     rmdir($dirname);
     return true;
}

/* *****************************************************************************
** FUNCIÓN PHP QUE PERMITE VALIDAR EL RUT CHILENO
** EXPLICADO PASO A PASO
** PUEDEN EDITARLO Y MEJORARLO
** ************************************************************************** */
/**
 * valida el digito verificador del rut
 * @param  rut en formato => 12.345.678-9 ó
 * @param  rut en formato => 12345678-9
 * @return  boolean
 */
function validRut($rut)
{
    // pasamos el rut ingresado a mayuscula y quitamos los ceros si los tiene
    $rut = str_replace('.', '', strtoupper($rut));
    // valida el numero de caracteres
    if (strlen($rut) < 9 || strlen($rut) > 10) {
        return false;
    }else {
        // valida si ingreso el gion
        if (substr($rut, -2, 1) != '-') {
            return false;
        }else {
            // valida si ha ingresado numeros
            if (!ctype_digit(substr($rut, 0, strlen($rut)-2))) {
                return false;
            }else {
                $k          = [2, 3, 4, 5, 6, 7, 2, 3];         // constantes
                $dv         = substr($rut, -1);                 // digito verificador
                $rut        = substr($rut, 0, strlen($rut)-2);  // rut sin digito verificador
                $rut_array  = array_reverse(str_split($rut));   // pasamos el rut a un array e invertimos el orden
                // array para almacenar el resultado de la multiplicacion
                // de las constantes por los digitos del rut
                $result     = [];
                // multiplicamos los valores del array rut y lo multiplicamos por las constantes y
                // almacenamos el valor de cada multiplicacion en el array $result
                for ($j=0; $j < count($rut_array); $j++) {
                    $result[]   = $rut_array[$j] * $k[$j];
                }
                $suma           = array_sum($result);   // sumamos los valores del array $result
                $resto          = $suma % 11;           // Obtenemos el resto de la suma al dividirlo por 11 (modulo 11)
                $verificador    = 11 - $resto;
                // El Dígito Verificador es:
                // igual al resultado si éste está entre 1 y 9
                // K si el resultado es 10 y
                // 0 si el resultado es 11.
                switch ($verificador) {
                    case 11:
                        $verificador = 0;
                        break;
                    case 10:
                        $verificador = 'K';
                        break;
                    default:
                        $verificador = $verificador;
                        break;
                }
                return ($verificador == $dv) ? true : false ;
            }
        }
    }
}

function promedio_notas($notas){

  $sum = 0;
  $count = 0;
  for ($i=0; $i < count($notas); $i++) {
      $nota =  $notas[$i]->nota;
      $count ++;
      $sum = $sum + $nota;
  }

  if( $count != 0){
    return round($sum/$count, 1);
  }else{
    return 0;
  }



}


function separate_per_id($notas,$id){
  $users = array();
  for ($i=0; $i < count($notas); $i++) {
    if( $notas[$i]['id_usuario'] == $id ){
      array_push($users, $notas[$i]);
    }
  }

  $count = 0;
  $order = array();
  $prom = 0;
  $sum = 0;
  foreach ($users as $key => $value) {
      $preg = $value['pregunta'];
      $crit = $value['criterio'];
      $nota = $value['nota'];
      if( !array_key_exists($preg, $order)){
        $order[$preg] = array();
      }
      if( !array_key_exists($crit, $order[$preg] )){
        $order[$preg][$crit] = $nota;
        $sum += $nota;
      }
      $count++;
  }

  if( $count != 0 ){
    $prom = round($sum/$count, 1);
  }

  if( !array_key_exists('promedio', $order)){
    $order['promedio'] = $prom;
  }

  return $order;
}


function reorder_array_changekey($notas){
  $users = array();

  $count = 0;
  $order = array();
  $prom = 0;
  $sum = 0;

  foreach ($notas as $key) {
      $id   = $key['id_usuario'];
      $preg = $key['pregunta'];
      $crit = $key['criterio'];
      $nota = $key['nota'];

      if( !array_key_exists($id, $order)){
        $order[$id] = array();
      }

      if( !array_key_exists($preg, $order[$id])){
        $order[$id][$preg] = array();
      }

      if( !array_key_exists($crit, $order[$id][$preg] )){
        $order[$id][$preg][$crit] = $nota;
        $sum += $nota;
        $count++;
      }
  }

  return $order;

}


// $user[pregunta][criterio][nota]
