<h3>Formulario de Postulación CATEGORIA y/o LÍNEA  EMPRENDIMIENTO</h3>
<br><br>
Concurso “Lukas para Emprender 2018”
<br>
La información que se proporcione en el formulario se administrará bajo el concepto de absoluta confidencialidad y será revisado sólo por el equipo autorizado de trabajo de la Ilustre Municipalidad de Puerto Montt  y la Universidad de Los Lagos. Se garantiza absoluta reserva y discreción de la información proporcionada.

<br><br>

<table border="0" cellpadding="5">
  <tbody>
  <tr>
    <td width="200"><b>1.- Nombre del Proyecto</b></td>
    <td><?= $user[0]['nombre'] ?></td>
  </tr>
</tbody>
</table>

<br>
<br>

<table border="0" cellpadding="5">
   <tbody>
     <tr>
       <td colspan="1"><b>2.- Información de contacto del postulante.</b></td>
     </tr>
      <tr>
         <td width="200">Nombre y Apellidos</td>
         <td><?= $user[0]['nombres'].' '.$user[0]['apellidos'] ?></td>
      </tr>
      <tr>
         <td>RUT</td>
         <td><?= $user[0]['rut'] ?></td>
      </tr>
      <tr>
         <td>Profesión/Ocupación</td>
         <td><?= $user[0]['ocupacion'] ?></td>
      </tr>
      <tr>
         <td>Dirección</td>
         <td><?= $user[0]['direccion'] ?></td>
      </tr>
      <tr>
         <td>Teléfono móvil</td>
         <td><?= $user[0]['telefono'] ?></td>
      </tr>
      <tr>
         <td>Emails</td>
         <td><?= $user[0]['email'] ?></td>
      </tr>
   </tbody>
</table>

<br>
<br>

<table border="0" cellpadding="5">
   <tbody>
      <tr>
         <td colspan="1"><b>3.- Explique de forma breve su negocio y/o emprendimiento.</b></td>
      </tr>
      <tr>
         <td><?= $user[0]['resumen'] ?></td>
      </tr>
      <tr>
         <td colspan="1"><b>4.- Si cuenta con un equipo emprendedor, haga una breve reseña de ellos.</b></td>
      </tr>
      <tr>
         <td><?= $user[0]['equipo'] ?></td>
      </tr>
      <tr>
         <td colspan="1"><b>5.- ¿Qué hace su negocio y/o emprendimiento sea relevante o diferente de otros existentes?</b></td>
       </tr>
       <tr>
         <td><?= $user[0]['relevancia'] ?></td>
      </tr>
      <tr>
         <td colspan="1"><b>6.- ¿Qué hará con el financiamiento adjudicado de este Concurso?</b></td>
       </tr>
       <tr>
         <td><?= $user[0]['financiamiento'] ?></td>
      </tr>
      <tr>
         <td colspan="1"><b>7.- ¿Quiénes son sus potenciales clientes? breve descripción.</b></td>
       </tr>
       <tr>
         <td><?= $user[0]['clientes'] ?></td>
      </tr>
      <tr>
         <td colspan="1"><b>8.- ¿Cuál es el estado actual de su producto o servicio? Comente.</b></td>
       </tr>
       <tr>
         <td><?= $user[0]['estado'] ?></td>
      </tr>
      <tr>
         <td colspan="1"><b>9.- INFORMACIÓN COMPLEMENTARIA Y OTROS ASPECTOS QUE QUIERA USTED DESTACAR (Ejemplo premios obtenidos, ubicación de su negocio, años de experiencia, fotografías, certificados, etc.)</b></td>
       </tr>
       <tr>
         <td><?= $user[0]['complemento'] ?></td>
      </tr>
   </tbody>
</table>
