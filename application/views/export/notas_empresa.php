<?php  error_reporting( error_reporting() & ~E_NOTICE ); ?>
<div class="container-flex admin_container">

<a href="javascript:history.back()" class="btn btn-secondary"><i class="fas fa-arrow-circle-left"></i> Volver</a>
<button id="export_excel" class="btn btn-success"><i class="fas fa-file-excel"></i> Exportar tabla a Excel</button>
<br><br>
<div class="container_table_notas">

<table border="1" cellpadding="5" cellspacing="0" id="table_notas">
<thead>
  <tr>
    <td width="50"></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td colspan="3">1.- Explique de forma breve a qué se dedica su empresa y cuál es su idea para ella</td>
    <td colspan="3">2.- Si cuenta con un equipo emprendedor, haga una breve reseña de ellos.</td>
    <td colspan="3">3.- ¿Qué hará con el financiamiento adjudicado de este Concurso?</td>
    <td colspan="2">4.- ¿Quiénes son sus clientes? breve descripción</td>
    <td colspan="2">5.- ¿Cuenta con un sistema de comercialización?</td>
    <td colspan="2">6.- ¿Quién o quiénes son su competencia?</td>
    <td colspan="2">7.- ¿Cuáles son sus proyecciones de ventas esperadas para el próximo año (2020) en caso de serganador?</td>
    <td colspan="2">8.- ¿Generará nuevos puestos de trabajo en caso de obtener el financiamiento?</td>
    <td>9.- INFORMACIÓN COMPLEMENTARIA Y OTROS ASPECTOS QUE QUIERA USTED DESTACAR</td>
    <td></td>
  </tr>
  <tr>
    <td width="50">ID</td>
    <td>Nombre usuario</td>
    <td>Emprendimiento<br></td>
    <td>Des?</td>
    <td>Causa</td>
    <td>¿Describe claramente su propuesta de negocio?</td>
    <td>¿Define la oportunidad de negocio? (define problema ysolución)</td>
    <td>¿Describe claramente cuál es el producto o servicio queofrecerá?</td>
    <td>¿Posee un equipo (personas) emprendedor? </td>
    <td>¿El equipo emprendedor tiene experiencia en emprendimientos?</td>
    <td>¿Describe funciones y/o roles del equipo?</td>
    <td>¿Identifica los costos e inversiones a realizar?</td>
    <td>Entrega un presupuesto detallado de lo que adquirirácon el financiamiento.</td>
    <td>Razones de la adjudicación de lo solicitado para suemprendimiento (a lo menos tres).</td>
    <td>¿Describe los clientes? ¿Quiénes son? ¿Dónde están?</td>
    <td>¿Aporta con datos cuantitativos (valores numéricos aproximados) respecto a su mercado (entregar detalle)? </td>
    <td>Si o No (argumente respuesta)</td>
    <td>¿Cuáles son los canales de comercialización? Mencionaa los menos tres. </td>
    <td>¿Describe con claridad quienes son su competencia a nivel local, regional o nacional (según corresponda)?</td>
    <td>¿Identifica ventajas y desventajas sobre su competencia? ( a lo menos dos por cada una) </td>
    <td>¿Señala valores ($) estimados y / o aproximados deventas a futuro? ( proyección )</td>
    <td>Entrega estado actual de ventas ( anuales y/o mensuales) </td>
    <td>¿Cuántos puestos de trabajo generaría? (entregue un dato aproximado )</td>
    <td>¿Qué funciones desempeñarían en su empresa los nuevos puestos de trabajo generados?</td>
    <td>¿Entrega información complementaria relevante al proyecto? (fotografías, certificados, capacitaciones, etc.)</td>
    <td>Promedio</td>
  </tr>
</thead>
<tbody>
    <?php

      $asd = reorder_array_changekey($notas);

      foreach ($user as $key) {
        $id = $key['id_usuario'];         

         echo    '<tr>
                  <td width="50">'.$id.'</td>
                  <td>'.$key['nombres'].' '.$key['apellidos'].'</td>
                  <td>'.$key['nombre'].'</td>';

        if( $key['descalificado'] == 1 ){
              echo '<td>X</td>';
            }else{
              echo '<td></td>';
            }

            echo '<td>'.$key['descalificado_causa'].'</td>';
         

        $user = separate_per_id($notas,$id);

        echo '<td align="center" id="'.$id.'_p1_c1">'; if(array_key_exists(1, $user)){ echo $user[1][1]; }else{ echo 0; } echo '</td>
              <td align="center" id="'.$id.'_p1_c2">'; if(array_key_exists(1, $user)){ echo $user[1][2]; }else{ echo 0; } echo '</td>
              <td align="center" id="'.$id.'_p1_c3">'; if(array_key_exists(1, $user)){ echo $user[1][3]; }else{ echo 0; } echo '</td>
              <td align="center" id="'.$id.'_p2_c1">'; if(array_key_exists(2, $user)){ echo $user[2][1]; }else{ echo 0; } echo '</td>
              <td align="center" id="'.$id.'_p2_c2">'; if(array_key_exists(2, $user)){ echo $user[2][2]; }else{ echo 0; } echo '</td>
              <td align="center" id="'.$id.'_p2_c3">'; if(array_key_exists(2, $user)){ echo $user[2][3]; }else{ echo 0; } echo '</td>
              <td align="center" id="'.$id.'_p3_c1">'; if(array_key_exists(3, $user)){ echo $user[3][1]; }else{ echo 0; } echo '</td>
              <td align="center" id="'.$id.'_p3_c2">'; if(array_key_exists(3, $user)){ echo $user[3][2]; }else{ echo 0; } echo '</td>
              <td align="center" id="'.$id.'_p3_c3">'; if(array_key_exists(3, $user)){ echo $user[3][3]; }else{ echo 0; } echo '</td>
              <td align="center" id="'.$id.'_p4_c1">'; if(array_key_exists(4, $user)){ echo $user[4][1]; }else{ echo 0; } echo '</td>
              <td align="center" id="'.$id.'_p4_c2">'; if(array_key_exists(4, $user)){ echo $user[4][2]; }else{ echo 0; } echo '</td>
              <td align="center" id="'.$id.'_p5_c1">'; if(array_key_exists(5, $user)){ echo $user[5][1]; }else{ echo 0; } echo '</td>
              <td align="center" id="'.$id.'_p5_c2">'; if(array_key_exists(5, $user)){ echo $user[5][2]; }else{ echo 0; } echo '</td>
              <td align="center" id="'.$id.'_p6_c1">'; if(array_key_exists(6, $user)){ echo $user[6][1]; }else{ echo 0; } echo '</td>
              <td align="center" id="'.$id.'_p6_c2">'; if(array_key_exists(6, $user)){ echo $user[6][2]; }else{ echo 0; } echo '</td>
              <td align="center" id="'.$id.'_p7_c1">'; if(array_key_exists(7, $user)){ echo $user[7][1]; }else{ echo 0; } echo '</td>
              <td align="center" id="'.$id.'_p7_c2">'; if(array_key_exists(7, $user)){ echo $user[7][2]; }else{ echo 0; } echo '</td>
              <td align="center" id="'.$id.'_p8_c1">'; if(array_key_exists(8, $user)){ echo $user[8][1]; }else{ echo 0; } echo '</td>
              <td align="center" id="'.$id.'_p8_c2">'; if(array_key_exists(8, $user)){ echo $user[8][2]; }else{ echo 0; } echo '</td>
              <td align="center" id="'.$id.'_p9_c1">'; if(array_key_exists(9, $user)){ echo $user[9][1]; }else{ echo 0; } echo '</td>
              <td align="center" id="promedio">'; echo $user['promedio']; echo '</td>';
         echo '</tr>';
                  
       } 
    ?>
  </tbody>
</table>

</div>
</div>
<script type="text/javascript">
  $(document).ready(function() {

    $('#export_excel').on('click', function(e){
        e.preventDefault();
        ResultsToTable();
    });
    
    function ResultsToTable(){    
        var table = decodeURIComponent("#table_notas");
        $(table).table2excel({
            exclude: ".noExl",
            name: "Empresa",
            filename: "notas_empresa"
        });
    }

    
});
</script>