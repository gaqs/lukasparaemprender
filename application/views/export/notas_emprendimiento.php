<?php  error_reporting( error_reporting() & ~E_NOTICE ); ?>
<div class="container-flex admin_container">

<a href="javascript:history.back()" class="btn btn-secondary"><i class="fas fa-arrow-circle-left"></i> Volver</a>
<button id="export_excel" class="btn btn-success"><i class="fas fa-file-excel"></i> Exportar tabla a Excel</button>
<br><br>
<div class="container_table_notas">



  <table border="1" cellpadding="5" cellspacing="0" id="table_notas">
    <thead>
      <tr>
        <td width="50"></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td colspan="3">1.- Explique de forma breve su negocio y/o emprendimiento.</td>
        <td colspan="3">2.- Si cuenta con un equipo emprendedor, haga una breve reseña de ellos.</td>
        <td>3.- ¿Qué hace su negocio y/o emprendimiento sea relevante o diferente de otros existentes?</td>
        <td colspan="3">4.- ¿Qué hará con el financiamiento adjudicado de este Concurso?</td>
        <td colspan="3">5.- ¿Quiénes son sus potenciales clientes? breve descripción.</td>
        <td>6.- ¿Cuál es el estado actual de su producto o servicio? Comente.</td>
        <td>7.- INFORMACIÓN COMPLEMENTARIA</td>
        <td></td>
      </tr>
      <tr>
        <td width="50">ID</td>
        <td>Nombre usuario</td>
        <td>Emprendimiento<br></td>
        <td>Des?</td>
        <td>Causa</td>
        <td>¿Describe claramente su propuesta de negocio?</td>
        <td>Define la oportunidad de negocio? (define problema y solución)</td>
        <td>¿Describe claramente cuál es el producto o servicio queofrecerá?</td>
        <td>¿Posee un equipo( personas) emprendedor? (datonumérico) Argumentar</td>
        <td>¿El equipo emprendedor tiene experiencia en emprendimientos? ( si o no , fundamentando respuesta)</td>
        <td>¿Describe funciones y/o roles del equipo emprendedor?</td>
        <td>¿Describe los rasgos diferenciadores de su producto o servicio? Mencione a los menos tres.</td>
        <td>¿Identifica los costos e inversiones a realizar parala puesta en marcha?</td>
        <td>Entrega un presupuesto detallado (no cotización)de lo que adquirirá con el financiamientosolicitado.</td>
        <td>Razones de la adjudicación de lo solicitado para suemprendimiento ( a lo menos tres )</td>
        <td>¿Describe cualitativamente a los clientes? ¿Quiénes son? ¿Dónde están?</td>
        <td>¿Identifica los canales de comercialización? ¿Cómo pretende llegar y vender a sus clientes?</td>
        <td>¿Aporta con datos cuantitativos (valores numéricos aproximados) respecto a su mercado (entregar detalle)?</td>
        <td>¿Menciona el estado (inicio, desarrollo,consolidación, declive, etc.) de su producto oservicio? </td>
        <td>¿Entrega información complementaria relevante al proyecto? (fotografías, certificados, capacitaciones, ubicación, etc.)</td>
        <td>Promedio</td>
      </tr>
    </thead>
    <tbody>
        <?php

          $asd = reorder_array_changekey($notas);

          foreach ($user as $key) {
            $id = $key['id_usuario'];
            echo    '<tr>
                      <td width="50">'.$id.'</td>
                      <td>'.$key['nombres'].' '.$key['apellidos'].'</td>
                      <td>'.$key['nombre'].'</td>';

            if( $key['descalificado'] == 1 ){
              echo '<td>X</td>';
            }else{
              echo '<td></td>';
            }

            echo '<td>'.$key['descalificado_causa'].'</td>';
                      

            $user = separate_per_id($notas,$id);

            echo '<td align="center" id="'.$id.'_p1_c1">'; if(array_key_exists(1, $user)){ echo $user[1][1]; }else{ echo 0; } echo '</td>
                  <td align="center" id="'.$id.'_p1_c2">'; if(array_key_exists(1, $user)){ echo $user[1][2]; }else{ echo 0; } echo '</td>
                  <td align="center" id="'.$id.'_p1_c3">'; if(array_key_exists(1, $user)){ echo $user[1][3]; }else{ echo 0; } echo '</td>
                  <td align="center" id="'.$id.'_p2_c1">'; if(array_key_exists(2, $user)){ echo $user[2][1]; }else{ echo 0; } echo '</td>
                  <td align="center" id="'.$id.'_p2_c2">'; if(array_key_exists(2, $user)){ echo $user[2][2]; }else{ echo 0; } echo '</td>
                  <td align="center" id="'.$id.'_p2_c3">'; if(array_key_exists(2, $user)){ echo $user[2][3]; }else{ echo 0; } echo '</td>
                  <td align="center" id="'.$id.'_p3_c1">'; if(array_key_exists(3, $user)){ echo $user[3][1]; }else{ echo 0; } echo '</td>
                  <td align="center" id="'.$id.'_p4_c1">'; if(array_key_exists(4, $user)){ echo $user[4][1]; }else{ echo 0; } echo '</td>
                  <td align="center" id="'.$id.'_p4_c2">'; if(array_key_exists(4, $user)){ echo $user[4][2]; }else{ echo 0; } echo '</td>
                  <td align="center" id="'.$id.'_p4_c3">'; if(array_key_exists(4, $user)){ echo $user[4][3]; }else{ echo 0; } echo '</td>
                  <td align="center" id="'.$id.'_p5_c1">'; if(array_key_exists(5, $user)){ echo $user[5][1]; }else{ echo 0; } echo '</td>
                  <td align="center" id="'.$id.'_p5_c2">'; if(array_key_exists(5, $user)){ echo $user[5][2]; }else{ echo 0; } echo '</td>
                  <td align="center" id="'.$id.'_p5_c3">'; if(array_key_exists(5, $user)){ echo $user[5][3]; }else{ echo 0; } echo '</td>
                  <td align="center" id="'.$id.'_p6_c1">'; if(array_key_exists(6, $user)){ echo $user[6][1]; }else{ echo 0; } echo '</td>
                  <td align="center" id="'.$id.'_p7_c1">'; if(array_key_exists(7, $user)){ echo $user[7][1]; }else{ echo 0; } echo '</td>
                  <td align="center" id="promedio">'; echo $user['promedio']; echo '</td>';
             echo '</tr>';
                      
           } 
        ?>
       </tbody>
    </table>




</div>
</div>
<script type="text/javascript">
  $(document).ready(function() {

    $('#export_excel').on('click', function(e){
        e.preventDefault();
        ResultsToTable();
    });
    
    function ResultsToTable(){    
        var table = decodeURIComponent("#table_notas");
        $(table).table2excel({
            exclude: ".noExl",
            name: "Emprendimiento",
            filename: "notas_emprendimiento"
        });
    }

    
});
</script>