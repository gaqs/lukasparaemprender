<script type="text/javascript">
	$('.carousel').carousel({
	  interval: false,
	  keyboard: false
	})
</script>
<?php $session=$this->session->userdata('user_name'); $disabled = ''; if( isset($session) ){ $disabled = 'disabled'; } ?>
<div class="back"></div> <!-- background slider y menu header -->
<div class="aviso text-center">Página en periodo de<br>prueba y evaluación</div>


<div class="container-fluid p-0 mt-5">

	<div id="slider_header" class="carousel slide" data-ride="carousel">
		<ol class="carousel-indicators">
			<li data-target="#slider_header" data-slide-to="0" class="active"></li>
			<li data-target="#slider_header" data-slide-to="1"></li>
			<li data-target="#slider_header" data-slide-to="2"></li>
		</ol>
		<div class="carousel-inner">
			<div class="carousel-item active">
				<div class="d-block w-100 slider_item" alt="...">
					<div class="row mx-auto">

							<div class="col-md-4 d-flex justify-content-end">
								<img src="<?= base_url();?>image/lukitas_border.png" class="w-100 logo">
							</div>
							<div class="col-md-8 d-inline align-self-center"> <span class="line_1">9° CONCURSO</span>
								<br> <span class="line_2">Luka$ para Emprender</span>
								<br> <span class="line_3">2019</span>
								<hr class="line_separator"><br>
								<span class="line_4">Subdirección de Desarrollo Económico Local</span>
								<br> <span class="line_4">Dirección de Desarrollo Comunitario</span>
							</div>

					</div>
				</div>
			</div>

			<div class="carousel-item">
				<div class="d-block w-100 slider_item" alt="...">
					<div class="row mx-auto seleccion" style="display: inherit;text-align: center;padding-top: 150px;">
						<h3 class="text-white"><b>11 de Junio del 2019</b></h3>
						<h1>RESULTADOS PRE SELECCIÓN!</h1>

						<a href="<?= base_url(); ?>files/resultados/Preseleccionados_LukasparaEmprender_2019.xlsx" target="_blank" class="btn btn-success btn-lg p_none"><i class="fas fa-file-excel"></i> VER RESULTADOS PRESELECCIÓN</a>
						<br>
						<small class="text-light">(*) Resultados disponibles el 11 de Junio del 2019</small>
					</div>
				</div>
			</div>

			<div class="carousel-item">
				<div class="d-block w-100 slider_item" alt="...">
					<div class="row mx-auto seleccion" style="display: inherit;text-align: center;padding-top: 150px;">
						<h3 class="text-white"><b>31 de Julio del 2019</b></h3>
						<h1>GANADORES DEL CONCURSO!</h1>

						<a href="<?= base_url(); ?>files/resultados/Ganadores_Categoria_Emprendimiento_Empresa_lukas_2019.pdf" target="_blank" class="btn btn-primary btn-lg p_none"><i class="fas fa-file-pdf"></i> VER LISTADO GANADORES</a>
						<br>
						<small class="text-light">(*) Resultados disponibles el 31 de Julio del 2019</small>
					</div>
				</div>
			</div>
			<!--
			<div class="carousel-item">
				<div class="d-block w-100 slider_item" alt="..."></div>
			</div>
			-->

		</div>
		<a class="carousel-control-prev" href="#slider_header" role="button" data-slide="prev">
			<span class="carousel-control-prev-icon" aria-hidden="true"></span>
			<span class="sr-only">Previous</span>
		</a>
		<a class="carousel-control-next" href="#slider_header" role="button" data-slide="next">
			<span class="carousel-control-next-icon" aria-hidden="true"></span>
			<span class="sr-only">Next</span>
		</a>
	</div>



<!--
	<div class="container-fluid col-md-9" style="top:80px;">
		<div class="row">
			<div class="col-md-4 d-flex justify-content-end">
				<img src="<?= base_url();?>image/lukitas_border.png" class="w-100 logo">
			</div>
			<div class="col-md-8 d-inline align-self-center"> <span class="line_1">9° CONCURSO</span>
				<br> <span class="line_2">Luka$ para Emprender</span>
				<br> <span class="line_3">2019</span>
				<hr class="line_separator"><span class="line_4">Subdirección de Desarrollo Económico Local</span>
				<br> <span class="line_4">Dirección de Desarrollo Comunitario</span>
			</div>
		</div>
	</div>
-->

</div>






<div class="container-fluid" style="background-color: #00307f">
	<div class="container text-center time_info">
		<p>Resultados Preselección. <b>11 de Junio</b> del 2019
		<p>Listado de ganadores. <b>31 de Julio</b> del 2019</p>
		<!--
		<div id="countdown" style="text-transform: lowercase;"><b>00</b> horas <b>00</b> minutos <b>00</b> segundos</div>
		-->
	</div>
</div>

<div class="container text-align w-75 mt-5 mb-5" id="ganadores">
	<div class="row">
		<div class="col-md-12 bg-light p-4 rounded">
			<h4><b>Listado de Ganadores Concurso Lukas para Emprender</b></h4>
			<hr>
			<p>
				*Los ganadores del concurso serán contactados vía <b><u>correo electrónico</u></b> para mayor información.
			</p>
			<a href="<?= base_url(); ?>files/resultados/Ganadores_Categoria_Emprendimiento_Empresa_lukas_2019.pdf" target="_blank" class="btn btn-primary btn-lg p_none"><i class="fas fa-file-pdf"></i>  VER LISTADO GANADORES</a>
			<br>
			<small class="text-muted">(*) Resultados disponibles el 31 de Julio del 2019</small>
		</div>
	</div>
</div>


<div class="container text-align w-75 mt-5 mb-5" id="preseleccion">
	<div class="row">
		<div class="col-md-12 bg-light p-4 rounded">
			<h4>Resultados preselección</h4>
			<hr>
			<p>
1.- Las notas <b>NO SON ACUMULATIVAS</b> durante el proceso del concurso.<br>
2.- Los preseleccionados de la categoría EMPRENDIMIENTO son 35<br>
3.- Los preseleccionados de la categoría EMPRESA son 20.<br>
4.- Se comunicaran con todos los preseleccionados para realizar las visitas a terreno, entre el 14 al 26 de Junio.<br>
5.- Dudas, consultas serán realizadas <b><u>UNICAMENTE VIA CORREO ELECTRONICO</u></b>, en <a href="mailto:lukasparaemprender@puertomontt.cl">lukasparaemprender@puertomontt.cl</a> o <a href="mailto:lukasparaemprender@gmail.com">lukasparaemprender@gmail.com</a>.<br><br>
<b>
SE RECUERDA A TODOS LOS POSTULANTES QUE EL MEDIO DE COMUNICACION EN ESTÁ
PRIMERA ETAPA ENTRE POSTULANTES Y COMISION TÉCNICA EVALUADORA ES CORREO
ELECTRONICO ENTREGADO EN EL FORMULARIO DE POSTULACION.
</b>
			</p>

			<a href="<?= base_url(); ?>files/resultados/Preseleccionados_LukasparaEmprender_2019.xlsx" target="_blank" class="btn btn-success btn-lg p_none"><i class="fas fa-file-excel"></i>  VER RESULTADOS PRESELECCIÓN</a>
			<br>
			<small class="text-muted">(*) Resultados disponibles el 11 de Junio del 2019</small>
		</div>
	</div>
</div>


<div class="container text-align w-75 mt-5 mb-5">
	<div class="row" id="concurso">
		<div class="col-md-3">
			<div class="alert alert-warning card_container" role="alert">
	      <h2 class="alert-heading"><b><i class="fas fa-book-reader"></i> PASO 1</b></h2>
	      <h5>Leer las bases del concurso</h5>
	    </div>
		</div>
		<div clasS="col-md-9 bg-light p-4 rounded">
			<h5>Bases del concurso</h5>
			<p>Bases de postulación para el concurso <b>Luka$ para Emprender</b>. Puerto Montt, Abril 2019.</p>
				<a href="<?= base_url(); ?>files/BasesLukasparaemp2019.pdf" target="_blank" class="btn btn-primary btn-lg p_none"><i class="fas fa-download"></i> Ver bases</a>

		</div>
	</div>
<hr>
	<div class="row">
		<div class="col-md-3">
			<div class="alert alert-warning card_container" role="alert">
	      <h2 class="alert-heading"><b><i class="fas fa-align-left"></i> PASO 2</b></h2>
	      <h5>Llenar todos los campos del formulario</h5>
	    </div>
		</div>
		<div clasS="col-md-9 bg-light p-4 rounded">
			<h5>Seleccione una opción</h5>
			<form class="needs-validation" action="home/selection" method="post" novalidate>
				<div class="form-row">
					<div class="form-group w-100">
						<select class="custom-select w-100" id="select_type" name="select_type" required disabled>
							<option value="1">Emprendimiento</option>
							<option value="2">Empresa</option>
						</select>
					</div>
				</div>
				<div class="form-row">
					<div class="form-group w-100">
						<div class="custom-control custom-checkbox">
							<input type="checkbox" class="custom-control-input" id="customControlValidation1" required disabled>
							<label class="custom-control-label" for="customControlValidation1">Acepto haber leido las bases y tener pleno conocimiento de lo que estoy haciendo</label>
							<div class="invalid-feedback">Marque esta casilla si desea continuar</div>
						</div>
					</div>
				</div>
				<button type="submit" class="btn btn-secondary btn-lg p_none" type="submit" name="submit" disabled><i class="fas fa-check-square"></i> Aceptar y continuar</button>
			</form>
		</div>
	</div>
<hr>
	<div class="row">
		<div class="col-md-3">
			<div class="alert alert-warning card_container" role="alert">
	      <h2 class="alert-heading"><b><i class="fas fa-sign-in-alt"></i> PASO 3</b></h2>
	      <h5>Iniciar sesión con los datos enviados a su correo</h5>
	    </div>
		</div>
		<div clasS="col-md-9 bg-light p-4 rounded">
			<h5>Iniciar sesión</h5>
			<p>Si ya se registró, llenando el formulario de postulación, puede iniciar sesión con los datos enviados a su correo.</p>
			<a href="<?=base_url();?>login/user" class="btn btn-primary btn-lg p_none" tabindex="-1" aria-disabled="true">
				<i class="fas fa-sign-in-alt"></i> Iniciar sesión
			</a>
		</div>
	</div>
</div>


<!-- End parallax
<div class="container text-align w-75">
	<div class="card bg-transparent border-0 align-self-center">
		<div class="card-body">
			<div class="row">
				<div class="card w-100 card_file">
				  <div class="row no-gutters">
				    <div class="col-md-3 text-center align-self-center">
				      <i class="far fa-file-pdf icon_file"></i>
				    </div>
				    <div class="col-md-9">
				      <div class="card-body">
				        <h5 class="card-title">Bases del concurso</h5>
				        <p class="card-text">Bases de postulación para el concurso <b>Luka$ para Emprender</b>. Bases de postulación para categorías.<br>Puerto Montt, Abril 2019.</p>
								<p class="card-text">

									<a href="./uploaded_files/Bases_LukasparaEmprender_2019.pdf" target="_blank" class="btn btn-primary btn-lg" type="submit" name="submit"><i class="fas fa-download"></i> Ver bases</a></p>
				      </div>
				    </div>
				  </div>
				</div>
			</div>
			<br>
			<div class="row">
					<div class="card w-100">
						<div class="card-body text-center" style="padding-bottom: 0.4em;">
							<h5 class="card-title">Seleccione una opción</h5>
              <p class="card-text">
              <div class="row">
                <div class="col-md-8 registro_card">
                  <div class="card option_card">
                    <div class="card-body">

                      <b>Concursar</b>
                      <br><br>
                      <form class="needs-validation" action="home/selection" method="post" novalidate>
                        <div class="form-row">
                          <div class="form-group w-100">
                            <select class="custom-select w-100" id="select_type" name="select_type" required>
                              <option value="1">Emprendimiento</option>
                              <option value="2">Empresa</option>
                            </select>
                          </div>
                        </div>
                        <div class="form-row">
                          <div class="form-group w-100">
                            <div class="custom-control custom-checkbox">
                              <input type="checkbox" class="custom-control-input" id="customControlValidation1" required>
                              <label class="custom-control-label" for="customControlValidation1">Acepto haber leido las bases y tener pleno conocimiento de lo que estoy haciendo</label>
                              <div class="invalid-feedback">Marque esta casilla si desea continuar</div>
                            </div>
                          </div>
                        </div>
                        <br>
                        <button type="submit" class="btn btn-primary btn-lg" type="submit" name="submit" <?=$disabled; ?>><i class="fas fa-check-square"></i> Aceptar y continuar</button>
                      </form>
                    </div>
                  </div>

                </div>

                <div class="col-md-4">
                  <div class="card option_card">
                    <div class="card-body">
                      <b>Iniciar sesión</b><br><br>
                      <p>Si ya se registró, llenando el formulario de postulación, puede iniciar sesión con los datos enviados a su correo.</p>
                      <br>
                      <a href="<?=base_url();?>login/user" class="btn btn-primary btn-lg" tabindex="-1" aria-disabled="true">
                        <i class="fas fa-sign-in-alt"></i> Iniciar sesión
                      </a>
                    </div>
                  </div>


                </div>
              </div>


						</p>
          </div>
					</div>

			</div>
		</div>
	</div>
</div>

-->

<div class="conteiner-fluid container_sponsors">
	<div class="container">
		<div class="row justify-content-md-center">
			<div class="col-md-2 col-5 align-self-center">
		<a href="#" target="_blank">
          <img src="<?=base_url();?>image/escudopuertomontt_border.png" class="w-100">
        </a>
			</div>
			<div class="col-md-2 col-5 align-self-center"> <a href="#" target="_blank">
          <img src="<?=base_url();?>image/dideco_logo_trans.png" class="w-100">
        </a>
			</div>
			<div class="col-md-3 col-7 align-self-center"> <a href="#" target="_blank">
          <img src="<?=base_url();?>image/universidadloslagos.png" class="w-100">
        </a>
			</div>
			<div class="col-md-2 col-5 align-self-center"> <a href="#" target="_blank">
          <img src="<?=base_url();?>image/logopuertomontt.png" class="w-100">
        </a>
			</div>
		</div>
	</div>
</div>
