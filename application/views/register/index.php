<div class="container fix_header">
   <div class="row justify-content-center">
      <div class="col-md-9">
         <div class="card" style="border:none;">
            <div class="card-body">
             <h1 class="h3 mb-3">Registrarse</h1>
              <hr>
        
              <p class="text-muted">
                <small>Todos los campos son obligatorios. Se enviará un correo con instrucciones para activar su cuenta y llenar su formulario para el concurso <b>Lukas para Emprender</b>.</small>
              </p>

               <form class="needs-validation" action="<?= base_url();?>register/insert" method="post" enctype="multipart/form-data" style="width:100%;"  novalidate >

                     <div class="form-group row">
                        <label for="user_type" class="col-sm-3 col-form-label"><b>Seleccione tipo</b></label>
                        <div class="col-sm-9">
                           <select class="custom-select w-100" id="user_type" name="user_type" required>
                              <option value="0">Emprendimiento</option>
                              <option value="1">Empresa</option>
                           </select>
                        </div>
                     </div>

                  <div class="form-group row">
                     <label for="user_rut" class="col-sm-3 col-form-label">RUT</label>
                     <div class="col-sm-9">
                        <span class="fas fa-id-card form-control-feedback"></span>
                        <input type="text" class="form-control" id="user_rut" name="user_rut" placeholder="12345678-9" maxlength="12" required>
                        <div class="invalid-feedback">RUT incorrecto o ya utilizado.</div>
                     </div>
                  </div>
                  <div class="form-group row">
                     <label for="user_name" class="col-sm-3 col-form-label">Nombre completo</label>
                     <div class="col-sm-4">
                        <span class="fas fa-user form-control-feedback"></span>
                        <input type="text" class="form-control" id="user_name" name="user_name" placeholder="Nombres" required>
                        <div class="invalid-feedback">Ingrese Nombre.</div>
                     </div>
                     <div class="col-sm-5">
                        <span class="fas fa-user form-control-feedback"></span>
                        <input type="text" class="form-control" id="user_surname" name="user_surname" placeholder="Apellidos" required>
                        <div class="invalid-feedback">Ingrese Apellidos.</div>
                     </div>
                  </div>
                  <div class="form-group row">
                     <label for="user_email" class="col-sm-3 col-form-label"><b>Correo electrónico</b></label>
                     <div class="col-sm-9">
                        <span class="fas fa-at form-control-feedback"></span>
                        <input type="email" class="form-control" id="user_email" name="user_email" placeholder="nombre@dominio.com" required>
                        <div class="invalid-feedback">Coreo electrónico obligatorio.</div>
                     </div>
                  </div>
                  <br>
                  <div class="form-group row">
                     <label for="user_pass" class="col-sm-3 col-form-label">Contraseña</label>
                     <div class="col-sm-9">
                        <span class="fas fa-key form-control-feedback"></span>
                        <div class="input-group" id="group_password">
                           <input type="password" class="form-control" id="user_pass" name="user_pass" placeholder="*********" required>
                           <div class="input-group-append">
                              <a href="" class="input-group-text"><i class="fas fa-eye-slash"></i></a>
                           </div>
                        </div>
                        <div class="invalid-feedback">Ingrese contraseña.</div>
                     </div>
                  </div>
                  <div class="form-group row">
                     <label for="user_repeat" class="col-sm-3 col-form-label">Repetir contraseña</label>
                     <div class="col-sm-9">
                        <span class="fas fa-key form-control-feedback"></span>
                        <input type="password" class="form-control" id="user_repeat" name="user_repeat" placeholder="*********" required>
                        <div class="invalid-feedback">Las contraseñas no coinciden.</div>
                     </div>
                  </div>

                  <br>
                  <button type="submit" class="btn btn-primary w-100" id="submit_button" disabled>
                     Registrarse <i class="fas fa-sign-in-alt"></i>
                  </button>

                  <br><br>

                  <small>
                     <center>
                        <p>¿Ya posees una cuenta? <a href="<?= base_url();?>login/user">Inicia sesión aquí</a>.</p>
                        <p class="mt-5 mb-3 text-muted">Municipalidad de Puerto Montt - 2019</p>
                     </center>
                  </small>
               </form>
            </div>
         </div>
      </div>
   </div>
</div>
<script type="text/javascript">
   $('#user_repeat').on('keyup', function () {

      if ($('#user_pass').val() != $('#user_repeat').val()) {
         $('#user_repeat').next('.invalid-feedback').text('Las contraseñas no coinciden.');
         $('#user_repeat').removeClass('is-valid').addClass('is-invalid');

      } else {
         $('#user_repeat').removeClass('is-invalid').addClass('is-valid');
      }
   });

</script>


