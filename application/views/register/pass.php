<?php $this->load->helper('cookie'); ?>
<div class="container fix_header">
  <div class="row justify-content-center">
    <div class="col-md-5">
      <div class="card">
        <div class="card-body">
      <form class="needs-validation" action="<?= base_url();?>register/reset_pass" method="post" style="width:100%;"  novalidate >
        <input type="hidden" id="hash" name="hash" value="<?php echo $_GET['hash']; ?>" >
      <h1 class="h3 mb-3">Recuperar contraseña</h1>
        <hr>
        <p class="text-muted">
          <small>Ingrese una nueva contraseña y confirme en el campo inferior.</small>
        </p>
        <div class="form-group">
          <label for="user_email">Contraseña temporal</label>
          <span class="fas fa-key form-control-feedback"></span>
          <input type="password" class="form-control" id="old_pass" placeholder="********" name="old_pass" required>
          <div class="invalid-feedback">Ingrese contraseña</div>
        </div>
      <div class="form-group">
        <label for="user_email">Nueva Contraseña</label>
        <span class="fas fa-key form-control-feedback"></span>
        <input type="password" class="form-control" id="new_pass" placeholder="********" name="new_pass" required>
        <div class="invalid-feedback">Ingrese contraseña</div>
      </div>
      <div class="form-group">
        <label for="user_email">Repetir Contraseña</label>
        <span class="fas fa-key form-control-feedback"></span>
        <input type="password" class="form-control" id="rep_pass" placeholder="*********" name="rep_pass" required>
        <div class="invalid-feedback">Ingrese contraseña</div>
      </div>
      <br>
      <center>
        <button type="submit" class="btn btn-primary w-100">Actualizar contraseña <i class="fas fa-sign-in-alt"></i></button>
        <small>
          <p class="mt-5 mb-3 text-muted">Municipalidad de Puerto Montt - 2019</p>
        </small>
        </center>
      </form>

    </div>
  </div>
    </div>
  </div>
</div>
