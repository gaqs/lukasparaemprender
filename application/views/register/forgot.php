
<div class="container fix_header">
  <div class="row justify-content-center">
    <div class="col-md-5">
      <div class="card">
        <div class="card-body">
      <form class="needs-validation" action="<?= base_url();?>register/send_recover_pass" method="post" style="width:100%;"  novalidate >
        <h1 class="h3 mb-3">Recuperar contraseña</h1>
        <hr>
        <p class="text-muted">
          <small>Olvidaste tu contraseña?. Te enviaremos un correo con instrucciones para resetear su contraseña asociada a su cuenta.</small>
        </p>

        <div class="form-group">
          <label for="user_email">Correo electrónico</label>
          <span class="fas fa-at form-control-feedback"></span>
          <input type="email" class="form-control" id="registered_email" placeholder="nombre@dominio.com" name="email" required>
          <div class="invalid-feedback">Ingrese correo electrónico</div>
        </div>

        <button type="submit" class="btn btn-primary w-100">Recuperar contraseña <i class="fas fa-sign-in-alt"></i></button>
        <center>
          <small><p class="mt-5 mb-3 text-muted">Municipalidad de Puerto Montt - 2019</p></small>
        </center>
      </form>

    </div>
  </div>
    </div>
  </div>
</div>