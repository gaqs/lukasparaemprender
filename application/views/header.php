<!doctype html>
<html lang="en">
  <head>
    <!-- Favicon and touch icons -->
    <link rel="shortcut icon" href="<?=base_url();?>image/icon.ico">

    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Luka$ para emprender</title>

    <!-- CSS -->
    <link rel="stylesheet" href="<?= base_url();?>bootstrap/css/bootstrap.css" >
    <link rel="stylesheet" href="<?= base_url();?>fontawesome/css/all.css" >
    <link rel="stylesheet" href="<?= base_url();?>css/style.css" >
    <link href="https://fonts.googleapis.com/css?family=Lobster" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Optional JavaScript -->
    <script src="<?= base_url();?>js/jquery-3.3.1.js"></script>
    <script src="<?= base_url();?>bootstrap/js/bootstrap.bundle.js"></script>
    <script src="<?= base_url();?>js/bs-custom-file-input.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCqbhAvlHQRqqyN4YVkVRvTOPVrxSVjA6c&callback=initMap" type="text/javascript"></script>
    <script type="text/javascript">
    		function preventLoadRoboto(){
    			var head = document.getElementsByTagName('head')[0];
    			var insertBefore = head.insertBefore;
    			head.insertBefore = function (newElement, referenceElement) {
    				if (newElement.href && newElement.href.indexOf('https://fonts.googleapis.com/css?family=Roboto') === 0) {
    					//console.info('Prevented Roboto from loading!');
        				return;
    				}
    				insertBefore.call(head, newElement, referenceElement);
    			};

          /* Nombre del archivo en el custom file input bootstrap */
          $(document).ready(function(){
            $('.custom-file-input').on('change',function(e){
               var fileName = e.target.files[0].name;
               $(this).next('.custom-file-label').html(fileName);
            });
           });

    		}
    </script>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-137432398-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-137432398-1');

      $(function () {
        $('[data-toggle="tooltip"]').tooltip({
          html: true
        });
        $('[data-tt="tooltip"]').tooltip({
          html: true
        });

      });

    </script>
  </head>
  <body>

  <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
    <a class="navbar-brand" href="<?= base_url(); ?>">
        <b>LUKAS PARA</b> EMPRENDER

    </a>

  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse navbar-right" id="navbarNav">
    <ul class="navbar-nav ml-auto">
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="concurso" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">9° Concurso</a>
       <div class="dropdown-menu" aria-labelledby="concurso">
         <a class="dropdown-item" href="<?=base_url();?>">Inicio</a>
         <a class="dropdown-item" href="<?=base_url();?>files/BasesLukasparaemp2019.pdf" target="_blank">Bases </a>
         <a class="dropdown-item" href="<?=base_url();?>#ganadores">Ganadores </a>
       </div>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="concurso" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Desafío innovación social</a>
       <div class="dropdown-menu" aria-labelledby="concurso">
         <a class="dropdown-item" href="<?=base_url();?>innovacion">Inicio</a>
         <a class="dropdown-item" href="<?=base_url();?>files/BasesDesafioInnovacion2019.pdf" target="_blank">Bases </a>
         <a class="dropdown-item" href="<?=base_url();?>innovacion#resultados">Ganadores</a>
       </div>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?= base_url(); ?>home/contacto">Consultas</a>
      </li>
      <li>
        <a class="nav-link" href="<?= base_url(); ?>register">Registrarse</a>
      </li>
      <li class="nav-item dropdown">
        <?php
          $user_name = $this->session->userdata('user_name');
          $inn_name = $this->session->userdata('inn_name');
          if( isset( $user_name )) {
            echo '<a class="nav-link dropdown-toggle" href="#" id="session_user" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Hola! '.$user_name.'
                   </a>
                   <div class="dropdown-menu" aria-labelledby="session_user">
                     <a class="dropdown-item" href="'.base_url().'user">Formulario</a>
                     <a class="dropdown-item" href="'.base_url().'login/user_destroy">Logout</a>
                   </div>';
          }else if( isset( $inn_name ) ){
            echo '<a class="nav-link dropdown-toggle" href="#" id="session_user" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Hola! '.$inn_name.'
                   </a>
                   <div class="dropdown-menu" aria-labelledby="session_user">
                     <a class="dropdown-item" href="'.base_url().'user/innovacion">Formulario</a>
                     <a class="dropdown-item" href="'.base_url().'login/inn_destroy">Logout</a>
                   </div>';
          }else{
            echo '<a class="nav-link" href="'.base_url().'login/user">Ingresar</a>';
          }
        ?>
      </li>
    </ul>
  </div>
</div>
</nav>
