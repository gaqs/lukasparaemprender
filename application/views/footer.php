<!-- Footer -->
<footer class="page-footer font-small mdb-color pt-4 mt-4" style="margin-top: 0 !important;color:white;background-color:#202020;">
  <!-- Footer Links -->
  <div class="container text-center text-md-left">

    <!-- Footer links -->
    <div class="row text-center text-md-left mt-3 pb-3">

      <!-- Grid column -->
      <div class="col-md-4 col-lg-4 col-xl-4 mx-auto mt-3">
        <h6 class="text-uppercase mb-4 font-weight-bold">DIDECO</h6>
        <p>
          <address>
            <strong>Subdirección de Desarrollo Económico Local<br>Dirección de Desarrollo Comunitario</strong>
            <br>
            <a href="https://www.puertomontt.cl/unidades-municipales/dideco/" target="_blank">Más información</a>
          </address>
        </p>
      </div>
      <!-- Grid column -->

      <hr class="w-100 clearfix d-md-none">

      <!-- Grid column -->
      <hr class="w-100 clearfix d-md-none">

      <!-- Grid column -->
      <div class="col-md-5 col-lg-4 col-xl-4 mx-auto mt-4">
        <h6 class="text-uppercase mb-4 font-weight-bold">CONTACTO</h6>
        <p><i class="fa fa-home mr-3"></i>Av. Presidente Ibañez #600.<br>Edificio Consistorial II<br></p>
        <p><i class="fa fa-envelope mr-3"></i>lukasparaemprender@puertomontt.cl</p>
        <p><i class="fa fa-phone mr-3"></i> (+65) 2 261315</p>
        <p><i class="fa fa-phone mr-3"></i> (+65) 2 261323</p>
      </div>
      <!-- Grid column -->

      <!-- Grid column -->
      <div class="col-md-4 col-lg-3 col-xl-3 mx-auto mt-3">
        <h6 class="text-uppercase mb-4 font-weight-bold">Links Útiles</h6>
        <p><a href="https://www.puertomontt.cl/" target="_blank">Municipalidad Puerto Montt</a></p>
        <p><a href="http://www.ulagos.cl/" target="_blank">Universidad de los Lagos</a></p>
        <p><a href="http://www.ulagos.cl/category/campus-pto-montt/" target="_blank">ULL Campus Puerto Montt</a></p>
        <p> <a href="<?= base_url();?>login" target="_blank">Acceso Administrador</a></p>
      </div>

    </div>
    <!-- Footer links -->

    <hr>

    <!-- Grid row -->
    <div class="row d-flex align-items-center pb-2">

      <!-- Grid column -->
      <div class="col-md-12 col-lg-12">

        <!--Copyright-->
        <small class="text-center text-md-left">Municipalidad de Puerto Montt |<strong> Gustavo Quilodrán Sanhueza</strong> | contact: gaqs.02@gmail.com </small>

      </div>
      <!-- Grid column -->
    </div>
    <!-- Grid row -->

  </div>
  <!-- Footer Links -->

</footer>
<!-- Footer -->
</body>
</html>
  <script src="<?= base_url();?>js/jscript.js"></script>
  <script>

  var url = '<?= base_url();?>';

  $('body').on('focusout', '#user_rut, #user_email', function(){
      var vrut = 0;
      var vcorreo = 0;

      var input_rut = $('#user_rut')[0];
      var rut = $(input_rut).val();
      var input_email = $('#user_email')[0];
      var email = $(input_email).val();

      if( rut != '' ){
        checkRut(input_rut);
        rut = $(input_rut).val();

        $.ajax({
          url : url + "admin/verify_rut",
          type: "POST",
          data : 'rut='+rut,
          success: function(html){
            if(html == 'error'){
              $('#user_rut').next('.invalid-feedback').text('RUT ya utilizado o erroreo pruebe otro.');
              $('#user_rut').removeClass('is-valid').addClass('is-invalid');
              vrut = 0;

            }else if( html == 'error_validacion'){
              $('#user_rut').next('.invalid-feedback').text('Formato RUT incorrecto.');
              $('#user_rut').removeClass('is-valid').addClass('is-invalid');
              vrut = 0;

            }else{
              $('#user_rut').removeClass('is-invalid').addClass('is-valid');
              vrut = 1;
            }

            validation_submit(vrut,vcorreo);

          }
        });
      }

      if( email != '' ){
        $.ajax({
          url : url + "admin/verify_email",
          type: "POST",
          data : 'email='+email,
          success: function(html){
            if(html == 'error'){
              $('#user_email').next('.invalid-feedback').text('Correo ya utilizado, pruebe otro.');
              $('#user_email').removeClass('is-valid').addClass('is-invalid');
              vcorreo = 0
            }else{
              $('#user_email').removeClass('is-invalid').addClass('is-valid');
              vcorreo = 1;
            }

            validation_submit(vrut,vcorreo);

          }
        });
      }
  });

  $('body').on('focusout', '#business_rut', function(){ checkRut(this); });
  $('body').on('focusin', '#user_rut', function(){ $('#user_rut').removeClass('is-invalid is-valid'); });
  $('body').on('focusin', '#user_email', function(){ $('#user_email').removeClass('is-invalid is-valid'); });
  $('body').on('submit', '#submit_button', function(){
    $('button.btn-xlg i').removeClass('fas fa-share-square').addClass('fas fa-circle-notch fa-spin');
  });


  function validation_submit(vrut,vcorreo){
      if( vrut == 1 && vcorreo == 1){
          $('#submit_button').attr('disabled', false);
      }else{
          $('#submit_button').attr('disabled', true);
      }
  }
<<<<<<< HEAD
=======

/*
  // Set the date we're counting down to
var countDownDate = new Date("May 8, 2019 23:59:59").getTime();

// Update the count down every 1 second
var x = setInterval(function() {

  // Get todays date and time
  var now = new Date().getTime();

  // Find the distance between now and the count down date
  var distance = countDownDate - now;

  // Time calculations for days, hours, minutes and seconds
  var days = Math.floor(distance / (1000 * 60 * 60 * 24));
  var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
  var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
  var seconds = Math.floor((distance % (1000 * 60)) / 1000);

  // Output the result in an element with id="demo"
  document.getElementById("countdown").innerHTML = "<b>" + hours + "</b> horas <b>"
  + minutes + "</b> minutos <b>" + seconds + "</b> segundos ";

  // If the count down is over, write some text
  if (distance < 0) {
    clearInterval(x);
    document.getElementById("countdown").innerHTML = "<b>00</b> horas <b>00</b> minutos <b>00</b> segundos";
  }
}, 1000);
*/



$("#group_password a").on('click', function(event) {
        event.preventDefault();
        if($('#group_password input').attr("type") == "text"){
            $('#group_password input').attr('type', 'password');
            $('#group_password i').addClass( "fa-eye-slash" );
            $('#group_password i').removeClass( "fa-eye" );
        }else if($('#group_password input').attr("type") == "password"){
            $('#group_password input').attr('type', 'text');
            $('#group_password i').removeClass( "fa-eye-slash" );
            $('#group_password i').addClass( "fa-eye" );
        }
    });


>>>>>>> registro
  </script>
