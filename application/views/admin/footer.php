</div>
        <!-- /#page-content-wrapper -->

    </div>
    <!-- /#wrapper -->

</body>
</html>
<script>

var url = '<?= base_url(); ?>';

$("#menu-toggle").click(function(e) {
    e.preventDefault();
    $("#wrapper").toggleClass("toggled");
});

(function() {
  'use strict';
  window.addEventListener('load', function() {
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        }
        form.classList.add('was-validated');
      }, false);
    });
  }, false);
})();

$('#delete_modal').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget)
  var recipient = button.data('whatever')
  var modal = $(this)
  modal.find('.modal-body input').val(recipient)
})

function confirmSubmit() {
  var agree = confirm("¿Está seguro que desea guardar?");
  if (agree) return true ;
  else return false ;
}

$(document).ready(function(){

  $('#users_table').DataTable({
    responsive: true
  });

  $('body').on('click', '#edit_user', function(){
			var id = $(this).val();
			$.ajax({
				url : url + "admin/modal_edit_empren",
				type: "POST",
				data : 'id='+id,
				success: function(html){
					$('#db_modal .modal-lg .modal-content').html(html);
					$('#db_modal').modal('toggle');
				}
			});
		});

    $('body').on('click', '#edit_empresa', function(){
  			var id = $(this).val();
  			$.ajax({
  				url : url + "admin/modal_edit_empresa",
  				type: "POST",
  				data : 'id='+id,
  				success: function(html){
  					$('#db_modal .modal-lg .modal-content').html(html);
  					$('#db_modal').modal('toggle');
  				}
  			});
  		});

      $('body').on('click', '#edit_inn_user', function(){
          var group_id = $(this).val();
          $.ajax({
            url : url + "admin/modal_edit_inn",
            type: "POST",
            data : 'group_id='+group_id,
            success: function(html){
              $('#db_modal .modal-lg .modal-content').html(html);
              $('#db_modal').modal('toggle');
            }
          });
        });

});

    //Jquery notas por preguntas
    $('body').on('click','input[type="radio"]', function(){
        //id usuario
        var idusuario = $('#user_id').val();

        //nota
        var nota = $(this).val();

        //n criterio
        var criterio = $(this).attr('name');
        criterio = criterio.replace('radio_notes_','');

        nquestion = criterio.substr(0,1);
        criterio = criterio.substr(2,1);

        //console.log('id usuario: '+idusuario+'   n pregunta: '+nquestion+'   n criterio: '+criterio+'   n nota: '+nota);

        $.ajax({
            url : url + "admin/save_notas",
            type: "POST",
            data : 'id_usuario='+idusuario+'&npregunta='+nquestion+'&ncriterio='+criterio+'&nota='+nota,
            success: function(html){
            }
        });


    });



    function exportTableToExcel(tableID, filename = ''){
      var downloadLink;
      var dataType = 'application/vnd.ms-excel';
      var tableSelect = document.getElementById(tableID);
      var tableHTML = tableSelect.outerHTML.replace(/ /g, '%20');

      // Specify file name
      filename = filename?filename+'.xls':'excel_data.xls';

      // Create download link element
      downloadLink = document.createElement("a");

      document.body.appendChild(downloadLink);

      if(navigator.msSaveOrOpenBlob){
          var blob = new Blob(['\ufeff', tableHTML], {
              type: dataType
          });
          navigator.msSaveOrOpenBlob( blob, filename);
      }else{
          // Create a link to the file
          downloadLink.href = 'data:' + dataType + ', ' + tableHTML;

          // Setting the file name
          downloadLink.download = filename;

          //triggering the function
          downloadLink.click();
      }
  }


</script>
