<?php
$array_val = array();
foreach ($user[0] as $key => $value) {
  $pos = strpos( $key, 'val_' );
  if( $pos == 'false' && $value == '0'){
      array_push($array_val, $key);
  }
}
$val = implode(',',$array_val);
$promedio = promedio_notas($notas);

$checked = '';
if( $user[0]['descalificado'] == 1 ){
    $checked = 'checked';
}

?>
<div class="modal-header">
    <h5 class="modal-title" id="">
        Editar Emprendimiento <b><span class="ml-10">[nota promedio: <div class="d-inline" id="promedio"><?= $promedio; ?></div>]</span></b>
    </h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
     <span aria-hidden="true">&times;</span>
     </button>
</div>

    <div class="modal-body">
      <form class="needs-validation" novalidate action="admin/edit_user" method="post" enctype="multipart/form-data">

        <div class="card sub_card card_db">
            <div class="card-header">
                <i class="fas fa-user-alt"></i> Informacion básica
            </div>
            <div class="card-body">
                <input type="hidden" value="<?= $user[0]['id'] ?>" class="form-control" id="user_id" name="user_id">
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="check_proyect_name" name="b_checks[]" value="val_nombre" disabled>
                            <label class="custom-control-label" for="check_proyect_name">Nombre proyecto</label>
                        </div>
                        <input type="text" value="<?= $user[0]['nombre'] ?>" class="form-control" id="user_proyect_name" name="user_proyect_name" placeholder="Ingrese nombre del proyecto" readonly>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="check_rut" name="checks[]" value="val_rut" disabled>
                            <label class="custom-control-label" for="check_rut">RUT</label>
                        </div>
                        <input type="text" value="<?= $user[0]['rut'] ?>" class="form-control" id="user_rut" name="user_rut" placeholder="RUT" placeholder="" oninput="checkRut(this)" required readonly>
                        <div class="invalid-feedback">RUT incorrecto.</div>
                    </div>

                    <div class="form-group col-md-4">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="check_nombres" name="checks[]" value="val_nombres">
                            <label class="custom-control-label" for="check_nombres">Nombres</label>
                        </div>
                        <input type="text" value="<?= $user[0]['nombres'] ?>" class="form-control" id="user_name" name="user_name" placeholder="Nombre" required>
                        <div class="invalid-feedback">Ingrese Nombre.</div>
                    </div>

                    <div class="form-group col-md-4">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="check_apellidos" name="checks[]" value="val_apellidos">
                            <label class="custom-control-label" for="check_apellidos">Apellidos</label>
                        </div>
                        <input type="text" value="<?= $user[0]['apellidos'] ?>" class="form-control" id="user_surname" name="user_surname" placeholder="Apellidos" required>
                        <div class="invalid-feedback">Ingrese Apellidos.</div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" id="check_ocupacion" name="checks[]" value="val_ocupacion">
                        <label class="custom-control-label" for="check_ocupacion">Ocupacion</label>
                    </div>
                    <input type="text" value="<?= $user[0]['ocupacion'] ?>" class="form-control" id="user_job" name="user_job" placeholder="Ocupacion/Profesion" required>
                    <div class="invalid-feedback">Ingrese Ocupación.</div>
                </div>
                <div class="form-group">
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" id="check_direccion" name="checks[]" value="val_direccion">
                        <label class="custom-control-label" for="check_direccion">Direccion</label>
                    </div>
                    <input type="text" value="<?= $user[0]['direccion'] ?>" class="form-control" id="user_address" name="user_address" placeholder="Departamento, Direccion" required>
                    <div class="invalid-feedback">Ingrese Dirección.</div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="check_telefono" name="checks[]" value="val_telefono">
                            <label class="custom-control-label" for="check_telefono">Telefono</label>
                        </div>
                        <input type="text" value="<?= $user[0]['telefono'] ?>" class="form-control" id="user_phone" name="user_phone" placeholder="Telefono" size="10" required>
                        <div class="invalid-feedback">Ingrese Teléfono.</div>
                    </div>
                    <div class="form-group col-md-6">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="check_email" name="checks[]" value="val_email" disabled>
                            <label class="custom-control-label" for="check_email">Email</label>
                        </div>
                        <input type="email" value="<?= $user[0]['email'] ?>" class="form-control" id="user_email" name="user_email" placeholder="Correo electronico" required readonly>
                        <div class="invalid-feedback">Ingrese Email.</div>
                    </div>
                </div>
            </div>
        </div>


        <br>


        <div class="card sub_card">
            <div class="card-header">
                <i class="fas fa-briefcase"></i> Informacion relevante al negocio
            </div>
            <div class="card-body">
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="check_explicacion" name="b_checks[]" value="val_resumen">
                    <label class="custom-control-label" for="check_explicacion">1.- Explique de forma breve su negocio y/o emprendimiento.</label>
                </div>
                <textarea class="form-control" id="bussiness_explication" name="bussiness_explication" rows="4" required><?= trim($user[0]['resumen']) ?></textarea>

                <!-- INTERFAZ PREGUNTAS RADIOBUTTONS -->
                <div class="container_notes" style="font-size: 12px;">
                  <button class="btn btn-light w-100" type="button" data-toggle="collapse" data-target="#collapse_notas1" aria-expanded="false" aria-controls="collapse_notas1">
                    Abrir criterios y seleccionar nota
                  </button>
                
                <div class="collapse" id="collapse_notas1">
                  <div class="card card-body p-2">

                        <table class="table table_notes">
                            <thead>
                                <tr>
                                    <th class="w-50">Factor</th>
                                    <th>Nota</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>¿Describe claramente su propuesta de negocio?</td>
                                    <td>
                                        <?php
                                        for($i=1; $i<8; $i++){
                                            echo '<div class="form-check form-check-inline">
                                                  <input class="form-check-input" type="radio" name="radio_notes_1_1" id="radio'.$i.'" value="'.$i.'">
                                                  <label class="form-check-label" for="inlineCheckbox1">'.$i.'</label>
                                                </div>';
                                        }
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Define la oportunidad de negocio? (define problema y solución)</td>
                                    <td>
                                        <?php
                                        for($i=1; $i<8; $i++){

                                            echo '<div class="form-check form-check-inline">
                                                  <input class="form-check-input" type="radio" name="radio_notes_1_2" id="radio'.$i.'" value="'.$i.'">
                                                  <label class="form-check-label" for="inlineCheckbox1">'.$i.'</label>
                                                </div>';
                                        }
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>¿Describe claramente cuál es el producto o servicio queofrecerá?</td>
                                    <td>
                                        <?php
                                        for($i=1; $i<8; $i++){
                                            echo '<div class="form-check form-check-inline">
                                                  <input class="form-check-input" type="radio" name="radio_notes_1_3" id="radio'.$i.'" value="'.$i.'">
                                                  <label class="form-check-label" for="inlineCheckbox1">'.$i.'</label>
                                                </div>';
                                        }
                                        ?>
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                    
                  </div>
                </div>
            </div>
            <br>
            <!-- INTERFAZ PREGUNTAS RADIOBUTTONS -->
                <br>
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="check_team" name="b_checks[]" value="val_equipo">
                    <label class="custom-control-label" for="check_team">2.- Si cuenta con un equipo emprendedor, haga una breve reseña de ellos.</label>
                </div>
                <textarea class="form-control" id="bussiness_team" name="bussiness_team" rows="4" required><?= trim($user[0]['equipo']) ?></textarea>
                <!-- INTERFAZ PREGUNTAS RADIOBUTTONS -->
                <div class="container_notes" style="font-size: 12px;">
                  <button class="btn btn-light w-100" type="button" data-toggle="collapse" data-target="#collapse_notas2" aria-expanded="false" aria-controls="collapse_notas2">
                    Abrir criterios y seleccionar nota
                  </button>
                
                <div class="collapse" id="collapse_notas2">
                  <div class="card card-body p-2">

                        <table class="table table_notes">
                            <thead>
                                <tr>
                                    <th class="w-50">Factor</th>
                                    <th>Nota</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>¿Posee un equipo( personas)  emprendedor? (datonumérico) Argumentar</td>
                                    <td>
                                        <?php
                                        for($i=1; $i<8; $i++){
                                            echo '<div class="form-check form-check-inline">
                                                  <input class="form-check-input" type="radio" name="radio_notes_2_1" id="radio'.$i.'" value="'.$i.'">
                                                  <label class="form-check-label" for="inlineCheckbox1">'.$i.'</label>
                                                </div>';
                                        }
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>¿El equipo emprendedor tiene experiencia en emprendimientos? ( si o no , fundamentando respuesta)</td>
                                    <td>
                                        <?php
                                        for($i=1; $i<8; $i++){

                                            echo '<div class="form-check form-check-inline">
                                                  <input class="form-check-input" type="radio" name="radio_notes_2_2" id="radio'.$i.'" value="'.$i.'">
                                                  <label class="form-check-label" for="inlineCheckbox1">'.$i.'</label>
                                                </div>';
                                        }
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>¿Describe funciones y/o roles del equipo emprendedor?</td>
                                    <td>
                                        <?php
                                        for($i=1; $i<8; $i++){
                                            echo '<div class="form-check form-check-inline">
                                                  <input class="form-check-input" type="radio" name="radio_notes_2_3" id="radio'.$i.'" value="'.$i.'">
                                                  <label class="form-check-label" for="inlineCheckbox1">'.$i.'</label>
                                                </div>';
                                        }
                                        ?>
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                    
                  </div>
                </div>
            </div>
            <br>
            <!-- INTERFAZ PREGUNTAS RADIOBUTTONS -->
                <br>
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="check_diferences" name="b_checks[]" value="val_relevancia">
                    <label class="custom-control-label" for="check_diferences">3.- ¿Qué hace su negocio y/o emprendimiento sea relevante o diferente de otros existentes?</label>
                </div>
                <textarea class="form-control" id="bussiness_diferences" name="bussiness_diferences" rows="4" required>  <?= trim($user[0]['relevancia']) ?></textarea>
                <!-- INTERFAZ PREGUNTAS RADIOBUTTONS -->
                <div class="container_notes" style="font-size: 12px;">
                  <button class="btn btn-light w-100" type="button" data-toggle="collapse" data-target="#collapse_notas3" aria-expanded="false" aria-controls="collapse_notas3">
                    Abrir criterios y seleccionar nota
                  </button>
                
                <div class="collapse" id="collapse_notas3">
                  <div class="card card-body p-2">

                        <table class="table table_notes">
                            <thead>
                                <tr>
                                    <th class="w-50">Factor</th>
                                    <th>Nota</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>¿Describe los rasgos diferenciadores de su producto o servicio? Mencione a los menos tres.</td>
                                    <td>
                                        <?php
                                        for($i=1; $i<8; $i++){
                                            echo '<div class="form-check form-check-inline">
                                                  <input class="form-check-input" type="radio" name="radio_notes_3_1" id="radio'.$i.'" value="'.$i.'">
                                                  <label class="form-check-label" for="inlineCheckbox1">'.$i.'</label>
                                                </div>';
                                        }
                                        ?>
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                    
                  </div>
                </div>
            </div>
            <br>
            <!-- INTERFAZ PREGUNTAS RADIOBUTTONS -->
                <br>
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="check_finances" name="b_checks[]" value="val_financiamiento">
                    <label class="custom-control-label" for="check_finances">4.- ¿Qué hará con el financiamiento adjudicado de este Concurso?</label>
                </div>
                <textarea class="form-control" id="bussiness_finances" name="bussiness_finances" rows="4" required><?= trim($user[0]['financiamiento']) ?></textarea>
                <!-- INTERFAZ PREGUNTAS RADIOBUTTONS -->
                <div class="container_notes" style="font-size: 12px;">
                  <button class="btn btn-light w-100" type="button" data-toggle="collapse" data-target="#collapse_notas4" aria-expanded="false" aria-controls="collapse_notas4">
                    Abrir criterios y seleccionar nota
                  </button>
                
                <div class="collapse" id="collapse_notas4">
                  <div class="card card-body p-2">

                        <table class="table table_notes">
                            <thead>
                                <tr>
                                    <th class="w-50">Factor</th>
                                    <th>Nota</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>¿Identifica los costos e inversiones a realizar parala puesta en marcha?</td>
                                    <td>
                                        <?php
                                        for($i=1; $i<8; $i++){
                                            echo '<div class="form-check form-check-inline">
                                                  <input class="form-check-input" type="radio" name="radio_notes_4_1" id="radio'.$i.'" value="'.$i.'">
                                                  <label class="form-check-label" for="inlineCheckbox1">'.$i.'</label>
                                                </div>';
                                        }
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Entrega un presupuesto detallado  (no cotización)de   lo   que   adquirirá   con   el   financiamientosolicitado.</td>
                                    <td>
                                        <?php
                                        for($i=1; $i<8; $i++){

                                            echo '<div class="form-check form-check-inline">
                                                  <input class="form-check-input" type="radio" name="radio_notes_4_2" id="radio'.$i.'" value="'.$i.'">
                                                  <label class="form-check-label" for="inlineCheckbox1">'.$i.'</label>
                                                </div>';
                                        }
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Razones de la adjudicación de lo solicitado para suemprendimiento ( a lo menos tres )</td>
                                    <td>
                                        <?php
                                        for($i=1; $i<8; $i++){
                                            echo '<div class="form-check form-check-inline">
                                                  <input class="form-check-input" type="radio" name="radio_notes_4_3" id="radio'.$i.'" value="'.$i.'">
                                                  <label class="form-check-label" for="inlineCheckbox1">'.$i.'</label>
                                                </div>';
                                        }
                                        ?>
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                    
                  </div>
                </div>
            </div>
            <br>
            <!-- INTERFAZ PREGUNTAS RADIOBUTTONS -->
                <br>
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="check_clients" name="b_checks[]" value="val_clientes">
                    <label class="custom-control-label" for="check_clients">5.- ¿Quiénes son sus potenciales clientes? breve descripción.</label>
                </div>
                <textarea class="form-control" id="bussiness_clients" name="bussiness_clients" rows="4" required><?= trim($user[0]['clientes']) ?></textarea>
                <!-- INTERFAZ PREGUNTAS RADIOBUTTONS -->
                <div class="container_notes" style="font-size: 12px;">
                  <button class="btn btn-light w-100" type="button" data-toggle="collapse" data-target="#collapse_notas5" aria-expanded="false" aria-controls="collapse_notas5">
                    Abrir criterios y seleccionar nota
                  </button>
                
                <div class="collapse" id="collapse_notas5">
                  <div class="card card-body p-2">

                        <table class="table table_notes">
                            <thead>
                                <tr>
                                    <th class="w-50">Factor</th>
                                    <th>Nota</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>¿Describe cualitativamente a los clientes? ¿Quiénes son? ¿Dónde están?</td>
                                    <td>
                                        <?php
                                        for($i=1; $i<8; $i++){
                                            echo '<div class="form-check form-check-inline">
                                                  <input class="form-check-input" type="radio" name="radio_notes_5_1" id="radio'.$i.'" value="'.$i.'">
                                                  <label class="form-check-label" for="inlineCheckbox1">'.$i.'</label>
                                                </div>';
                                        }
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>¿Identifica los canales de comercialización? ¿Cómo pretende llegar y  vender  a sus clientes?</td>
                                    <td>
                                        <?php
                                        for($i=1; $i<8; $i++){

                                            echo '<div class="form-check form-check-inline">
                                                  <input class="form-check-input" type="radio" name="radio_notes_5_2" id="radio'.$i.'" value="'.$i.'">
                                                  <label class="form-check-label" for="inlineCheckbox1">'.$i.'</label>
                                                </div>';
                                        }
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>¿Aporta con datos cuantitativos (valores numéricos aproximados) respecto a su mercado (entregar detalle)?</td>
                                    <td>
                                        <?php
                                        for($i=1; $i<8; $i++){
                                            echo '<div class="form-check form-check-inline">
                                                  <input class="form-check-input" type="radio" name="radio_notes_5_3" id="radio'.$i.'" value="'.$i.'">
                                                  <label class="form-check-label" for="inlineCheckbox1">'.$i.'</label>
                                                </div>';
                                        }
                                        ?>
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                    
                  </div>
                </div>
            </div>
            <br>
            <!-- INTERFAZ PREGUNTAS RADIOBUTTONS -->
                <br>
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="check_status" name="b_checks[]" value="val_estado">
                    <label class="custom-control-label" for="check_status">6.- ¿Cuál es el estado actual de su producto o servicio? Comente.</label>
                </div>
                <textarea class="form-control" id="bussiness_status" name="bussiness_status" rows="4" required><?= trim($user[0]['estado']) ?></textarea>
                <!-- INTERFAZ PREGUNTAS RADIOBUTTONS -->
                <div class="container_notes" style="font-size: 12px;">
                  <button class="btn btn-light w-100" type="button" data-toggle="collapse" data-target="#collapse_notas6" aria-expanded="false" aria-controls="collapse_notas6">
                    Abrir criterios y seleccionar nota
                  </button>
                
                <div class="collapse" id="collapse_notas6">
                  <div class="card card-body p-2">

                        <table class="table table_notes">
                            <thead>
                                <tr>
                                    <th class="w-50">Factor</th>
                                    <th>Nota</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>¿Menciona   el   estado   (inicio,   desarrollo,consolidación, declive, etc.) de su producto oservicio? </td>
                                    <td>
                                        <?php
                                        for($i=1; $i<8; $i++){
                                            echo '<div class="form-check form-check-inline">
                                                  <input class="form-check-input" type="radio" name="radio_notes_6_1" id="radio'.$i.'" value="'.$i.'">
                                                  <label class="form-check-label" for="inlineCheckbox1">'.$i.'</label>
                                                </div>';
                                        }
                                        ?>
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                    
                  </div>
                </div>
            </div>
            <br>
            <!-- INTERFAZ PREGUNTAS RADIOBUTTONS -->
                <br>
                <div class="row">
                  <div class="col-md-7">
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" id="check_complementary" name="b_checks[]" value="val_complemento">
                        <label class="custom-control-label" for="check_complementary">7.- INFORMACIÓN COMPLEMENTARIA</label>
                    </div>
                    <textarea class="form-control" id="bussiness_complementary" name="bussiness_complementary" rows="6" required><?= trim($user[0]['complemento']) ?></textarea>
                  </div>
                  <div class="col-md-5">
                    <span class="span float-left">Archivos complementarios.</span>
                    <br><br>
                      <?php
                        if( file_exists($user[0]['path_complemento1']) ){
                          echo '<a class="btn btn-primary mr-3" href="'.$user[0]['path_complemento1'].'" download>
                                  <i class="fas fa-file-download"></i> 1.- Archivo complementario
                                </a><br><br>';
                        }
                        if( file_exists($user[0]['path_complemento2']) ){
                          echo '<a class="btn btn-primary mr-3" href="'.$user[0]['path_complemento2'].'" download>
                                  <i class="fas fa-file-download"></i> 2.- Archivo complementario
                                </a><br>';
                        }
                        if( !file_exists($user[0]['path_complemento1']) && !file_exists($user[0]['path_complemento2']) ){
                          echo '<p class="small font-italic">No existen archivos complementarios.</p>';
                        }
                      ?>
                  </div>
                </div>
                <!-- INTERFAZ PREGUNTAS RADIOBUTTONS -->
                <div class="container_notes" style="font-size: 12px;">
                  <button class="btn btn-light w-100" type="button" data-toggle="collapse" data-target="#collapse_notas7" aria-expanded="false" aria-controls="collapse_notas7">
                    Abrir criterios y seleccionar nota
                  </button>
                
                <div class="collapse" id="collapse_notas7">
                  <div class="card card-body p-2">

                        <table class="table table_notes">
                            <thead>
                                <tr>
                                    <th class="w-50">Factor</th>
                                    <th>Nota</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>¿Entrega información complementaria relevante  al proyecto? (fotografías, certificados, capacitaciones, ubicación, etc.)</td>
                                    <td>
                                        <?php
                                        for($i=1; $i<8; $i++){
                                            echo '<div class="form-check form-check-inline">
                                                  <input class="form-check-input" type="radio" name="radio_notes_7_1" id="radio'.$i.'" value="'.$i.'">
                                                  <label class="form-check-label" for="inlineCheckbox1">'.$i.'</label>
                                                </div>';
                                        }
                                        ?>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                  </div>
                </div>
            </div>
            <br>
            <!-- INTERFAZ PREGUNTAS RADIOBUTTONS -->


            </div>
        </div>


        <br>



        <div class="card sub_card">
            <div class="card-header">
                <i class="fas fa-file"></i> Documentacion necesaria
            </div>
            <div class="card-body">

                <table class="table">
                    <thead>
                        <tr>
                            <th data-priority="1">CHECK</th>
                            <th data-priority="1">NOMBRE ARCHIVO</th>
                            <th data-priority="1">ACCIONES</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="text-center">
                                <?php
                                $disabled;
                                if( file_exists($user[0]['path_cedula']) ){
                                  echo '<i class="fas fa-check-circle icon_existe"></i>';
                                  $disabled = '';
                                }else{
                                  echo '<i class="fas fa-times-circle icon_noexiste"></i>';
                                  $disabled = 'disabled';
                                }
                               ?>
                            </td>
                            <td>CEDULA DE IDENTIDAD</td>
                            <td>
                              <a class="btn btn-primary mr-3 <?= $disabled; ?> " href="<?= $user[0]['path_cedula'] ?>" download>
                                <i class="fas fa-file-download"></i>
                              </a>
                              <div class="custom-control custom-checkbox" style="display:inline-block;">
                                  <input type="hidden" value="<?= $user[0]['path_cedula'] ?>" name="path_cedula">
                                  <input type="checkbox" <?= $disabled; ?>  class="custom-control-input" id="path_cedula" name="b_checks[]" value="val_path_cedula">
                                  <label class="custom-control-label" for="path_cedula">Eliminar?</label>
                              </div>


                            </td>
                        </tr>
                        <tr>
                            <td class="text-center">
                                <?php
                                if( file_exists($user[0]['path_residencia']) ){
                                  echo '<i class="fas fa-check-circle icon_existe"></i>';
                                  $disabled = '';
                                }else{
                                  echo '<i class="fas fa-times-circle icon_noexiste"></i>';
                                  $disabled = 'disabled';
                                }
                               ?>
                            </td>
                            <td>CERTIFICADO DE RESIDENCIA</td>
                            <td>
                              <a class="btn btn-primary mr-3 <?= $disabled; ?> " href="<?= $user[0]['path_residencia'] ?>" download>
                                <i class="fas fa-file-download"></i>
                              </a>
                              <div class="custom-control custom-checkbox" style="display:inline-block;">
                                  <input type="hidden" value="<?= $user[0]['path_residencia'] ?>" name="path_residencia">
                                  <input type="checkbox" <?= $disabled; ?> class="custom-control-input" id="path_residencia" name="b_checks[]" value="val_path_residencia">
                                  <label class="custom-control-label" for="path_residencia">Eliminar?</label>
                              </div>
                            </td>
                        </tr>
                    </tbody>
                </table>

            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        <i class="fas fa-envelope"></i> Informar al usuario
                    </div>
                    <div class="card-body">
                        <textarea class="form-control informe_email" name="mensaje_error" rows="5" placeholder="Ingrese errores que cometio el usuario"></textarea>
                        <br>
                        <button type="submit" class="btn btn-primary" id="save_n_send" name="action" value="savensend">Enviar correo y Guardar</button>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card border-danger sub_card">
                    <div class="card-header bg-danger text-light">
                        <i class="fas fa-user-slash"></i> Descalificar al usuario
                    </div>
                    <div class="card-body">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="descalificar" name="des_emprendimiento" <?=$checked;?> >
                            <label class="custom-control-label" for="descalificar">Click aqui para descalificar al usuario.</label>
                        </div>

                        <textarea class="form-control informe_email" name="des_textarea" rows="2" placeholder="Ingrese las causas del por que se descalificó al usuario"><?= trim($user[0]['descalificado_causa']) ?></textarea>
                    </div>
                </div>
            </div>
        </div>
        
        <br>
    

        <div class="modal-footer">

            <button type="submit" class="btn btn-primary" id="save_info" name="action" value="save" onClick='return confirmSubmit()'>Guardar</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        </div>
        </form>
    </div>
    <script>
      $(document).ready(function(){
        var val = '<?= $val ?>';
        array = val.split(',');

        for (var i = 0; i < array.length; i++) {
          $('input[value="'+array[i]+'"]:checkbox').prop('checked',true);
        }


        //carga de notas
        var notas = <?php echo json_encode($notas); ?>;

        for (var i = 0; i < notas.length; i++) {
            var id_usuario = notas[i]['id_usuario'];
            var pregunta = notas[i]['pregunta'];
            var criterio = notas[i]['criterio'];
            var nota = notas[i]['nota'];

            $('input#radio'+nota+'[name="radio_notes_'+pregunta+'_'+criterio+'"]').attr('checked','checked');
        }

      });
    </script>
