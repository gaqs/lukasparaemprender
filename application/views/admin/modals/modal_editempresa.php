<?php
$array_val = array();
foreach ($user[0] as $key => $value) {
  $pos = strpos( $key, 'val_' );
  if( $pos == 'false' && $value == '0'){
      array_push($array_val, $key);
  }
}
$val = implode(',',$array_val);
$promedio = promedio_notas($notas);

$checked = '';
if( $user[0]['descalificado'] == 1 ){
    $checked = 'checked';
}

?>
<div class="modal-header">
    <h5 class="modal-title" id="">
        Editar Empresa <b><span class="ml-10">[nota promedio: <div class="d-inline" id="promedio"><?= $promedio; ?></div>]</span></b>
    </h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
   <span aria-hidden="true">&times;</span>
   </button>
</div>
<div class="modal-body">
<form class="needs-validation" novalidate action="admin/edit_empresa" method="post" enctype="multipart/form-data">

    <div class="card sub_card card_db">
        <div class="card-header">
            <i class="fas fa-user-alt"></i> Informacion básica
        </div>
        <div class="card-body">
            <input type="hidden" value="<?= $user[0]['id'] ?>" class="form-control" id="user_id" name="user_id">
            <div class="form-row">
                <div class="form-group col-md-6">
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" id="check_proyect_name"  name="b_checks[]" value="val_nombre" disabled>
                        <label class="custom-control-label" for="check_proyect_name">Nombre proyecto</label>
                    </div>
                    <input type="text" value="<?= $user[0]['nombre'] ?>" class="form-control" id="user_proyect_name" name="user_proyect_name" placeholder="Ingrese nombre del proyecto" value="<?= 'asd '?>" readonly>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-4">
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" id="check_rut" name="checks[]" value="val_rut" disabled>
                        <label class="custom-control-label" for="check_rut">RUT</label>
                    </div>
                    <input type="text" value="<?= $user[0]['rut'] ?>" class="form-control" id="user_rut" name="user_rut" placeholder="RUT" placeholder="" oninput="checkRut(this)" required readonly>
                    <div class="invalid-feedback">RUT incorrecto.</div>
                </div>

                <div class="form-group col-md-4">
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" id="check_nombres" name="checks[]" value="val_nombres">
                        <label class="custom-control-label" for="check_nombres">Nombres</label>
                    </div>
                    <input type="text" value="<?= $user[0]['nombres'] ?>" class="form-control" id="user_name" name="user_name" placeholder="Nombre" required>
                    <div class="invalid-feedback">Ingrese Nombre.</div>
                </div>

                <div class="form-group col-md-4">
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" id="check_apellidos" name="checks[]" value="val_apellidos">
                        <label class="custom-control-label" for="check_apellidos">Apellidos</label>
                    </div>
                    <input type="text" value="<?= $user[0]['apellidos'] ?>" class="form-control" id="user_surname" name="user_surname" placeholder="Apellidos" required>
                    <div class="invalid-feedback">Ingrese Apellidos.</div>
                </div>
            </div>
            <div class="form-group">
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="check_ocupacion" name="checks[]" value="val_ocupacion">
                    <label class="custom-control-label" for="check_ocupacion">Ocupacion</label>
                </div>
                <input type="text" value="<?= $user[0]['ocupacion'] ?>" class="form-control" id="user_job" name="user_job" placeholder="Ocupacion/Profesion" required>
                <div class="invalid-feedback">Ingrese Ocupación.</div>
            </div>
            <div class="form-row">
              <div class="form-group col-md-6">
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="check_empresa" name="checks[]" value="val_empresa">
                    <label class="custom-control-label" for="check_empresa">Empresa</label>
                </div>
                <input type="text" value="<?= $user[0]['empresa'] ?>" class="form-control" id="user_business" name="user_business" placeholder="Nombre de la empresa" size="10" required>
                <div class="invalid-feedback">Ingrese Empresa.</div>
              </div>
              <div class="form-group col-md-6">
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="check_cargo" name="checks[]" value="val_cargo">
                    <label class="custom-control-label" for="check_cargo">Cargo</label>
                </div>
                <input type="text" value="<?= $user[0]['cargo'] ?>" class="form-control" id="cargo" name="user_position" placeholder="Cargo dentro de la empresa" required >
                <div class="invalid-feedback">Ingrese cargo dentro de la empresa.</div>
              </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" id="check_telefono" name="checks[]" value="val_telefono">
                        <label class="custom-control-label" for="check_telefono">Telefono</label>
                    </div>
                    <input type="text" value="<?= $user[0]['telefono'] ?>" class="form-control" id="user_phone" name="user_phone" placeholder="Telefono" size="10" required>
                    <div class="invalid-feedback">Ingrese Teléfono.</div>
                </div>
                <div class="form-group col-md-6">
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" id="check_email" name="checks[]" value="val_email" disabled>
                        <label class="custom-control-label" for="check_email">Email</label>
                    </div>
                    <input type="email" value="<?= $user[0]['email'] ?>" class="form-control" id="user_email" name="user_email" placeholder="Correo electronico" required readonly>
                    <div class="invalid-feedback">Ingrese Email.</div>
                </div>
            </div>
        </div>
    </div>


    <br>


    <div class="card sub_card">
        <div class="card-header">
            <i class="fas fa-user-alt"></i> Datos de la Empresa
        </div>
        <div class="card-body">
            <div class="form-row">
                <div class="form-group col-md-3">
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" id="check_business_rut" name="c_checks[]" value="val_rut_empresa">
                        <label class="custom-control-label" for="check_business_rut">RUT empresa</label>
                    </div>
                    <input type="text" value="<?= $user[0]['rut_empresa'] ?>" class="form-control" id="business_rut" name="business_rut" placeholder="RUT" placeholder="" required>
                    <div class="invalid-feedback">RUT incorrecto.</div>
                </div>

                <div class="form-group col-md-5">
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" id="check_business_name" name="c_checks[]" value="val_razon_social">
                        <label class="custom-control-label" for="check_business_name">Razon Social</label>
                    </div>
                    <input type="text" value="<?= $user[0]['razon_social'] ?>" class="form-control" id="business_name" name="business_name" placeholder="Nombre" required>
                    <div class="invalid-feedback">Ingrese Nombre empresa</div>
                </div>

                <div class="form-group col-md-4">
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" id="check_business_date" name="c_checks[]" value="val_inicio">
                        <label class="custom-control-label" for="check_business_date">Fecha inicio actividades</label>
                    </div>
                    <input type="date" value="<?= $user[0]['inicio'] ?>" class="form-control" id="business_date" name="business_date" placeholder="" required>
                    <div class="invalid-feedback">Ingrese fecha inicio actividades.</div>
                </div>
            </div>
            <div class="form-group">
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="check_business_address" name="c_checks[]" value="val_direccion_empresa">
                    <label class="custom-control-label" for="check_business_address">Direccion</label>
                </div>
                <input type="text" value="<?= $user[0]['direccion_empresa'] ?>" class="form-control" id="business_address" name="business_address" placeholder="Direccion empresa" required>
                <div class="invalid-feedback">Ingrese Direccion.</div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-5">
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" id="check_business_phone" name="c_checks[]" value="val_telefono_empresa">
                        <label class="custom-control-label" for="check_business_phone">Telefono</label>
                    </div>
                    <input type="text" value="<?= $user[0]['telefono_empresa'] ?>" class="form-control" id="business_phone" name="business_phone" placeholder="Telefono empresa" size="10" required>
                    <div class="invalid-feedback">Ingrese Telefono.</div>
                </div>
                <div class="form-group col-md-7">
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" id="check_business_web" name="c_checks[]" value="val_web">
                        <label class="custom-control-label" for="check_business_web">Pagina web</label>
                    </div>
                    <input type="text" value="<?= $user[0]['web'] ?>" class="form-control" id="business_web" name="business_web" placeholder="Pagina web">
                    <div class="invalid-feedback"></div>
                </div>
            </div>
        </div>
    </div>

    <br>


    <div class="card sub_card">
        <div class="card-header">
            <i class="fas fa-briefcase"></i> Informacion relevante al negocio
        </div>
        <div class="card-body">
            <div class="custom-control custom-checkbox">
                <input type="checkbox" class="custom-control-input" id="check_explicacion" name="b_checks[]" value="val_explicacion">
                <label class="custom-control-label" for="check_explicacion">1.- Explique de forma breve a qué se dedica su empresa y cuál es su idea para ella.</label>
            </div>
            <textarea class="form-control" id="bussiness_explication" name="bussiness_explication" rows="4"  required><?= trim($user[0]['explicacion']) ?></textarea>
            <!-- INTERFAZ PREGUNTAS RADIOBUTTONS -->
                <div class="container_notes" style="font-size: 12px;">
                  <button class="btn btn-light w-100" type="button" data-toggle="collapse" data-target="#collapse_notas1" aria-expanded="false" aria-controls="collapse_notas1">
                    Abrir criterios y seleccionar nota
                  </button>
                
                <div class="collapse" id="collapse_notas1">
                    <div class="card card-body p-2">
                        <table class="table table_notes">
                            <thead>
                                <tr>
                                    <th class="w-50">Factor</th>
                                    <th>Nota</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>¿Describe claramente su propuesta de negocio?</td>
                                    <td>
                                        <?php
                                        for($i=1; $i<8; $i++){
                                            echo '<div class="form-check form-check-inline">
                                                  <input class="form-check-input" type="radio" name="radio_notes_1_1" id="radio'.$i.'" value="'.$i.'">
                                                  <label class="form-check-label" for="inlineCheckbox1">'.$i.'</label>
                                                </div>';
                                        }
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>¿Define la oportunidad de negocio? (define problema ysolución)</td>
                                    <td>
                                        <?php
                                        for($i=1; $i<8; $i++){

                                            echo '<div class="form-check form-check-inline">
                                                  <input class="form-check-input" type="radio" name="radio_notes_1_2" id="radio'.$i.'" value="'.$i.'">
                                                  <label class="form-check-label" for="inlineCheckbox1">'.$i.'</label>
                                                </div>';
                                        }
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>¿Describe claramente cuál es el producto o servicio queofrecerá?</td>
                                    <td>
                                        <?php
                                        for($i=1; $i<8; $i++){
                                            echo '<div class="form-check form-check-inline">
                                                  <input class="form-check-input" type="radio" name="radio_notes_1_3" id="radio'.$i.'" value="'.$i.'">
                                                  <label class="form-check-label" for="inlineCheckbox1">'.$i.'</label>
                                                </div>';
                                        }
                                        ?>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <br>
            <!-- INTERFAZ PREGUNTAS RADIOBUTTONS -->
            <br>
            <div class="custom-control custom-checkbox">
                <input type="checkbox" class="custom-control-input" id="check_team" name="b_checks[]" value="val_equipo">
                <label class="custom-control-label" for="check_team">2.- Si cuenta con un equipo emprendedor, haga una breve reseña de ellos</label>
            </div>
            <textarea class="form-control" id="bussiness_team" name="bussiness_team" rows="4"  required><?= trim($user[0]['equipo']) ?></textarea>
            <!-- INTERFAZ PREGUNTAS RADIOBUTTONS -->
                <div class="container_notes" style="font-size: 12px;">
                  <button class="btn btn-light w-100" type="button" data-toggle="collapse" data-target="#collapse_notas2" aria-expanded="false" aria-controls="collapse_notas2">
                    Abrir criterios y seleccionar nota
                  </button>
                
                <div class="collapse" id="collapse_notas2">
                    <div class="card card-body p-2">
                        <table class="table table_notes">
                            <thead>
                                <tr>
                                    <th class="w-50">Factor</th>
                                    <th>Nota</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>¿Posee un equipo (personas) emprendedor? </td>
                                    <td>
                                        <?php
                                        for($i=1; $i<8; $i++){
                                            echo '<div class="form-check form-check-inline">
                                                  <input class="form-check-input" type="radio" name="radio_notes_2_1" id="radio'.$i.'" value="'.$i.'">
                                                  <label class="form-check-label" for="inlineCheckbox1">'.$i.'</label>
                                                </div>';
                                        }
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>¿El equipo emprendedor tiene experiencia en emprendimientos?</td>
                                    <td>
                                        <?php
                                        for($i=1; $i<8; $i++){

                                            echo '<div class="form-check form-check-inline">
                                                  <input class="form-check-input" type="radio" name="radio_notes_2_2" id="radio'.$i.'" value="'.$i.'">
                                                  <label class="form-check-label" for="inlineCheckbox1">'.$i.'</label>
                                                </div>';
                                        }
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>¿Describe funciones y/o roles del equipo?</td>
                                    <td>
                                        <?php
                                        for($i=1; $i<8; $i++){
                                            echo '<div class="form-check form-check-inline">
                                                  <input class="form-check-input" type="radio" name="radio_notes_2_3" id="radio'.$i.'" value="'.$i.'">
                                                  <label class="form-check-label" for="inlineCheckbox1">'.$i.'</label>
                                                </div>';
                                        }
                                        ?>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <br>
            <!-- INTERFAZ PREGUNTAS RADIOBUTTONS -->
            <br>
            <div class="custom-control custom-checkbox">
                <input type="checkbox" class="custom-control-input" id="check_finances" name="b_checks[]" value="val_financiamiento">
                <label class="custom-control-label" for="check_finances">3.- ¿Qué hará con el financiamiento adjudicado de este Concurso?</label>
            </div>
            <textarea class="form-control" id="bussiness_finances" name="bussiness_finances" rows="4"  required>  <?= trim($user[0]['financiamiento']) ?></textarea>
            <!-- INTERFAZ PREGUNTAS RADIOBUTTONS -->
                <div class="container_notes" style="font-size: 12px;">
                  <button class="btn btn-light w-100" type="button" data-toggle="collapse" data-target="#collapse_notas3" aria-expanded="false" aria-controls="collapse_notas3">
                    Abrir criterios y seleccionar nota
                  </button>
                
                <div class="collapse" id="collapse_notas3">
                    <div class="card card-body p-2">
                        <table class="table table_notes">
                            <thead>
                                <tr>
                                    <th class="w-50">Factor</th>
                                    <th>Nota</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>¿Identifica los costos e inversiones a realizar?</td>
                                    <td>
                                        <?php
                                        for($i=1; $i<8; $i++){
                                            echo '<div class="form-check form-check-inline">
                                                  <input class="form-check-input" type="radio" name="radio_notes_3_1" id="radio'.$i.'" value="'.$i.'">
                                                  <label class="form-check-label" for="inlineCheckbox1">'.$i.'</label>
                                                </div>';
                                        }
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Entrega  un presupuesto detallado de lo que adquirirácon el financiamiento.</td>
                                    <td>
                                        <?php
                                        for($i=1; $i<8; $i++){

                                            echo '<div class="form-check form-check-inline">
                                                  <input class="form-check-input" type="radio" name="radio_notes_3_2" id="radio'.$i.'" value="'.$i.'">
                                                  <label class="form-check-label" for="inlineCheckbox1">'.$i.'</label>
                                                </div>';
                                        }
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Razones de la adjudicación de lo solicitado para suemprendimiento (a lo menos tres).</td>
                                    <td>
                                        <?php
                                        for($i=1; $i<8; $i++){
                                            echo '<div class="form-check form-check-inline">
                                                  <input class="form-check-input" type="radio" name="radio_notes_3_3" id="radio'.$i.'" value="'.$i.'">
                                                  <label class="form-check-label" for="inlineCheckbox1">'.$i.'</label>
                                                </div>';
                                        }
                                        ?>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <br>
            <!-- INTERFAZ PREGUNTAS RADIOBUTTONS -->

            <br>
            <div class="custom-control custom-checkbox">
                <input type="checkbox" class="custom-control-input" id="check_clients" name="b_checks[]" value="val_clientes">
                <label class="custom-control-label" for="check_clients">4.- ¿Quiénes son sus potenciales clientes? breve descripción</label>
            </div>
            <textarea class="form-control" id="bussiness_clients" name="bussiness_clients" rows="4"  required><?= trim($user[0]['clientes']) ?></textarea>
            <!-- INTERFAZ PREGUNTAS RADIOBUTTONS -->
                <div class="container_notes" style="font-size: 12px;">
                  <button class="btn btn-light w-100" type="button" data-toggle="collapse" data-target="#collapse_notas4" aria-expanded="false" aria-controls="collapse_notas4">
                    Abrir criterios y seleccionar nota
                  </button>
                
                <div class="collapse" id="collapse_notas4">
                    <div class="card card-body p-2">
                        <table class="table table_notes">
                            <thead>
                                <tr>
                                    <th class="w-50">Factor</th>
                                    <th>Nota</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>¿Describe los clientes? ¿Quiénes son? ¿Dónde están?</td>
                                    <td>
                                        <?php
                                        for($i=1; $i<8; $i++){
                                            echo '<div class="form-check form-check-inline">
                                                  <input class="form-check-input" type="radio" name="radio_notes_4_1" id="radio'.$i.'" value="'.$i.'">
                                                  <label class="form-check-label" for="inlineCheckbox1">'.$i.'</label>
                                                </div>';
                                        }
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>¿Aporta con datos cuantitativos (valores numéricos aproximados) respecto a su mercado (entregar detalle)? </td>
                                    <td>
                                        <?php
                                        for($i=1; $i<8; $i++){

                                            echo '<div class="form-check form-check-inline">
                                                  <input class="form-check-input" type="radio" name="radio_notes_4_2" id="radio'.$i.'" value="'.$i.'">
                                                  <label class="form-check-label" for="inlineCheckbox1">'.$i.'</label>
                                                </div>';
                                        }
                                        ?>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <br>
            <!-- INTERFAZ PREGUNTAS RADIOBUTTONS -->
            <br>
            <div class="custom-control custom-checkbox">
                <input type="checkbox" class="custom-control-input" id="check_commercialization" name="b_checks[]" value="val_comercializacion">
                <label class="custom-control-label" for="check_commercialization">5.- ¿Cuenta con un sistema de comercialización?.</label>
            </div>
            <textarea class="form-control" id="bussiness_commercialization" name="bussiness_commercialization" rows="4"  required><?= trim($user[0]['comercializacion']) ?></textarea>
            <!-- INTERFAZ PREGUNTAS RADIOBUTTONS -->
                <div class="container_notes" style="font-size: 12px;">
                  <button class="btn btn-light w-100" type="button" data-toggle="collapse" data-target="#collapse_notas5" aria-expanded="false" aria-controls="collapse_notas5">
                    Abrir criterios y seleccionar nota
                  </button>
                
                <div class="collapse" id="collapse_notas5">
                    <div class="card card-body p-2">
                        <table class="table table_notes">
                            <thead>
                                <tr>
                                    <th class="w-50">Factor</th>
                                    <th>Nota</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Si o No (argumente respuesta)</td>
                                    <td>
                                        <?php
                                        for($i=1; $i<8; $i++){
                                            echo '<div class="form-check form-check-inline">
                                                  <input class="form-check-input" type="radio" name="radio_notes_5_1" id="radio'.$i.'" value="'.$i.'">
                                                  <label class="form-check-label" for="inlineCheckbox1">'.$i.'</label>
                                                </div>';
                                        }
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>¿Cuáles son los canales de comercialización? Mencionaa los menos tres. </td>
                                    <td>
                                        <?php
                                        for($i=1; $i<8; $i++){

                                            echo '<div class="form-check form-check-inline">
                                                  <input class="form-check-input" type="radio" name="radio_notes_5_2" id="radio'.$i.'" value="'.$i.'">
                                                  <label class="form-check-label" for="inlineCheckbox1">'.$i.'</label>
                                                </div>';
                                        }
                                        ?>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <br>
            <!-- INTERFAZ PREGUNTAS RADIOBUTTONS -->
            <br>
            <div class="custom-control custom-checkbox">
                <input type="checkbox" class="custom-control-input" id="check_competition" name="b_checks[]" value="val_competencia">
                <label class="custom-control-label" for="check_competition">6.- ¿Quiénes son su competencia?.</label>
            </div>
            <textarea class="form-control" id="bussiness_competition" name="bussiness_competition" rows="4"  required><?= trim($user[0]['competencia']) ?></textarea>
            <!-- INTERFAZ PREGUNTAS RADIOBUTTONS -->
                <div class="container_notes" style="font-size: 12px;">
                  <button class="btn btn-light w-100" type="button" data-toggle="collapse" data-target="#collapse_notas6" aria-expanded="false" aria-controls="collapse_notas6">
                    Abrir criterios y seleccionar nota
                  </button>
                
                <div class="collapse" id="collapse_notas6">
                    <div class="card card-body p-2">
                        <table class="table table_notes">
                            <thead>
                                <tr>
                                    <th class="w-50">Factor</th>
                                    <th>Nota</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>¿Describe con claridad quienes son su competencia a nivel local, regional o nacional (según corresponda)?</td>
                                    <td>
                                        <?php
                                        for($i=1; $i<8; $i++){
                                            echo '<div class="form-check form-check-inline">
                                                  <input class="form-check-input" type="radio" name="radio_notes_6_1" id="radio'.$i.'" value="'.$i.'">
                                                  <label class="form-check-label" for="inlineCheckbox1">'.$i.'</label>
                                                </div>';
                                        }
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>¿Identifica ventajas y desventajas sobre su competencia? ( a lo menos dos por cada una) </td>
                                    <td>
                                        <?php
                                        for($i=1; $i<8; $i++){

                                            echo '<div class="form-check form-check-inline">
                                                  <input class="form-check-input" type="radio" name="radio_notes_6_2" id="radio'.$i.'" value="'.$i.'">
                                                  <label class="form-check-label" for="inlineCheckbox1">'.$i.'</label>
                                                </div>';
                                        }
                                        ?>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <br>
            <!-- INTERFAZ PREGUNTAS RADIOBUTTONS -->
            <br>
            <div class="custom-control custom-checkbox">
                <input type="checkbox" class="custom-control-input" id="check_projection" name="b_checks[]" value="val_proyecciones">
                <label class="custom-control-label" for="check_projection">7.- Cuáles son sus Proyecciones de ventas esperadas para el próximo año (2020) , en caso de ser ganador. </label>
            </div>
            <textarea class="form-control" id="bussiness_projection" name="bussiness_projection"  rows="4" required><?= trim($user[0]['proyecciones']) ?></textarea>
            <!-- INTERFAZ PREGUNTAS RADIOBUTTONS -->
                <div class="container_notes" style="font-size: 12px;">
                  <button class="btn btn-light w-100" type="button" data-toggle="collapse" data-target="#collapse_notas7" aria-expanded="false" aria-controls="collapse_notas7">
                    Abrir criterios y seleccionar nota
                  </button>
                
                <div class="collapse" id="collapse_notas7">
                    <div class="card card-body p-2">
                        <table class="table table_notes">
                            <thead>
                                <tr>
                                    <th class="w-50">Factor</th>
                                    <th>Nota</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>¿Señala valores ($) estimados y / o aproximados  deventas a futuro? ( proyección )</td>
                                    <td>
                                        <?php
                                        for($i=1; $i<8; $i++){
                                            echo '<div class="form-check form-check-inline">
                                                  <input class="form-check-input" type="radio" name="radio_notes_7_1" id="radio'.$i.'" value="'.$i.'">
                                                  <label class="form-check-label" for="inlineCheckbox1">'.$i.'</label>
                                                </div>';
                                        }
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Entrega  estado actual de ventas ( anuales y/o mensuales) </td>
                                    <td>
                                        <?php
                                        for($i=1; $i<8; $i++){

                                            echo '<div class="form-check form-check-inline">
                                                  <input class="form-check-input" type="radio" name="radio_notes_7_2" id="radio'.$i.'" value="'.$i.'">
                                                  <label class="form-check-label" for="inlineCheckbox1">'.$i.'</label>
                                                </div>';
                                        }
                                        ?>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <br>
            <!-- INTERFAZ PREGUNTAS RADIOBUTTONS -->
            <br>
            <div class="custom-control custom-checkbox">
                <input type="checkbox" class="custom-control-input" id="check_jobs" name="b_checks[]" value="val_trabajos">
                <label class="custom-control-label" for="check_jobs">8.- ¿Generará nuevos puestos de trabajo en caso de obtener el financiamiento?.</label>
            </div>
            <textarea class="form-control" id="bussiness_jobs" name="bussiness_jobs"  rows="4" required><?= trim($user[0]['trabajos']) ?></textarea>
            <!-- INTERFAZ PREGUNTAS RADIOBUTTONS -->
                <div class="container_notes" style="font-size: 12px;">
                  <button class="btn btn-light w-100" type="button" data-toggle="collapse" data-target="#collapse_notas8" aria-expanded="false" aria-controls="collapse_notas8">
                    Abrir criterios y seleccionar nota
                  </button>
                
                <div class="collapse" id="collapse_notas8">
                    <div class="card card-body p-2">
                        <table class="table table_notes">
                            <thead>
                                <tr>
                                    <th class="w-50">Factor</th>
                                    <th>Nota</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>¿Cuántos puestos de trabajo generaría? (entregue un dato aproximado )</td>
                                    <td>
                                        <?php
                                        for($i=1; $i<8; $i++){
                                            echo '<div class="form-check form-check-inline">
                                                  <input class="form-check-input" type="radio" name="radio_notes_8_1" id="radio'.$i.'" value="'.$i.'">
                                                  <label class="form-check-label" for="inlineCheckbox1">'.$i.'</label>
                                                </div>';
                                        }
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>¿Qué funciones desempeñarían en su empresa los nuevos puestos de trabajo generados?</td>
                                    <td>
                                        <?php
                                        for($i=1; $i<8; $i++){

                                            echo '<div class="form-check form-check-inline">
                                                  <input class="form-check-input" type="radio" name="radio_notes_8_2" id="radio'.$i.'" value="'.$i.'">
                                                  <label class="form-check-label" for="inlineCheckbox1">'.$i.'</label>
                                                </div>';
                                        }
                                        ?>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <br>
            <!-- INTERFAZ PREGUNTAS RADIOBUTTONS -->
            <br>
            <div class="row">
              <div class="col-md-7">
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="check_complementary" name="b_checks[]" value="val_complemento">
                    <label class="custom-control-label" for="check_complementary">9.- INFORMACIÓN COMPLEMENTARIA</label>
                </div>
                <textarea class="form-control" id="bussiness_complementary" name="bussiness_complementary" rows="6" required><?= trim($user[0]['complemento']) ?></textarea>
              </div>
              <div class="col-md-5">
                <span class="span float-left">Archivos complementarios.</span>
                <br><br>
                <?php
                  if( file_exists($user[0]['path_complemento1']) ){
                    echo '<a class="btn btn-primary mr-3" href="'.$user[0]['path_complemento1'].'" download>
                            <i class="fas fa-file-download"></i> 1.- Archivo complementario
                          </a><br><br>';
                  }
                  if( file_exists($user[0]['path_complemento2']) ){
                    echo '<a class="btn btn-primary mr-3" href="'.$user[0]['path_complemento2'].'" download>
                            <i class="fas fa-file-download"></i> 2.- Archivo complementario
                          </a><br>';
                  }
                  if( !file_exists($user[0]['path_complemento1']) && !file_exists($user[0]['path_complemento2']) ){
                    echo '<p class="small font-italic">No existen archivos complementarios.</p>';
                  }
                ?>
              </div>
            </div>
            <!-- INTERFAZ PREGUNTAS RADIOBUTTONS -->
                <div class="container_notes" style="font-size: 12px;">
                  <button class="btn btn-light w-100" type="button" data-toggle="collapse" data-target="#collapse_notas9" aria-expanded="false" aria-controls="collapse_notas9">
                    Abrir criterios y seleccionar nota
                  </button>
                
                <div class="collapse" id="collapse_notas9">
                    <div class="card card-body p-2">
                        <table class="table table_notes">
                            <thead>
                                <tr>
                                    <th class="w-50">Factor</th>
                                    <th>Nota</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>¿Entrega información complementaria relevante al proyecto? (fotografías, certificados, capacitaciones, etc.)</td>
                                    <td>
                                        <?php
                                        for($i=1; $i<8; $i++){
                                            echo '<div class="form-check form-check-inline">
                                                  <input class="form-check-input" type="radio" name="radio_notes_9_1" id="radio'.$i.'" value="'.$i.'">
                                                  <label class="form-check-label" for="inlineCheckbox1">'.$i.'</label>
                                                </div>';
                                        }
                                        ?>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <br>
            <!-- INTERFAZ PREGUNTAS RADIOBUTTONS -->

        </div>
    </div>


    <br>



    <div class="card sub_card">
        <div class="card-header">
            <i class="fas fa-file"></i> Documentacion necesaria
        </div>
        <div class="card-body">

            <table class="table">
                <thead>
                    <tr>
                        <th data-priority="1">CHECK</th>
                        <th data-priority="1">NOMBRE ARCHIVO</th>
                        <th data-priority="1">ACCIONES</th>
                    </tr>
                </thead>
                <tbody>
                  <tr>
                      <td class="text-center">
                          <?php
                          $disabled;
                          if( file_exists($user[0]['path_cedula']) ){
                            echo '<i class="fas fa-check-circle icon_existe"></i>';
                            $disabled = '';
                          }else{
                            echo '<i class="fas fa-times-circle icon_noexiste"></i>';
                            $disabled = 'disabled';
                          }
                         ?>
                      </td>
                      <td>CEDULA DE IDENTIDAD</td>
                      <td>
                        <a class="btn btn-primary mr-3 <?= $disabled; ?> " href="<?= $user[0]['path_cedula'] ?>" download>
                          <i class="fas fa-file-download"></i>
                        </a>
                        <div class="custom-control custom-checkbox" style="display:inline-block;">
                            <input type="hidden" value="<?= $user[0]['path_cedula'] ?>" name="path_cedula">
                            <input type="checkbox" <?= $disabled; ?>  class="custom-control-input" id="path_cedula" name="b_checks[]" value="val_path_cedula">
                            <label class="custom-control-label" for="path_cedula">Eliminar?</label>
                        </div>


                      </td>
                  </tr>
                  <tr>
                      <td class="text-center">
                          <?php
                          if( file_exists($user[0]['path_rol']) ){
                            echo '<i class="fas fa-check-circle icon_existe"></i>';
                            $disabled = '';
                          }else{
                            echo '<i class="fas fa-times-circle icon_noexiste"></i>';
                            $disabled = 'disabled';
                          }
                         ?>
                      </td>
                      <td>ROL ÚNICO TRIBUTARIO</td>
                      <td>
                        <a class="btn btn-primary mr-3 <?= $disabled; ?> " href="<?= $user[0]['path_rol'] ?>" download>
                          <i class="fas fa-file-download"></i>
                        </a>
                        <div class="custom-control custom-checkbox" style="display:inline-block;">
                            <input type="hidden" value="<?= $user[0]['path_rol'] ?>" name="path_rol">
                            <input type="checkbox" <?= $disabled; ?> class="custom-control-input" id="path_rol" name="b_checks[]" value="val_path_rol">
                            <label class="custom-control-label" for="path_rol">Eliminar?</label>
                        </div>
                      </td>
                  </tr>

                  <tr>
                      <td class="text-center">
                          <?php
                          if( file_exists($user[0]['path_renta']) ){
                            echo '<i class="fas fa-check-circle icon_existe"></i>';
                            $disabled = '';
                          }else{
                            echo '<i class="fas fa-times-circle icon_noexiste"></i>';
                            $disabled = 'disabled';
                          }
                         ?>
                      </td>
                      <td>DECLARACIÓN DE IMPUESTO A LA RENTA</td>
                      <td>
                        <a class="btn btn-primary mr-3 <?= $disabled; ?> " href="<?= $user[0]['path_renta'] ?>" download>
                          <i class="fas fa-file-download"></i>
                        </a>
                        <div class="custom-control custom-checkbox" style="display:inline-block;">
                            <input type="hidden" value="<?= $user[0]['path_renta'] ?>" name="path_renta">
                            <input type="checkbox" <?= $disabled; ?> class="custom-control-input" id="path_renta" name="b_checks[]" value="val_path_renta">
                            <label class="custom-control-label" for="path_renta">Eliminar?</label>
                        </div>
                      </td>
                  </tr>

                  <tr>
                      <td class="text-center">
                          <?php
                          if( file_exists($user[0]['path_patente']) ){
                            echo '<i class="fas fa-check-circle icon_existe"></i>';
                            $disabled = '';
                          }else{
                            echo '<i class="fas fa-times-circle icon_noexiste"></i>';
                            $disabled = 'disabled';
                          }
                         ?>
                      </td>
                      <td>PATENTE MUNICIPAL</td>
                      <td>
                        <a class="btn btn-primary mr-3 <?= $disabled; ?> " href="<?= $user[0]['path_patente'] ?>" download>
                          <i class="fas fa-file-download"></i>
                        </a>
                        <div class="custom-control custom-checkbox" style="display:inline-block;">
                            <input type="hidden" value="<?= $user[0]['path_patente'] ?>" name="path_patente">
                            <input type="checkbox" <?= $disabled; ?> class="custom-control-input" id="path_patente" name="b_checks[]" value="val_path_patente">
                            <label class="custom-control-label" for="path_patente">Eliminar?</label>
                        </div>
                      </td>
                  </tr>

                  <tr>
                      <td class="text-center">
                          <?php
                          if( file_exists($user[0]['path_otrospermisos']) ){
                            echo '<i class="fas fa-check-circle icon_existe"></i>';
                            $disabled = '';
                          }else{
                            echo '<i class="fas fa-times-circle icon_noexiste"></i>';
                            $disabled = 'disabled';
                          }
                         ?>
                      </td>
                      <td>OTROS PERMISOS DE OPERACIÓN</td>
                      <td>
                        <a class="btn btn-primary mr-3 <?= $disabled; ?> " href="<?= $user[0]['path_otrospermisos'] ?>" download>
                          <i class="fas fa-file-download"></i>
                        </a>
                        <div class="custom-control custom-checkbox" style="display:inline-block;">
                            <input type="hidden" value="<?= $user[0]['path_otrospermisos'] ?>" name="path_otrospermisos">
                            <input type="checkbox" <?= $disabled; ?> class="custom-control-input" id="path_otrospermisos" name="b_checks[]" value="val_path_otrospermisos">
                            <label class="custom-control-label" for="path_otrospermisos">Eliminar?</label>
                        </div>
                      </td>
                  </tr>

                  <tr>
                      <td class="text-center">
                          <?php
                          if( file_exists($user[0]['path_boleta']) ){
                            echo '<i class="fas fa-check-circle icon_existe"></i>';
                            $disabled = '';
                          }else{
                            echo '<i class="fas fa-times-circle icon_noexiste"></i>';
                            $disabled = 'disabled';
                          }
                         ?>
                      </td>
                      <td>BOLETA DE AGUA, LUZ O GAS</td>
                      <td>
                        <a class="btn btn-primary mr-3 <?= $disabled; ?> " href="<?= $user[0]['path_boleta'] ?>" download>
                          <i class="fas fa-file-download"></i>
                        </a>
                        <div class="custom-control custom-checkbox" style="display:inline-block;">
                            <input type="hidden" value="<?= $user[0]['path_boleta'] ?>" name="path_boleta">
                            <input type="checkbox" <?= $disabled; ?> class="custom-control-input" id="path_boleta" name="b_checks[]" value="val_path_boleta">
                            <label class="custom-control-label" for="path_boleta">Eliminar?</label>
                        </div>
                      </td>
                  </tr>
                </tbody>
            </table>

        </div>
    </div>
    <br>
    <div class="row">
            <div class="col-md-6">
                    <div class="card">
                        <div class="card-header">
                            <i class="fas fa-file"></i> Informar al usuario
                        </div>
                        <div class="card-body">
                            <textarea class="form-control informe_email" name="mensaje_error" rows="5" placeholder="Ingrese errores que cometio el usuario"></textarea>
                            <br>
                            <button type="submit" class="btn btn-primary" id="save_n_send" name="action" value="savensend">Enviar correo y Guardar</button>
                        </div>
                    </div>
            </div>
            <div class="col-md-6">
                <div class="card border-danger sub_card">
                    <div class="card-header bg-danger text-light">
                        <i class="fas fa-user-slash"></i> Descalificar al usuario
                    </div>
                    <div class="card-body">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="descalificar" name="des_empresa" <?=$checked;?>>
                            <label class="custom-control-label" for="descalificar">Click aqui para descalificar al usuario.</label>
                        </div>

                        <textarea class="form-control informe_email" name="des_textarea" rows="2" placeholder="Ingrese las causas del por que se descalificó al usuario"><?= trim($user[0]['descalificado_causa']) ?></textarea>
                    </div>
                </div>
            </div>
        </div>

        <br>

    <div class="modal-footer">

      <button type="submit" class="btn btn-primary" id="save_info" name="action" value="save" onClick='return confirmSubmit()'>Guardar</button>
      <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
    </div>
  </form>
</div>
<script>
  $(document).ready(function(){
    var val = '<?= $val ?>';
    array = val.split(',');

    for (var i = 0; i < array.length; i++) {
      $('input[value="'+array[i]+'"]:checkbox').prop('checked',true);
    }

     //carga de notas
        var notas = <?php echo json_encode($notas); ?>;

        for (var i = 0; i < notas.length; i++) {
            var id_usuario = notas[i]['id_usuario'];
            var pregunta = notas[i]['pregunta'];
            var criterio = notas[i]['criterio'];
            var nota = notas[i]['nota'];

            $('input#radio'+nota+'[name="radio_notes_'+pregunta+'_'+criterio+'"]').attr('checked','checked');
        }


  });
</script>
