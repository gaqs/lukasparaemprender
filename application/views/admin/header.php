<!doctype html>
<html lang="en">
  <head>
    <!-- Favicon and touch icons -->
    <link rel="shortcut icon" href="favicon.png">

    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Luka$ para emprender</title>

    <!-- CSS -->
    <link rel="stylesheet" href="<?= base_url();?>bootstrap/css/bootstrap.css" >
    <link rel="stylesheet" href="<?= base_url();?>fontawesome/css/all.css" >
    <link rel="stylesheet" href="<?= base_url();?>css/admin/style.css" >
    <!-- DataTables CSS -->
    <link rel="stylesheet" type="text/css" href="<?= base_url();?>css/admin/datatables.css"/>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Optional JavaScript -->
    <script src="<?= base_url();?>js/jquery-3.3.1.js"></script>
    <script src="<?= base_url();?>bootstrap/js/bootstrap.bundle.js"></script>
    <script src="<?= base_url();?>js/bs-custom-file-input.js"></script>
    <script src="<?= base_url();?>js/admin/jscript.js"></script>
    <!-- DataTables JS -->
    <script type="text/javascript" src="<?= base_url();?>js/admin/datatables.js"></script>

    <!-- dev URL -->
    <script src="<?= base_url();?>js/admin/base64.js"></script>
    <script src="<?= base_url();?>js/admin/jquery.table2excel.js"></script>


    <script>
    $(document).ready(function(){

      $(function () {
        $('[data-toggle="tooltip"]').tooltip({
          html: true
        });
        $('[data-tt="tooltip"]').tooltip({
          html: true
        });

      });
    });

    </script>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-137432398-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-137432398-1');
    </script>



  </head>
  <body>

    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
      <div class="container">
      <a class="navbar-brand" href="#">
          <b>Sistema de Administración</b>

    <?php
    $admin_name = $this->session->userdata('name');
    if( isset( $admin_name )) {
      $toggled="";
    }else{
      $toggled='toggled';
    }
    ?>

    <div id="wrapper" class="<?= $toggled;?>">
    <!-- Sidebar -->
    <div id="sidebar-wrapper" class="text-center">
        <ul class="sidebar-nav">
          <!--
            <li>
              <b class="text-muted">Sistema de Administracion</b>
            </li>
          -->
            <br>

              <strong style="font-size:1.25rem;">Gustavo Quilodrán</strong>
              <br>
              <em class="text-muted">gaqs.02@gmail.com</em>
              <br>
              <small class="text-muted">-- Administrador</small>
              <br>

            <br>
            <a class="btn btn-danger" href="<?= base_url();?>login/destroy">Logout</a>
            <hr>
            <div class="date_config_wrapper">
              <li><a href="<?=base_url();?>admin"><i class="fas fa-home"></i> Página inicio</a></li>
            </div>
            <hr>
            <div class="date_config_wrapper">
              <h5 style="text-indent: 20px;"><b>General</b></h5>
              <li><a href="<?=base_url();?>export/notas_emprendimiento"><i class="fas fa-table"></i> Notas Emprendimientos</a></li>
              <li><a href="<?=base_url();?>export/notas_empresa"><i class="fas fa-table"></i> Notas Empresas</a></li>
              <li><a href="#" ><i class="fas fa-calendar-day"></i> Configuración fechas</a></li>
            </div>

        </ul>
    </div>
    <!-- /#sidebar-wrapper -->

    <!-- Page Content -->
        <div id="page-content-wrapper">






<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <div class="container">

    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
<<<<<<< HEAD
    <div class="collapse navbar-collapse navbar-right" id="navbarNav">
      <ul class="navbar-nav ml-auto">
        <li class="nav-item active">
          <a class="nav-link" href="<?=  base_url(); ?>">Página principal</a>
        </li>
        <li class="nav-item active">
          <a class="nav-link" href="<?=  base_url(); ?>admin">9° Concurso</a>
        </li>
        <li class="nav-item active">
          <a class="nav-link" href="<?=  base_url(); ?>admin/innovacion">Innovación social</a>
        </li>
=======

    <div class="collapse navbar-collapse" id="navbarNav">
      <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
>>>>>>> registro
        <li class="nav-item dropdown">
          <?php
          $admin_name = $this->session->userdata('name');
          if( isset( $admin_name )) {
            echo '<a href="#menu-toggle" class="btn btn-primary" id="menu-toggle">
              <i class="fas fa-bars"></i>
            </a>';
          }
          ?>
        </li>
        <li class="nav-item active">
          <a class="nav-link" href="#">Sistema de Administración</a>
        </li>
      </ul>
    </div>
  </div>
</nav>
