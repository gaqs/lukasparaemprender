<style type="text/css">
  .form-group .form-control-feedback {
    position: absolute;
    z-index: 2;
    display: block;
    width: 2.375rem;
    height: 2.375rem;
    line-height: 2.375rem;
    text-align: center;
    pointer-events: none;
    color: #aaa;
}
.form-group input{
  padding-left: 40px;
}

  
</style>
<?php $this->load->helper('cookie'); ?>
<div class="container fix_header">
  <div class="row justify-content-center">
    <div class="col-md-5">
      <div class="card">
        <div class="card-body">
      <form class="needs-validation" action="<?= base_url();?>login/access" method="post" style="width:100%;"  novalidate >
      <h1 class="h3 mb-3">Iniciar sesión <b>Administrador</b></h1>
      <div class="form-group">
        <label for="user_email">Correo electrónico</label>
        <span class="fas fa-at form-control-feedback"></span>
        <input type="email" class="form-control" id="user_email" placeholder="nombre@dominio.com" name="admin_email" value="<?php if (get_cookie('aemail')) { echo get_cookie('aemail'); } ?>" required>
        <div class="invalid-feedback">Ingrese correo electrónico</div>
      </div>
      <div class="form-group">
        <label for="user_email">Contraseña</label>
        <span class="fas fa-key form-control-feedback"></span>
        <input type="password" class="form-control" id="user_pass" name="admin_pass" placeholder="********" value="<?php if (get_cookie('uemail')) { echo get_cookie('aemail'); } ?>" required>
        <div class="invalid-feedback">Ingrese contraseña</div>
      </div>
      <div class="custom-control custom-checkbox mb-3">
          <input type="checkbox" class="custom-control-input" id="user_remember" name="admin_remember" <?php if (get_cookie('aremember')) { echo 'checked'; } ?> >
          <label class="custom-control-label" for="user_remember">Recordarme</label>
      </div>
      <button type="submit" class="btn btn-primary">Ingresar <i class="fas fa-sign-in-alt"></i></button>
        <p class="mt-5 mb-3 text-muted">&copy; Municipalidad de Puerto Montt 2019</p>
      </form>
    </div>
  </div>
    </div>
  </div>
</div>
