<div class="container-flex admin_container">
        <table id="users_table" class="table-striped table-bordered" style="width:100%;">
            <thead>
                <tr>
                    <th data-priority="1">Id</th>
                    <th data-priority="1">Tipo</th>
                    <th data-priority="1">RUT</th>
                    <th data-priority="1">Nombres</th>
                    <th data-priority="1">Apellidos</th>
                    <th data-priority="1">Correo</th>
                    <th data-priority="2">Ingreso</th>
                    <th data-priority="2">Edición</th>
                    <th data-priority="1">Acción</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    foreach ($usuarios as $row) {
                        echo '<tr>
                                <td class="first_row">'.$row['id'].'</td>
                                <td>';
                                if( $row['tipo_usuario'] == 0 ){
                                    echo 'Emprendimiento';
                                }else{
                                    echo 'Empresa';
                                }
                        echo  '</td>
                                <td>'.$row['rut'].'</td>
                                <td>'.$row['nombres'].'</td>
                                <td>'.$row['apellidos'].'</td>
                                <td>'.$row['email'].'</td>
                                <td>'.$row['insert_date'].'</td>
                                <td>'.$row['edit_date'].'</td>
                                <td class="td_accion">
                                  <div class="btn-group" role="group" aria-label="">';
                                  if( $row['tipo_usuario'] == 0 ){ //empresario o empresa
                                      echo '<button type="button" class="btn btn-primary btn_db" id="edit_user" value="'.$row['id'].'" data-toggle="tooltip" data-placement="top" title="<em>Editar usuario</em>"><i class="fas fa-edit"></i></button>
                                      <a href="'.base_url().'export/export_user?id='.$row['id'].'" target="_blank" class="btn btn-success btn_db" id="export_user" data-toggle="tooltip" data-placement="top" title="<em>Exportar a PDF</em>"><i class="fas fa-file-export"></i></a>';
                                  }else{
                                      echo '<button type="button" class="btn btn-primary btn_db" id="edit_empresa" value="'.$row['id'].'" data-toggle="tooltip" data-placement="top" title="<em>Editar usuario</em>"><i class="fas fa-edit"></i></button><a href="'.base_url().'export/export_empresa?id='.$row['id'].'" target="_blank" class="btn btn-success btn_db" id="export_user" data-toggle="tooltip" data-placement="top" title="<em>Exportar a PDF</em>"><i class="fas fa-file-export"></i></a>';
                                  }
                                  echo '<button class="btn btn-danger btn_db" data-whatever="'.$row['id'].'" data-toggle="modal" data-target="#delete_modal" data-tt="tooltip" data-placement="top" title="<em>Eliminar usuario</em>"><i class="fas fa-trash-alt"></i></button>
                                </div>
                              </td>
                            </tr>';
                    }
                ?>
            </tbody>
        </table>


    <!-- Modal -->
    <div class="modal fade" id="db_modal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content"></div>
        </div>
    </div>

    <div class="modal fade" id="correct_modal" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">

            <h5 class="modal-title">Editar Usuario</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <p>Usuario editado correctamente.</p>
          </div>
          <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal" onClick="window.location.reload()">Cerrar</button>

          </div>
        </div>
      </div>
    </div>


    <!-- Modal de eliminar usuario -->
     <div class="modal fade" id="delete_modal" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Eliminar Usuario</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <form action="admin/delete_user" method="post">
          <div class="modal-body">
            <p>¿Esta seguro que desea eliminar este Usuario?</p>
            <input type="hidden" class="form-control" id="recipient-name" name="user_id">
          </div>
          <div class="modal-footer">
            <input type="submit" href="" class="btn btn-primary btn_delete" value="Eliminar">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
          </div>
        </form>
        </div>
      </div>
    </div>

</div>
