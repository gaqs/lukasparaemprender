#########################
#######DEPRECATED########
#########################

<?php $this->load->helper('cookie'); ?>
<div class="container fix_header">
  <div class="row justify-content-center">
    <div class="col-md-5">
      <div class="card">
        <div class="card-body">
      <form class="needs-validation" action="<?= base_url();?>login/inn_access" method="post" style="width:100%;"  novalidate >
      <h1 class="h3 mb-3">Iniciar sesión<br><b>Desafío innovación</b></h1>
      <div class="form-group">
        <label for="user_email">Correo electrónico</label>
        <span class="fas fa-at form-control-feedback"></span>
        <input type="email" class="form-control" id="registered_email" placeholder="nombre@dominio.com" name="email" required value="<?php if (get_cookie('inn_email')) { echo get_cookie('inn_email'); } ?>">
        <div class="invalid-feedback">Ingrese correo electrónico</div>
      </div>
      <div class="form-group">
        <label for="user_email">Contraseña</label>
        <span class="fas fa-key form-control-feedback"></span>
        <input type="password" class="form-control" id="user_pass" placeholder="Contraseña" name="pass" required value="<?php if (get_cookie('inn_password')) { echo get_cookie('inn_password'); } ?>">
        <div class="invalid-feedback">Ingrese contraseña</div>
      </div>
      <div class="custom-control custom-checkbox mb-3">
          <input type="checkbox" class="custom-control-input" id="user_remember" name="remember" <?php if (get_cookie('inn_remember')) { echo 'checked'; } ?>>
          <label class="custom-control-label" for="user_remember">Recordarme</label>
      </div>
      <button type="submit" class="btn btn-primary">Iniciar sesión <i class="fas fa-sign-in-alt"></i></button>
        <p class="mt-5 mb-3 text-muted">&copy; Municipalidad de Puerto Montt 2019</p>
      </form>
    </div>
  </div>
    </div>
  </div>
</div>
