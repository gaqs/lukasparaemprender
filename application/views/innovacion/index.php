<?php $session=$this->session->userdata('user_name'); $disabled = ''; if( isset($session) ){ $disabled = 'disabled'; } ?>
<div class="back inn_back"></div> <!-- background slider y menu header -->
<div class="aviso text-center">Página en periodo de<br>prueba y evaluación</div>


<div class="container-fluid p-0 mt-5">

	<div id="slider_header" class="carousel slide" data-ride="carousel">
		<ol class="carousel-indicators">
			<li data-target="#slider_header" data-slide-to="0" class="active"></li>
			<li data-target="#slider_header" data-slide-to="1"></li>
			<li data-target="#slider_header" data-slide-to="2"></li>
		</ol>
		<div class="carousel-inner">
			<div class="carousel-item active">
				<div class="d-block w-100 slider_item" alt="...">
					<div class="row mx-auto">

							<div class="col-md-4 d-flex justify-content-end">
								<img src="<?= base_url();?>image/inn_lukitas_border.png" class="w-100 logo">
							</div>
							<div class="col-md-8 d-inline align-self-center"> <span class="line_1">DESAFÍO INNOVACIÓN SOCIAL</span>
								<br> <span class="line_2">Luka$ para Emprender</span>
								<br> <span class="line_3">2019</span>
								<hr class="line_separator">
								<span class="line_4">Subdirección de Desarrollo Económico Local</span>
								<br> <span class="line_4">Dirección de Desarrollo Comunitario</span>
							</div>

					</div>
				</div>
			</div>
      <div class="carousel-item">
				<div class="d-block w-100 slider_item" alt="...">
					<div class="row mx-auto seleccion" style="display: inherit;text-align: center;padding-top: 150px;">
						<h3 class="text-white"><b>02 de Setiembre del 2019</b></h3>
						<h1>POSTULACIÓN DESAFÍO DE INNOVACIÓN SOCIAL</h1>

						<a href="<?= base_url(); ?>innovacion/form" class="btn btn-primary btn-lg p_none">
              <i class="fas fa-external-link-alt"></i> Ingresar al formulario
            </a>
						<br>
					</div>
				</div>
			</div>
      <!--
			<div class="carousel-item">
				<div class="d-block w-100 slider_item" alt="...">
					<div class="row mx-auto seleccion" style="display: inherit;text-align: center;padding-top: 150px;">
						<h3 class="text-white"><b>11 de Junio del 2019</b></h3>
						<h1>RESULTADOS PRE SELECCIÓN!</h1>

						<a href="<?= base_url(); ?>files/resultados/Preseleccionados_LukasparaEmprender_2019.xlsx" target="_blank" class="btn btn-success btn-lg p_none"><i class="fas fa-file-excel"></i> VER RESULTADOS PRESELECCIÓN</a>
						<br>
						<small class="text-light">(*) Resultados disponibles el 11 de Junio del 2019</small>
					</div>
				</div>
			</div>

			<div class="carousel-item">
				<div class="d-block w-100 slider_item" alt="...">
					<div class="row mx-auto seleccion" style="display: inherit;text-align: center;padding-top: 150px;">
						<h3 class="text-white"><b>31 de Julio del 2019</b></h3>
						<h1>GANADORES DEL CONCURSO!</h1>

						<a href="<?= base_url(); ?>files/resultados/Ganadores_Categoria_Emprendimiento_Empresa_lukas_2019.pdf" target="_blank" class="btn btn-primary btn-lg p_none disabled"><i class="fas fa-file-pdf"></i> VER LISTADO GANADORES</a>
						<br>
						<small class="text-light">(*) Resultados disponibles el 31 de Julio del 2019</small>
					</div>
				</div>
			</div>

			<div class="carousel-item">
				<div class="d-block w-100 slider_item" alt="..."></div>
			</div>
			-->

		</div>
		<a class="carousel-control-prev" href="#slider_header" role="button" data-slide="prev">
			<span class="carousel-control-prev-icon" aria-hidden="true"></span>
			<span class="sr-only">Previous</span>
		</a>
		<a class="carousel-control-next" href="#slider_header" role="button" data-slide="next">
			<span class="carousel-control-next-icon" aria-hidden="true"></span>
			<span class="sr-only">Next</span>
		</a>
	</div>
</div>

<div class="container mb-5" style="margin-top:100px;">
  <h2 class="text-center"><b>PRÓXIMAS</b> FECHAS</h2>
  <br><br><br>
  <div class="card-group">
    <div class="card d-block text-center border-0">
      <span class="fa-stack fa-4x">
        <i class="fa fa-circle fa-stack-2x icon_background"></i>
        <i class="fa fa-hourglass-start fa-stack-1x icon_center"></i>
      </span>
      <div class="card-body">
        <h5 class="card-title"><b>Lanzamiento postulación</b></h5>
        <hr>
        <p class="card-text">Se realizará el día <b>02 de Septiembre</b>, y se hara por este mismo medio. Si desea postular haga <a href="<?= base_url(); ?>innovacion/form" target="_blank">click aquí</a>.</p>
        <p class="card-text"><small class="text-muted"></small></p>
      </div>
    </div>
    <div class="card d-block text-center border-0">
      <span class="fa-stack fa-4x">
        <i class="fa fa-circle fa-stack-2x icon_background"></i>
        <i class="fa fa-times-circle fa-stack-1x icon_center"></i>
      </span>
      <div class="card-body">
        <h5 class="card-title"><b>Cierre postulación</b></h5>
        <hr>
        <p class="card-text">Previamente se realizarán charlas en instituciones de educación Superior, el cierre será el dia <b>16 de Octubre</b>.</p>
        <p class="card-text"><small class="text-muted"></small></p>
      </div>
    </div>
    <div class="card d-block text-center border-0">
      <span class="fa-stack fa-4x">
        <i class="fa fa-circle fa-stack-2x icon_background"></i>
        <i class="fa fa-list-ol fa-stack-1x icon_center"></i>
      </span>
      <div class="card-body">
        <h5 class="card-title"><b>Listado seleccionados</b></h5>
        <hr>
        <p class="card-text">Se comunicarán los seleccionados por este mismo medio, el día <b>22 de Octubre</b>.</p>
        <p class="card-text"><small class="text-muted"></small></p>
      </div>
    </div>
    <div class="card d-block text-center border-0">
      <span class="fa-stack fa-4x">
        <i class="fa fa-circle fa-stack-2x icon_background"></i>
        <i class="fas fa-trophy fa-stack-1x icon_center"></i>
      </span>
      <div class="card-body">
        <h5 class="card-title"><b>Resultado Ganadores</b></h5>
        <hr>
        <p class="card-text">Luego de la presentacion de los ganadores, el resultado se presentara el <b>15 de Noviembre</b>.</p>
        <p class="card-text"><small class="text-muted"></small></p>
      </div>
    </div>
  </div>
</div>
</div>

<div class="container"><hr></div>

<div class="container text-align mt-5 mb-5">
  <h2 class="text-center mb-5 mt-5"><b>FORMULARIO </b>DE INSCRIPCIÓN</h2>
  <br>

	<div class="row" id="concurso">
		<div class="col-md-3">
			<div class="alert alert-warning card_container" role="alert">
	      <h3 class="alert-heading"><b><i class="fas fa-book-reader"></i> PASO 1</b></h3>
	      <h4>Leer las bases del desafío</h4>
	    </div>
		</div>
		<div clasS="col-md-9 bg-light p-4 rounded">
			<h4>Bases del Desafío</h4>
      <hr>
			<p>Bases de postulación para desafío de innovación social <b>Luka$ para Emprender</b>. Puerto Montt, Agosto 2019.</p>
				<a href="<?= base_url(); ?>files/BasesDesafioInnovacion2019.pdf" target="_blank" class="btn btn-primary" style="width:300px;"><i class="fas fa-download"></i> Ver bases</a>
		</div>
	</div>
<br>

<div class="row" id="concurso">
  <div class="col-md-3">
    <div class="alert alert-warning card_container" role="alert">
      <h3 class="alert-heading"><b><i class="fas fa-align-justify"></i> PASO 2</b></h3>
      <h4>Llenar formulario de inscripción</h4>
    </div>
  </div>
  <div clasS="col-md-9 bg-light p-4 rounded">
    <h4>Formulario desafío innovación</h4>
    <hr>
    <p>Proceda a llenar los todos los campos del formulario, para participar en el desafío de innovación social.</p>
      <a href="<?= base_url(); ?>innovacion/form" class="btn btn-primary" style="width:300px;"><i class="fas fa-external-link-alt"></i> Ingresar</a>

  </div>
</div>
</div>

<div class="container"><hr></div>

<div class="container text-align mt-5 mb-5" id="resultados">
  <h2 class="text-center mb-5 mt-5"><b>RESULTADOS</b> DESAFÍO INNOVACIÓN SOCIAL</h2>
  <footer class="blockquote-footer text-center"><cite title="Source Title">Próximamente.</cite></footer>

  <div class="container text-align mt-5 mb-5 text-center">
  	<div class="row">
  		<div class="col-md-12 bg-light p-4 rounded">
  			<h4>Comunicado de los <b>seleccionados</b>.</h4>
  			<hr>
  			<p>
  				*Los pre-seleccionados del desafío serán contactados vía <b><u>correo electrónico</u></b> para mayor información.
  			</p>
  			<a href="#" target="_blank" class="btn btn-primary p_none disabled" style="width:300px;"><i class="fas fa-file-pdf"></i>  Ver listado seleccionados</a>
  			<br>
  			<small class="text-muted">(*) Resultados disponibles el 22 de Octubre del 2019</small>
  		</div>
  	</div>
  </div>


  <div class="container text-align mt-5 mb-5 text-center">
  	<div class="row">
  		<div class="col-md-12 bg-light p-4 rounded">
  			<h4>Listado de Ganadores <b>Concurso Innovación social</b>.</h4>
  			<hr>
  			<p>
  				*Los ganadores del concurso serán contactados vía <b><u>correo electrónico</u></b> para mayor información.
  			</p>
  			<a href="#" target="_blank" class="btn btn-primary p_none disabled" style="width:300px;"><i class="fas fa-file-pdf"></i> Ver listado ganadores</a>
  			<br>
  			<small class="text-muted">(*) Resultados disponibles el 15 de Noviembre del 2019</small>
  		</div>
  	</div>
  </div>
</div>

<hr>


<div class="conteiner-fluid container_sponsors">
	<div class="container">
		<div class="row justify-content-md-center">
			<div class="col-md-2 col-5 align-self-center">
		<a href="#" target="_blank">
          <img src="<?=base_url();?>image/escudopuertomontt_border.png" class="w-100">
        </a>
			</div>
			<div class="col-md-2 col-5 align-self-center"> <a href="#" target="_blank">
          <img src="<?=base_url();?>image/dideco_logo_trans.png" class="w-100">
        </a>
			</div>
			<div class="col-md-3 col-7 align-self-center"> <a href="#" target="_blank">
          <img src="<?=base_url();?>image/universidadloslagos.png" class="w-100">
        </a>
			</div>
			<div class="col-md-2 col-5 align-self-center"> <a href="#" target="_blank">
          <img src="<?=base_url();?>image/logopuertomontt.png" class="w-100">
        </a>
			</div>
		</div>
	</div>
</div>
