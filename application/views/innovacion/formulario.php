<?php $qty_char = $this->config->item('qty_char') ?>
<div class="container fix_header">

  <div class="row justify-content-center">
    <div class="alert alert-warning card_container" role="alert">
      <h3 class="alert-heading"><b><i class="fas fa-lightbulb"></i> Recordatorio</b></h3>
      <p>La información que se proporcione en el formulario se administrará bajo el concepto de absoluta confidencialidad y será revisado sólo por el equipo autorizado de trabajo de la Ilustre Municipalidad de Puerto Montt y la Universidad de Los Lagos. Se garantiza absoluta reserva y discreción de la información proporcionada. </p>
      <hr>
      <p class="mb-0">Todos los campos son obligatorios.</p>
    </div>
  </div>


  <form class="needs-validation" novalidate action="<?=base_url()?>innovacion/insert" method="post" enctype="multipart/form-data">
    <div class="row justify-content-center">
      <div class="card card_container">
        <div class="card-header bg-secondary text-white">
          <i class="fas fa-pen-square"></i> Informacion básica Integrantes y Roles
        </div>

        <div class="card-body">

          <!-- TABS -->
          <ul class="nav nav-tabs nav-fill" id="myTab" role="tablist">
            <li class="nav-item">
              <a class="nav-link active" id="ejecutivo-tab" data-toggle="tab" href="#ejecutivo" role="tab" aria-controls="ejecutivo" aria-selected="true">Ejecutivo</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="finanzas-tab" data-toggle="tab" href="#finanzas" role="tab" aria-controls="finanzas" aria-selected="false">Finanzas</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="operaciones-tab" data-toggle="tab" href="#operaciones" role="tab" aria-controls="operaciones" aria-selected="false">Operaciones</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="comunicaciones-tab" data-toggle="tab" href="#comunicaciones" role="tab" aria-controls="comunicaciones" aria-selected="false">Comunicaciones (opcional)</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="comunitario-tab" data-toggle="tab" href="#comunitario" role="tab" aria-controls="comunitario" aria-selected="false">Comunitario (opcional)</a>
            </li>
          </ul>
          <!-- TABS -->

          <!--
          1.-Director Ejecutivo
          2.-Director de Finanzas
          3.-Director de Operaciones
          4.-Director de Comunicaciones (opcional)
          5.-Director Comunitario (opcional)
          -->

          <div class="tab-content" id="myTabContent">
            <div class="tab-pane fadeshow active" id="ejecutivo" role="tabpanel" aria-labelledby="ejecutivo-tab">
              <input type="hidden" class="form-control" id="user_rol" name="user_rol1" placeholder="Rol" value="1">
              <br>
              <div class="alert alert-danger" role="alert">
                <small><i class="fas fa-exclamation-triangle"></i> <b>IMPORTANTE.</b> El Director ejecutivo tendrá los <b>permisos administrativos</b> de la cuenta para ingresar más adelante.</small>
              </div>

                <div class="form-row">
                  <div class="form-group col-md-4">
                    <label for="user_rut1"><b>RUT <span class="text-danger">*</span></b></label>
                    <span class="fas fa-id-card form-control-feedback"></span>
                    <input type="text" class="form-control" id="user_rut" name="user_rut1" placeholder="RUT" placeholder="" required>
                    <div class="invalid-feedback">RUT incorrecto o ya utilizado.</div>
                  </div>

                  <div class="form-group col-md-4">
                    <label for="user_name"><b>Nombres <span class="text-danger">*</span></b></label>
                    <span class="fas fa-user form-control-feedback"></span>
                    <input type="text" class="form-control" id="user_name" name="user_name1" placeholder="Primer y segundo nombre" required>
                    <div class="invalid-feedback">Ingrese Nombre.</div>
                  </div>

                  <div class="form-group col-md-4">
                    <label for="user_surname"><b>Apellidos <span class="text-danger">*</span></b></label>
                    <span class="fas fa-user form-control-feedback"></span>
                    <input type="text" class="form-control" id="user_surname" name="user_surname1" placeholder="Primer y segundo apellido" required>
                    <div class="invalid-feedback">Ingrese Apellidos.</div>
                  </div>
                </div>
                <div class="form-row">
                  <div class="form-group col-md-4">
                    <label for="user_email"><b>Email <span class="text-danger">*</span></b></label>
                    <span class="fas fa-at form-control-feedback"></span>
                    <input type="email" class="form-control" id="user_email" name="user_email1" placeholder="nombre@dominio.com" required>
                    <div class="invalid-feedback">Ingrese Email.</div>
                  </div>
                  <div class="form-group col-md-5">
                    <label for="user_address"><b>Dirección</b></label>
                    <span class="fas fa-map-marker-alt form-control-feedback"></span>
                    <input type="text" class="form-control" id="user_address" name="user_address1" placeholder="Dirección, departamento, block" >
                    <div class="invalid-feedback">Ingrese Dirección.</div>
                  </div>
                  <div class="form-group col-md-3">
                    <label for="user_phone"><b>Telefono</b></label>
                    <span class="fas fa-phone form-control-feedback"></span>
                    <input type="text" class="form-control" id="user_phone" name="user_phone1" placeholder="Número celular" size="10">
                    <div class="invalid-feedback">Ingrese Teléfono.</div>
                  </div>
                </div>
                  <div class="form-row">
                    <div class="form-group col-md-12">
                      <label for="user_description"><b>Descripcion breve</b></label>
                      <span class="fas fa-address-card form-control-feedback"></span>
                      <input type="text" class="form-control" id="user_description" name="user_description1" placeholder="Breve decripción" size="10" >
                      <div class="invalid-feedback">Ingrese Descripción.</div>
                    </div>
                  </div>

                  <div class="form-row">
                    <div class="form-group col-md-6">

                      <label for="user_alumno_regular"><b>Certificado Alumno regular</b></label><br>
                      <div class="custom-file w-100">
                        <input type="file" class="custom-file-input" id="user_alumno_regular" name="alumno_regular1" aria-describedby="fileHelpBlock" >
                        <label class="custom-file-label" for="user_alumno_regular">Elegir Archivo...</label>
                        <small id="fileHelpBlock" class="form-text text-muted">
                          Recuerde que los documentos deben estar en formato .doc, .docx, .pdf, .jpg o .png
                        </small>
                        <div class="invalid-feedback">El certificado de alumno regular es obligatorio.</div>
                      </div>

                    </div>
                    <div class="form-group col-md-6">

                      <label for="user_cedula_identidad"><b>Fotocopia de la cedula de identidad</b></label><br>
                      <div class="custom-file w-100">
                        <input type="file" class="custom-file-input" id="user_cedula_identidad" name="cedula_identidad1" aria-describedby="fileHelpBlock" >
                        <label class="custom-file-label" for="user_cedula_identidad">Elegir Archivo...</label>
                        <small id="fileHelpBlock" class="form-text text-muted">
                          Recuerde que los documentos deben estar en formato .doc, .docx, .pdf, .jpg o .png
                        </small>
                        <div class="invalid-feedback">Fotocopia cédula de identidad.</div>
                      </div>
                    </div>
                  </div>
            </div><!--  end of .tab-content ejecutivo -->
            <div class="tab-pane fade" id="finanzas" role="tabpanel" aria-labelledby="finanzas-tab">
              <input type="hidden" class="form-control" id="user_rol" name="user_rol2" placeholder="Rol" value="2">
              <br>
                <div class="form-row">
                  <div class="form-group col-md-4">
                    <label for="user_rut1"><b>RUT <span class="text-danger">*</span></b></label>
                    <span class="fas fa-id-card form-control-feedback"></span>
                    <input type="text" class="form-control" id="user_rut" name="user_rut2" placeholder="RUT" placeholder="" required>
                    <div class="invalid-feedback">RUT incorrecto o ya utilizado.</div>
                  </div>

                  <div class="form-group col-md-4">
                    <label for="user_name"><b>Nombres <span class="text-danger">*</span></b></label>
                    <span class="fas fa-user form-control-feedback"></span>
                    <input type="text" class="form-control" id="user_name" name="user_name2" placeholder="Primer y segundo nombre" required>
                    <div class="invalid-feedback">Ingrese Nombre.</div>
                  </div>

                  <div class="form-group col-md-4">
                    <label for="user_surname"><b>Apellidos <span class="text-danger">*</span></b></label>
                    <span class="fas fa-user form-control-feedback"></span>
                    <input type="text" class="form-control" id="user_surname" name="user_surname2" placeholder="Primer y segundo apellido" required>
                    <div class="invalid-feedback">Ingrese Apellidos.</div>
                  </div>
                </div>
                <div class="form-row">
                  <div class="form-group col-md-4">
                    <label for="user_email"><b>Email <span class="text-danger">*</span></b></label>
                    <span class="fas fa-at form-control-feedback"></span>
                    <input type="email" class="form-control" id="user_email" name="user_email2" placeholder="nombre@dominio.com" required>
                    <div class="invalid-feedback">Ingrese Email.</div>
                  </div>
                  <div class="form-group col-md-5">
                    <label for="user_address"><b>Dirección</b></label>
                    <span class="fas fa-map-marker-alt form-control-feedback"></span>
                    <input type="text" class="form-control" id="user_address" name="user_address2" placeholder="Dirección, departamento, block" >
                    <div class="invalid-feedback">Ingrese Dirección.</div>
                  </div>
                  <div class="form-group col-md-3">
                    <label for="user_phone"><b>Telefono</b></label>
                    <span class="fas fa-phone form-control-feedback"></span>
                    <input type="text" class="form-control" id="user_phone" name="user_phone2" placeholder="Número celular" size="10">
                    <div class="invalid-feedback">Ingrese Teléfono.</div>
                  </div>
                </div>
                  <div class="form-row">
                    <div class="form-group col-md-12">
                      <label for="user_description"><b>Descripcion breve</b></label>
                      <span class="fas fa-address-card form-control-feedback"></span>
                      <input type="text" class="form-control" id="user_description" name="user_description2" placeholder="Breve decripción" size="10" >
                      <div class="invalid-feedback">Ingrese descripción.</div>
                    </div>
                  </div>

                  <div class="form-row">
                    <div class="form-group col-md-6">

                      <label for="user_alumno_regular"><b>Certificado Alumno regular</b></label><br>
                      <div class="custom-file w-100">
                        <input type="file" class="custom-file-input" id="user_alumno_regular" name="alumno_regular2" aria-describedby="fileHelpBlock" >
                        <label class="custom-file-label" for="user_alumno_regular">Elegir Archivo...</label>
                        <small id="fileHelpBlock" class="form-text text-muted">
                          Recuerde que los documentos deben estar en formato .doc, .docx, .pdf, .jpg o .png
                        </small>
                        <div class="invalid-feedback">El certificado de alumno regular es obligatorio.</div>
                      </div>

                    </div>
                    <div class="form-group col-md-6">

                      <label for="user_cedula_identidad"><b>Fotocopia de la cedula de identidad</b></label><br>
                      <div class="custom-file w-100">
                        <input type="file" class="custom-file-input" id="user_cedula_identidad" name="cedula_identidad2" aria-describedby="fileHelpBlock" >
                        <label class="custom-file-label" for="user_cedula_identidad">Elegir Archivo...</label>
                        <small id="fileHelpBlock" class="form-text text-muted">
                          Recuerde que los documentos deben estar en formato .doc, .docx, .pdf, .jpg o .png
                        </small>
                        <div class="invalid-feedback">Fotocopia cédula de identidad.</div>
                      </div>
                    </div>
                  </div>
            </div><!--  end of .tab-content finanzas -->
            <div class="tab-pane fade" id="operaciones" role="tabpanel" aria-labelledby="operaciones-tab">
              <input type="hidden" class="form-control" id="user_rol" name="user_rol3" placeholder="Rol" value="3">
              <br>
                <div class="form-row">
                  <div class="form-group col-md-4">
                    <label for="user_rut1"><b>RUT <span class="text-danger">*</span></b></label>
                    <span class="fas fa-id-card form-control-feedback"></span>
                    <input type="text" class="form-control" id="user_rut" name="user_rut3" placeholder="RUT" placeholder="" required>
                    <div class="invalid-feedback">RUT incorrecto o ya utilizado.</div>
                  </div>

                  <div class="form-group col-md-4">
                    <label for="user_name"><b>Nombres <span class="text-danger">*</span></b></label>
                    <span class="fas fa-user form-control-feedback"></span>
                    <input type="text" class="form-control" id="user_name" name="user_name3" placeholder="Primer y segundo nombre" required>
                    <div class="invalid-feedback">Ingrese Nombre.</div>
                  </div>

                  <div class="form-group col-md-4">
                    <label for="user_surname"><b>Apellidos <span class="text-danger">*</span></b></label>
                    <span class="fas fa-user form-control-feedback"></span>
                    <input type="text" class="form-control" id="user_surname" name="user_surname3" placeholder="Primer y segundo apellido" required>
                    <div class="invalid-feedback">Ingrese Apellidos.</div>
                  </div>
                </div>
                <div class="form-row">
                  <div class="form-group col-md-4">
                    <label for="user_email"><b>Email <span class="text-danger">*</span></b></label>
                    <span class="fas fa-at form-control-feedback"></span>
                    <input type="email" class="form-control" id="user_email" name="user_email3" placeholder="nombre@dominio.com" required>
                    <div class="invalid-feedback">Ingrese Email.</div>
                  </div>
                  <div class="form-group col-md-5">
                    <label for="user_address"><b>Dirección</b></label>
                    <span class="fas fa-map-marker-alt form-control-feedback"></span>
                    <input type="text" class="form-control" id="user_address" name="user_address3" placeholder="Dirección, departamento, block" >
                    <div class="invalid-feedback">Ingrese Dirección.</div>
                  </div>
                  <div class="form-group col-md-3">
                    <label for="user_phone"><b>Telefono</b></label>
                    <span class="fas fa-phone form-control-feedback"></span>
                    <input type="text" class="form-control" id="user_phone" name="user_phone3" placeholder="Número celular" size="10">
                    <div class="invalid-feedback">Ingrese Teléfono.</div>
                  </div>
                </div>
                  <div class="form-row">
                    <div class="form-group col-md-8">
                      <label for="user_description"><b>Descripcion breve</b></label>
                      <span class="fas fa-address-card form-control-feedback"></span>
                      <input type="text" class="form-control" id="user_description" name="user_description3" placeholder="Breve decripción" size="10" >
                      <div class="invalid-feedback">Ingrese descripción.</div>
                    </div>
                  </div>

                  <div class="form-row">
                    <div class="form-group col-md-6">

                      <label for="user_alumno_regular"><b>Certificado Alumno regular</b></label><br>
                      <div class="custom-file w-100">
                        <input type="file" class="custom-file-input" id="user_alumno_regular" name="alumno_regular3" aria-describedby="fileHelpBlock" >
                        <label class="custom-file-label" for="user_alumno_regular">Elegir Archivo...</label>
                        <small id="fileHelpBlock" class="form-text text-muted">
                          Recuerde que los documentos deben estar en formato .doc, .docx, .pdf, .jpg o .png
                        </small>
                        <div class="invalid-feedback">El certificado de alumno regular es obligatorio.</div>
                      </div>

                    </div>
                    <div class="form-group col-md-6">

                      <label for="user_cedula_identidad"><b>Fotocopia de la cedula de identidad</b></label><br>
                      <div class="custom-file w-100">
                        <input type="file" class="custom-file-input" id="user_cedula_identidad" name="cedula_identidad3" aria-describedby="fileHelpBlock" >
                        <label class="custom-file-label" for="user_cedula_identidad">Elegir Archivo...</label>
                        <small id="fileHelpBlock" class="form-text text-muted">
                          Recuerde que los documentos deben estar en formato .doc, .docx, .pdf, .jpg o .png
                        </small>
                        <div class="invalid-feedback">Fotocopia cédula de identidad.</div>
                      </div>
                    </div>
                  </div>
            </div><!--  end of .tab-content operaciones -->
            <div class="tab-pane fade" id="comunicaciones" role="tabpanel" aria-labelledby="comunicaciones-tab">
              <input type="hidden" class="form-control" id="user_rol" name="user_rol4" placeholder="Rol" value="4">
              <br>
                <div class="form-row">
                  <div class="form-group col-md-4">
                    <label for="user_rut1"><b>RUT</b></label>
                    <span class="fas fa-id-card form-control-feedback"></span>
                    <input type="text" class="form-control" id="user_rut" name="user_rut4" placeholder="RUT" placeholder="" >
                    <div class="invalid-feedback">RUT incorrecto o ya utilizado.</div>
                  </div>

                  <div class="form-group col-md-4">
                    <label for="user_name"><b>Nombres</b></label>
                    <span class="fas fa-user form-control-feedback"></span>
                    <input type="text" class="form-control" id="user_name" name="user_name4" placeholder="Primer y segundo nombre" >
                    <div class="invalid-feedback">Ingrese Nombre.</div>
                  </div>

                  <div class="form-group col-md-4">
                    <label for="user_surname"><b>Apellidos</b></label>
                    <span class="fas fa-user form-control-feedback"></span>
                    <input type="text" class="form-control" id="user_surname" name="user_surname4" placeholder="Primer y segundo apellido" >
                    <div class="invalid-feedback">Ingrese Apellidos.</div>
                  </div>
                </div>
                <div class="form-row">
                  <div class="form-group col-md-4">
                    <label for="user_email"><b>Email</b></label>
                    <span class="fas fa-at form-control-feedback"></span>
                    <input type="email" class="form-control" id="user_email" name="user_email4" placeholder="nombre@dominio.com" >
                    <div class="invalid-feedback">Ingrese Email.</div>
                  </div>
                  <div class="form-group col-md-5">
                    <label for="user_address"><b>Dirección</b></label>
                    <span class="fas fa-map-marker-alt form-control-feedback"></span>
                    <input type="text" class="form-control" id="user_address" name="user_address4" placeholder="Dirección, departamento, block" >
                    <div class="invalid-feedback">Ingrese Dirección.</div>
                  </div>
                  <div class="form-group col-md-3">
                    <label for="user_phone"><b>Telefono</b></label>
                    <span class="fas fa-phone form-control-feedback"></span>
                    <input type="text" class="form-control" id="user_phone" name="user_phone4" placeholder="Número celular" size="10">
                    <div class="invalid-feedback">Ingrese Teléfono.</div>
                  </div>
                </div>
                  <div class="form-row">
                    <div class="form-group col-md-12">
                      <label for="user_description"><b>Descripcion breve</b></label>
                      <span class="fas fa-address-card form-control-feedback"></span>
                      <input type="text" class="form-control" id="user_description" name="user_description4" placeholder="Breve decripción" size="10" >
                      <div class="invalid-feedback">Ingrese descripción.</div>
                    </div>
                  </div>

                  <div class="form-row">
                    <div class="form-group col-md-6">

                      <label for="user_alumno_regular"><b>Certificado Alumno regular</b></label><br>
                      <div class="custom-file w-100">
                        <input type="file" class="custom-file-input" id="user_alumno_regular" name="alumno_regular4" aria-describedby="fileHelpBlock" >
                        <label class="custom-file-label" for="user_alumno_regular">Elegir Archivo...</label>
                        <small id="fileHelpBlock" class="form-text text-muted">
                          Recuerde que los documentos deben estar en formato .doc, .docx, .pdf, .jpg o .png
                        </small>
                        <div class="invalid-feedback">El certificado de alumno regular es obligatorio.</div>
                      </div>

                    </div>
                    <div class="form-group col-md-6">

                      <label for="user_cedula_identidad"><b>Fotocopia de la cedula de identidad</b></label><br>
                      <div class="custom-file w-100">
                        <input type="file" class="custom-file-input" id="user_cedula_identidad" name="cedula_identidad4" aria-describedby="fileHelpBlock" >
                        <label class="custom-file-label" for="user_cedula_identidad">Elegir Archivo...</label>
                        <small id="fileHelpBlock" class="form-text text-muted">
                          Recuerde que los documentos deben estar en formato .doc, .docx, .pdf, .jpg o .png
                        </small>
                        <div class="invalid-feedback">Fotocopia cédula de identidad.</div>
                      </div>
                    </div>
                  </div>
            </div><!--  end of .tab-content comunicaciones -->
            <div class="tab-pane fade" id="comunitario" role="tabpanel" aria-labelledby="comunitario-tab">
              <input type="hidden" class="form-control" id="user_rol" name="user_rol5" placeholder="Rol" value="5">
              <br>
                <div class="form-row">
                  <div class="form-group col-md-4">
                    <label for="user_rut1"><b>RUT</b></label>
                    <span class="fas fa-id-card form-control-feedback"></span>
                    <input type="text" class="form-control" id="user_rut" name="user_rut5" placeholder="RUT" placeholder="" >
                    <div class="invalid-feedback">RUT incorrecto o ya utilizado.</div>
                  </div>

                  <div class="form-group col-md-4">
                    <label for="user_name"><b>Nombres</b></label>
                    <span class="fas fa-user form-control-feedback"></span>
                    <input type="text" class="form-control" id="user_name" name="user_name5" placeholder="Primer y segundo nombre" >
                    <div class="invalid-feedback">Ingrese Nombre.</div>
                  </div>

                  <div class="form-group col-md-4">
                    <label for="user_surname"><b>Apellidos</b></label>
                    <span class="fas fa-user form-control-feedback"></span>
                    <input type="text" class="form-control" id="user_surname" name="user_surname5" placeholder="Primer y segundo apellido" >
                    <div class="invalid-feedback">Ingrese Apellidos.</div>
                  </div>
                </div>
                <div class="form-row">
                  <div class="form-group col-md-4">
                    <label for="user_email"><b>Email</b></label>
                    <span class="fas fa-at form-control-feedback"></span>
                    <input type="email" class="form-control" id="user_email" name="user_email5" placeholder="nombre@dominio.com" >
                    <div class="invalid-feedback">Ingrese Email.</div>
                  </div>
                  <div class="form-group col-md-5">
                    <label for="user_address"><b>Dirección</b></label>
                    <span class="fas fa-map-marker-alt form-control-feedback"></span>
                    <input type="text" class="form-control" id="user_address" name="user_address5" placeholder="Dirección, departamento, block" >
                    <div class="invalid-feedback">Ingrese Dirección.</div>
                  </div>
                  <div class="form-group col-md-3">
                    <label for="user_phone"><b>Telefono</b></label>
                    <span class="fas fa-phone form-control-feedback"></span>
                    <input type="text" class="form-control" id="user_phone" name="user_phone5" placeholder="Número celular" size="10" >
                    <div class="invalid-feedback">Ingrese Teléfono.</div>
                  </div>
                </div>
                  <div class="form-row">
                    <div class="form-group col-md-12">
                      <label for="user_description"><b>Descripcion breve</b></label>
                      <span class="fas fa-address-card form-control-feedback"></span>
                      <input type="text" class="form-control" id="user_description" name="user_description5" placeholder="Breve decripción" size="10" >
                      <div class="invalid-feedback">Ingrese Teléfono.</div>
                    </div>
                  </div>

                  <div class="form-row">
                    <div class="form-group col-md-6">

                      <label for="user_alumno_regular"><b>Certificado Alumno regular</b></label><br>
                      <div class="custom-file w-100">
                        <input type="file" class="custom-file-input" id="user_alumno_regular" name="alumno_regular5" aria-describedby="fileHelpBlock" >
                        <label class="custom-file-label" for="user_alumno_regular">Elegir Archivo...</label>
                        <small id="fileHelpBlock" class="form-text text-muted">
                          Recuerde que los documentos deben estar en formato .doc, .docx, .pdf, .jpg o .png
                        </small>
                        <div class="invalid-feedback">El certificado de alumno regular es obligatorio.</div>
                      </div>

                    </div>
                    <div class="form-group col-md-6">

                      <label for="user_cedula_identidad"><b>Fotocopia de la cedula de identidad</b></label><br>
                      <div class="custom-file w-100">
                        <input type="file" class="custom-file-input" id="user_cedula_identidad" name="cedula_identidad5" aria-describedby="fileHelpBlock" >
                        <label class="custom-file-label" for="user_cedula_identidad">Elegir Archivo...</label>
                        <small id="fileHelpBlock" class="form-text text-muted">
                          Recuerde que los documentos deben estar en formato .doc, .docx, .pdf, .jpg o .png
                        </small>
                        <div class="invalid-feedback">Fotocopia cédula de identidad.</div>
                      </div>
                    </div>
                  </div>
            </div><!--  end of .tab-content comunitario -->
          </div>
          <br>
          <span class="text-muted">(<b><span class="text-danger">*</span></b>) Campos obligatorios.</span>
        </div> <!-- End card body -->
      </div>
    </div>

    <br>

    <div class="row justify-content-center">
      <div class="card card_container">
        <div class="card-header bg-secondary text-white">
          <i class="fas fa-pen-square"></i> Informacion básica Mentor
        </div>

        <div class="card-body">
            <div class="form-row">
              <div class="form-group col-md-4">
                <label for="men_user_rut"><b>RUT <span class="text-danger">*</span></b></label>
                <span class="fas fa-id-card form-control-feedback"></span>
                <input type="text" class="form-control" id="men_user_rut" name="men_user_rut" placeholder="RUT" placeholder="" required>
                <div class="invalid-feedback">RUT incorrecto o ya utilizado.</div>
              </div>

              <div class="form-group col-md-4">
                <label for="men_user_name"><b>Nombres <span class="text-danger">*</span></b></label>
                <span class="fas fa-user form-control-feedback"></span>
                <input type="text" class="form-control" id="men_user_name" name="men_user_name" placeholder="Primer y segundo nombre" required>
                <div class="invalid-feedback">Ingrese Nombre.</div>
              </div>

              <div class="form-group col-md-4">
                <label for="men_user_surname"><b>Apellidos <span class="text-danger">*</span></b></label>
                <span class="fas fa-user form-control-feedback"></span>
                <input type="text" class="form-control" id="men_user_surname" name="men_user_surname" placeholder="Primer y segundo apellido" required>
                <div class="invalid-feedback">Ingrese Apellidos.</div>
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col-md-6">
                <label for="men_user_phone"><b>Telefono</b></label>
                <span class="fas fa-phone form-control-feedback"></span>
                <input type="text" class="form-control" id="men_user_phone" name="men_user_phone" placeholder="Número celular" size="10" >
                <div class="invalid-feedback">Ingrese Teléfono.</div>
              </div>
              <div class="form-group col-md-6">
                <label for="men_user_email"><b>Email <span class="text-danger">*</span></b></label>
                <span class="fas fa-at form-control-feedback"></span>
                <input type="email" class="form-control" id="men_user_email" name="men_user_email" placeholder="nombre@dominio.com" required>
                <div class="invalid-feedback">Ingrese Email.</div>
              </div>
            </div>
              <div class="form-row">
                <div class="form-group col-md-12">
                  <label for="men_user_description"><b>Descripcion breve</b></label>
                  <span class="fas fa-address-card form-control-feedback"></span>
                  <input type="text" class="form-control" id="men_user_description" name="men_user_description" placeholder="Breve decripción" size="10" >
                  <div class="invalid-feedback">Ingrese Descripcion.</div>
                </div>
              </div>
              <br>
              <span class="text-muted">(<b><span class="text-danger">*</span></b>) Campos obligatorios.</span>
            </div>
      </div>
    </div>

    <br>

    <div class="row justify-content-center">
      <div class="card card_container">
        <div class="card-header bg-secondary text-white">
          <i class="fas fa-pen-square"></i> Elección del desafio
        </div>
        <div class="card-body">
          <div class="form-row">
            <div class="custom-control custom-radio">
              <input type="radio" class="custom-control-input" id="desafio1" name="solution_challenge" value="1" required>
              <label class="custom-control-label" for="desafio1">
                <b>1.- <u>Eje Estratégico:</u> Desarrollo Humano, Cultura, Seguridad y Calidad de Vida. <span class="text-danger">*</span></b>
                <p class="pl-5"><b>DESAFIO 1.</b> Promover la participación para que la comunidad local acceda a espacios y servicios que contribuyan a la satisfacción de sus necesidades sociales, culturales, recreativas y de seguridad.</p>
              </label>
            </div>

            <div class="custom-control custom-radio mb-3">
              <input type="radio" class="custom-control-input" id="desafio2" name="solution_challenge" value="2" required>
              <label class="custom-control-label" for="desafio2">
                <b>2.- <u>Eje Estratégico:</u> Desarrollo Territorial y Medio Ambiente. <span class="text-danger">*</span></b>
                <p class="pl-5"><b>DESAFIO 2.</b> Contribuir a mitigar el impacto de las actividades humanas en el territorio, protegiendo los elementos de valor natural y cultural.</label>
            </div>

            <div class="alert alert-success" role="alert">
              <i class="fas fa-exclamation-triangle"></i> Para más información visite: <a href="https://www.puertomontt.cl/municipalidad/pladeco/" target="_blank">https://www.puertomontt.cl/municipalidad/pladeco/</a>
            </div>

          </div>

            <label for="solution_deepen"><b>Profundiza tu desafio (PLADECO 2016-2017).</b></label>
            <!--<span class="span float-right" id="count"><?= $qty_char ?></span>-->
            <textarea class="form-control" id="solution_deepen" name="solution_deepen" rows="5" data-limit=<?= $qty_char ?> ></textarea>
            <br>
            <label for="solution_recipient"><b>Beneficiarios.</b></label>
            <!--<span class="span float-right" id="count"><?= $qty_char ?></span>-->
            <textarea class="form-control" id="solution_recipient" name="solution_recipient" rows="5" data-limit=<?= $qty_char ?> ></textarea>



        </div>
      </div>
    </div>

    <br>

    <div class="row justify-content-center">
      <div class="card card_container">
        <div class="card-header bg-secondary text-white">
          <i class="fas fa-briefcase"></i>  Propuesta de solución
        </div>
        <div class="card-body">

              <label for="solution_proposal"><b>Propuesta de Valor:</b> ¿Qué busca solucionar esta idea? ¿Cómo se relaciona con los
                desafíos definidos desde el PLADECO?¿Qué es lo novedoso y cómo se diferencia de otras ideas?¿Por qué es la mejor solución?</label>
              <!--<span class="span float-right" id="count"><?= $qty_char ?></span>-->
              <textarea class="form-control" id="solution_proposal" name="solution_proposal" rows="5" data-limit=<?= $qty_char ?> ></textarea>
              <br>
              <label for="solution_community"><b>Comunidad Implicada:</b> ¿Quiénes se ven afectados por el problema y qué características tienen? ¿Cómo el proyecto involucrará a la comunidad?¿Qué representantes de esta comunidad participarían en el proyecto?¿Has establecido contacto con personas u organizaciones que estén relacionados con los desafíos establecidos?</label>
              <!--<span class="span float-right" id="count"><?= $qty_char ?></span>-->
              <textarea class="form-control" id="solution_community" name="solution_community" rows="5" data-limit=<?= $qty_char ?> ></textarea>
              <br>
              <label for="solution_prototipe"><b>Prototipo:</b> ¿cómo testear que nuestra idea sea factible? ¿Cómo llevar a cabo el
                prototipo, integrar feedback de la comunidad, iterar, monitorear y ajustar?</label>
              <!--<span class="span float-right" id="count"><?= $qty_char ?></span>-->
              <textarea class="form-control" id="solution_prototipe" name="solution_prototipe" rows="5" data-limit=<?= $qty_char ?> ></textarea>
              <br>
              <label for="solution_susten"><b>Sustenibilidad:</b> ¿Qué se espera obtener al final del proyecto? ¿El proyecto tendrá un triple resultado (económica, social y medioambiente)? ¿Cómo se piensa sostener una vez acabado el financiamiento? ¿Qué impacto podría tener el proyecto en el largo plazo?</label>
              <!--<span class="span float-right" id="count"><?= $qty_char ?></span>-->
              <textarea class="form-control" id="solution_susten" name="solution_susten" rows="5" data-limit=<?= $qty_char ?> ></textarea>
              <br>
              <label for="solution_test"><b>¿Qué necesito Testear con mi Idea?</b></label>
              <!--<span class="span float-right" id="count"><?= $qty_char ?></span>-->
              <textarea class="form-control" id="solution_test" name="solution_test" rows="5" data-limit=<?= $qty_char ?> ></textarea>
              <br>
              <label for="solution_test"><b>Presupuesto:</b> Definir los costos del proyecto y las coherencias con las actividades planificadas.</label>
              <!--<span class="span float-right" id="count"><?= $qty_char ?></span>-->
              <textarea class="form-control" id="solution_test" name="solution_budget" rows="5" data-limit=<?= $qty_char ?> ></textarea>
      </div>


    </div>
</div>

      <br>


    <div class="row justify-content-center">
      <div class="card card_container">
        <div class="card-header bg-secondary text-white">
          <i class="fas fa-file"></i> Documentacion necesaria
        </div>
        <div class="card-body">
              <label for="user_file_patrocinio"><b>1.- Carta de Patrocinio por parte de la Institución de Educación a la que pertenece.</b></label>
              <div class="custom-file">
                <input type="file" class="custom-file-input" id="user_file_patrocinio" name="patrocinio" aria-describedby="fileHelpBlock" >
                <label class="custom-file-label" for="user_file_identity">Elegir Archivo...</label>
                <small id="fileHelpBlock" class="form-text text-muted">
                  Recuerde que los documentos deben estar en formato .doc, .docx, .pdf, .jpg o .png
                </small>
                <div class="invalid-feedback">La copia de la cedula es obligatoria.</div>
              </div>

            <br><br>

            <label for="user_file_mentor"><b>2.- Carta de un Mentor-Académico de su institución, quien lo acompañará y asesorará
durante el proceso.</b></label>
            <div class="custom-file">
              <input type="file" class="custom-file-input" id="user_file_mentor" name="mentor_academico" aria-describedby="fileHelpBlock" >
              <label class="custom-file-label" for="user_file_residence">Elegir Archivo...</label>
              <small id="fileHelpBlock" class="form-text text-muted">
                Recuerde que los documentos deben estar en formato .doc, .docx, .pdf, .jpg o .png
              </small>
              <div class="invalid-feedback">El certificado de residencia es obligatorio.</div>
            </div>

            <br><br>

            <label for="user_file_reunion"><b>3.- Evidencias de reunión con la sociedad: organizaciones civiles.</b></label>
            <div class="custom-file">
              <input type="file" class="custom-file-input" id="user_file_reunion" name="reunion" aria-describedby="fileHelpBlock" >
              <label class="custom-file-label" for="user_file_residence">Elegir Archivo...</label>
              <small id="fileHelpBlock" class="form-text text-muted">
                Recuerde que los documentos deben estar en formato .doc, .docx, .pdf, .jpg o .png
              </small>
              <div class="invalid-feedback">El certificado de residencia es obligatorio.</div>
            </div>

            <br><br>

            <label for="user_file_instrumentos"><b>4.- Instrumentos de Levantamiento de Información.</b></label><br>
            <div class="custom-file">
              <input type="file" class="custom-file-input" id="user_file_instrumentos" name="instrumentos" aria-describedby="fileHelpBlock" >
              <label class="custom-file-label" for="user_file_residence">Elegir Archivo...</label>
              <small id="fileHelpBlock" class="form-text text-muted">
                Recuerde que los documentos deben estar en formato .doc, .docx, .pdf, .jpg o .png
              </small>
              <div class="invalid-feedback">El certificado de residencia es obligatorio.</div>
            </div>


        </div>
        <br><br>

          <button type="submit" class="btn btn-primary btn-lg btn-xlg" id="submit_button"><i class="fas fa-share-square"></i> Enviar información</button>



      </div>
    </div>


    </form>
</div>
