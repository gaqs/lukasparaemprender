<?php $qty_char = $this->config->item('qty_char') ?>
<div class="container fix_header">

  <div class="row justify-content-center">
    <div class="alert alert-warning card_container" role="alert">
      <h3 class="alert-heading"><b><i class="fas fa-lightbulb"></i> Recordatorio</b></h3>
      <p>La información que se proporcione en el formulario se administrará bajo el concepto de absoluta confidencialidad y será revisado sólo por el equipo autorizado de trabajo de la Ilustre Municipalidad de Puerto Montt y la Universidad de Los Lagos. Se garantiza absoluta reserva y discreción de la información proporcionada. </p>
      <hr>
      <p class="mb-0">Todos los campos son obligatorios.</p>
    </div>
  </div>


  <form class="needs-validation" novalidate action="empresa/insert" method="post" enctype="multipart/form-data">
    <div class="row justify-content-center">
      <div class="card card_container">
        <div class="card-header bg-secondary text-white">
          <i class="fas fa-pen-square"></i> Informacion básica
        </div>
        <div class="card-body">
            <div class="form-row">
              <div class="form-group col-md-6">
                <label for="user_project_name"><b>Nombre del Proyecto</b></label>
                <span class="fas fa-project-diagram form-control-feedback"></span>
                <input type="text" class="form-control" id="user_proyect_name" name="user_proyect_name" placeholder="Nombre identificatorio del proyecto" required>
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col-md-4">
                <label for="user_rut"><b>RUT</b></label>
                <span class="fas fa-id-card form-control-feedback"></span>
                <input type="text" class="form-control" id="user_rut" name="user_rut" placeholder="RUT" placeholder="" required>
                <div class="invalid-feedback">RUT incorrecto o ya utilizado.</div>
              </div>

              <div class="form-group col-md-4">
                <label for="user_name"><b>Nombres</b></label>
                <span class="fas fa-user form-control-feedback"></span>
                <input type="text" class="form-control" id="user_name" name="user_name" placeholder="Primer y segundo nombre" required>
                <div class="invalid-feedback">Ingrese Nombre.</div>
              </div>

              <div class="form-group col-md-4">
                <label for="user_surname"><b>Apellidos</b></label>
                <span class="fas fa-user form-control-feedback"></span>
                <input type="text" class="form-control" id="user_surname" name="user_surname" placeholder="Primer y segundo apellido" required>
                <div class="invalid-feedback">Ingrese Apellidos.</div>
              </div>
            </div>
            <div class="form-group">
              <label for="user_job"><b>Ocupacion</b></label>
              <span class="fas fa-user-tie form-control-feedback"></span>
              <input type="text" class="form-control" id="user_job" name="user_job" placeholder="Ocupación o profesión" required>
              <div class="invalid-feedback">Ingrese Ocupación.</div>
            </div>
            <div class="form-row">
              <div class="form-group col-md-6">
                <label for="user_phone"><b>Empresa</b></label>
                <span class="fas fa-building form-control-feedback"></span>
                <input type="text" class="form-control" id="user_business" name="user_business" placeholder="Nombre de la empresa" size="10" required>
                <div class="invalid-feedback">Ingrese Empresa.</div>
              </div>
              <div class="form-group col-md-6">
                <label for="user_email"><b>Cargo</b></label>
                <span class="fas fa-user-md form-control-feedback"></span>
                <input type="text" class="form-control" id="user_position" name="user_position" placeholder="Cargo dentro de la empresa" required>
                <div class="invalid-feedback">Ingrese cargo dentro de la empresa.</div>
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col-md-5">
                <label for="user_phone"><b>Telefono</b></label>
                <span class="fas fa-phone form-control-feedback"></span>
                <input type="text" class="form-control" id="user_phone" name="user_phone" placeholder="Telefóno celular" size="10" required>
                <div class="invalid-feedback">Ingrese Teléfono.</div>
              </div>
              <div class="form-group col-md-7">
                <label for="user_email"><b>Email</b></label>
                <span class="fas fa-at form-control-feedback"></span>
                <input type="email" class="form-control" id="user_email" name="user_email" placeholder="nombre@dominio.com" required>
                <div class="invalid-feedback">Ingrese Email.</div>
              </div>
            </div>
        </div>
      </div>
    </div>

    <br>

    <div class="row justify-content-center">
      <div class="card card_container">
        <div class="card-header bg-secondary text-white">
          <i class="fas fa-building"></i>  Datos de la Empresa
        </div>
        <div class="card-body">
            <div class="form-row">
              <div class="form-group col-md-4">
                <label for="user_rut"><b>RUT empresa</b></label>
                <span class="fas fa-id-card form-control-feedback"></span>
                <input type="text" class="form-control" id="business_rut" name="business_rut" placeholder="RUT empresa" required>
                <div class="invalid-feedback">RUT obligatorio</div>
              </div>

              <div class="form-group col-md-5">
                <label for="user_name"><b>Razon Social</b></label>
                <span class="fas fa-file-signature form-control-feedback"></span>
                <input type="text" class="form-control" id="business_name" name="business_name" placeholder="Razon social de la empresa" required>
                <div class="invalid-feedback">Ingrese razon social de la empresa</div>
              </div>

              <div class="form-group col-md-3">
                <label for="user_surname"><b>Fecha Inicio Actividades</b></label>
                <span class="fas fa-calendar-alt form-control-feedback"></span>
                <input type="date" class="form-control" id="business_date" name="business_date" placeholder="" required>
                <div class="invalid-feedback">Ingrese fecha inicio actividades.</div>
              </div>
            </div>
            <div class="form-group">
              <label for="user_job"><b>Dirección</b></label>
              <span class="fas fa-map-marker-alt form-control-feedback"></span>
              <input type="text" class="form-control" id="business_address" name="business_address" placeholder="Dirección de la empresa" required>
              <div class="invalid-feedback">Ingrese Dirección.</div>
            </div>
            <div class="form-row">
              <div class="form-group col-md-5">
                <label for="user_phone"><b>Telefono</b></label>
                <span class="fas fa-phone form-control-feedback"></span>
                <input type="text" class="form-control" id="business_phone" name="business_phone" placeholder="Teléfono de contacto de la empresa" size="10" required>
                <div class="invalid-feedback">Ingrese Telefono.</div>
              </div>
              <div class="form-group col-md-7">
                <label for="user_email"><b>Página web</b></label>
                <span class="fas fa-desktop form-control-feedback"></span>
                <input type="text" class="form-control" id="business_web" name="business_web" placeholder="http://www.dominio.com">
                <div class="invalid-feedback"></div>
              </div>
            </div>
        </div>
      </div>
    </div>

    <br>

    <div class="row justify-content-center">
      <div class="card card_container">
        <div class="card-header bg-secondary text-white">
          <i class="fas fa-briefcase"></i>  Información relevante al emprendimiento
        </div>
        <div class="card-body">

              <label for="bussiness_explication"><b>1.- Explique de forma breve a qué se dedica su empresa y cuál es su idea para ella.</b></label>
              <span class="span float-right" id="count"><?= $qty_char ?></span>
              <textarea class="form-control" id="bussiness_explication" name="bussiness_explication" rows="4" data-limit=<?= $qty_char ?> required></textarea>
              <br>
              <label for="bussiness_team"><b>2.- Si cuenta con un equipo emprendedor (personas), haga una breve reseña de ellos.</b></label>
              <span class="span float-right" id="count"><?= $qty_char ?></span>
              <textarea class="form-control" id="bussiness_team" name="bussiness_team" rows="4" data-limit=<?= $qty_char ?> required></textarea>
              <br>
              <label for="bussiness_diferences"><b>3.- ¿Qué hará con el financiamiento adjudicado de este Concurso?.</b></label>
              <span class="span float-right" id="count"><?= $qty_char ?></span>
              <textarea class="form-control" id="bussiness_finances" name="bussiness_finances" rows="4" data-limit=<?= $qty_char ?> required></textarea>
              <br>
              <label for="bussiness_finances"><b>4.- ¿Quiénes son sus potenciales clientes? breve descripción.</b></label>
              <span class="span float-right" id="count"><?= $qty_char ?></span>
              <textarea class="form-control" id="bussiness_clients" name="bussiness_clients" rows="4" data-limit=<?= $qty_char ?> required></textarea>
              <br>
              <label for="bussiness_commercialization"><b>5.- ¿Cuenta con un sistema de comercialización?.</b></label>
              <span class="span float-right" id="count"><?= $qty_char ?></span>
              <textarea class="form-control" id="bussiness_commercialization" name="bussiness_commercialization" rows="4" data-limit=<?= $qty_char ?> required></textarea>
              <br>
              <label for="bussiness_status"><b>6.- ¿Quiénes son su competencia?.</b></label>
              <span class="span float-right" id="count"><?= $qty_char ?></span>
              <textarea class="form-control" id="bussiness_competition" name="bussiness_competition" rows="4" data-limit=<?= $qty_char ?> required></textarea>
              <br>
              <label for="bussiness_complementary"><b>7.- Cuáles son sus Proyecciones de ventas esperadas para el próximo año (2020), en caso de ser ganador.</b></label>
              <span class="span float-right" id="count"><?= $qty_char ?></span>
              <textarea class="form-control" id="bussiness_projection" name="bussiness_projection" data-limit=<?= $qty_char ?> rows="4" required></textarea>
              <br>
              <label for="bussiness_jobs"><b>8.- ¿Generará nuevos puestos de trabajo en caso de obtener el financiamiento?.</b></label>
              <span class="span float-right" id="count"><?= $qty_char ?></span>
              <textarea class="form-control" id="bussiness_jobs" name="bussiness_jobs" rows="4" data-limit=<?= $qty_char ?> required></textarea>
              <br>
              <label for="bussiness_complementary"><b>9.- INFORMACIÓN COMPLEMENTARIA Y OTROS ASPECTOS QUE QUIERA USTED DESTACAR</b></label>
              <div class="row">
                <div class="col-md-7">
                  <span class="span float-right" id="count"><?= $qty_char ?></span>
                  <textarea class="form-control" id="bussiness_complementary" name="bussiness_complementary" rows="6" data-limit=<?= $qty_char ?> rows="4" required></textarea>
                </div>
                <div class="col-md-5">
                  <span class="span float-left">Si desea subir documentos relacionados.</span>
                  <br><br>
                  <div class="custom-file w-100">
                    <input type="file" class="custom-file-input" id="complement_file1" name="complemento1" aria-describedby="fileHelpBlock">
                    <label class="custom-file-label" for="user_file_identity">Elegir Archivo...</label>
                  </div>
                  <br><br>
                  <div class="custom-file w-100">
                    <input type="file" class="custom-file-input" id="complement_file2" name="complemento2" aria-describedby="fileHelpBlock">
                    <label class="custom-file-label" for="user_file_identity">Elegir Archivo...</label>
                    <small id="fileHelpBlock" class="form-text text-muted">
                      Recuerde que los documentos deben estar en formato .doc, .docx, .pdf, .jpg o .png
                    </small>
                  </div>
              </div>
            </div>

        </div>
      </div>


    </div>


      <br>


    <div class="row justify-content-center">
      <div class="card card_container">
        <div class="card-header bg-secondary text-white">
          <i class="fas fa-file"></i> Documentacion necesaria
        </div>
        <div class="card-body">
              <label for="user_file_identity"><b>1.- Copia de cédula de identidad por ambos lados del representante legal.</b></label>
              <div class="custom-file">
                <input type="file" class="custom-file-input" id="business_file_identity" name="cedula" aria-describedby="fileHelpBlock" required>
                <label class="custom-file-label" for="user_file_identity">Elegir Archivo...</label>
                <small id="fileHelpBlock" class="form-text text-muted">
                  Recuerde que los documentos deben estar en formato .doc, .docx, .pdf, .jpg o .png
                </small>
                <div class="invalid-feedback">La copia de la cedula es obligatoria.</div>
              </div>

            <br><br>

            <label for="user_file_residence"><b>2.- Copia Rol Único Tributario.</b></label><br>
            <div class="custom-file">
              <input type="file" class="custom-file-input" id="business_file_rol" name="rol" aria-describedby="fileHelpBlock" required>
              <label class="custom-file-label" for="business_file_rol">Elegir Archivo...</label>
              <small id="fileHelpBlock" class="form-text text-muted">
                Recuerde que los documentos deben estar en formato .doc, .docx, .pdf, .jpg o .png
              </small>
              <div class="invalid-feedback">El certificado del rol unico Tributario es obligatorio.</div>
            </div>

            <br><br>

            <label for="user_file_residence"><b>3.-Copia Declaración de Impuesto a la Renta 2018 según atañe.</b></label>
            <div class="custom-file">
              <input type="file" class="custom-file-input" id="business_file_rent" name="renta" aria-describedby="fileHelpBlock" required>
              <label class="custom-file-label" for="business_file_rent">Elegir Archivo...</label>
              <small id="fileHelpBlock" class="form-text text-muted">
                Recuerde que los documentos deben estar en formato .doc, .docx, .pdf, .jpg o .png
              </small>
              <div class="invalid-feedback">La Declaración de Impuesto a la Renta es obligatorio.</div>
            </div>

            <br><br>

            <label for="user_file_residence"><b>4.- Copia Patente Municipal del último semestre.</b></label><br>
            <div class="custom-file">
              <input type="file" class="custom-file-input" id="business_file_patent" name="patente" aria-describedby="fileHelpBlock" required>
              <label class="custom-file-label" for="business_file_patent">Elegir Archivo...</label>
              <small id="fileHelpBlock" class="form-text text-muted">
                Recuerde que los documentos deben estar en formato .doc, .docx, .pdf, .jpg o .png
              </small>
              <div class="invalid-feedback">La Copia Patente Municipal es obligatorio.</div>
            </div>

            <br><br>

            <label for="user_file_residence"><b>5.- Copia otros permisos de operación del negocio de acuerdo a su naturaleza (ejemplo resolución sanitaria, permisos sectoriales, etc.)</b></label>
            <div class="custom-file">
              <input type="file" class="custom-file-input" id="business_file_other" name="otrospermisos" aria-describedby="fileHelpBlock" required>
              <label class="custom-file-label" for="business_file_other">Elegir Archivo...</label>
              <small id="fileHelpBlock" class="form-text text-muted">
                Recuerde que los documentos deben estar en formato .doc, .docx, .pdf, .jpg o .png
              </small>
              <div class="invalid-feedback">La copia de permisos de operación obligatorio.</div>
            </div>

            <br><br>

            <label for="user_file_residence"><b>6.- Boleta de Agua, Luz o Gas ORIGINAL (una de las tres) que esté a nombre del POSTULANTE con una antigüedad máxima de tres meses, donde se identifique el domicilio particular o donde se desarrollará el emprendimiento. </b></label>
            <div class="custom-file">
              <input type="file" class="custom-file-input" id="business_file_ticket" name="boleta" aria-describedby="fileHelpBlock" required>
              <label class="custom-file-label" for="business_file_ticket">Elegir Archivo...</label>
              <small id="fileHelpBlock" class="form-text text-muted">
                Recuerde que los documentos deben estar en formato .doc, .docx, .pdf, .jpg o .png
              </small>
              <div class="invalid-feedback">La boleta es obligatorio.</div>
            </div>
        </div>
        <button type="submit" class="btn btn-primary btn-lg btn-xlg" id="submit_button" disabled><i class="fas fa-share-square"></i> Enviar información</button>

      </div>
    </div>


    </form>
</div>
