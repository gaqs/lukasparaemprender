<section class="contacto">
   <div class="container fix_header">
      <div class="row">
         <div class="col-md-6">
         	<div id="fixed_map" class="w-100" style="height: 500px;"></div>
         </div>
         <div class="col-md-6">
           <div class="text-dark">
            <h2><b>Dudas, consultas? Escribenos!</b></h2>
           	 <i class="fas fa-phone"></i> (+65) 2 261315
           	 <br>
             <i class="fas fa-phone"></i> (+65) 2 261323
           	 <br>
				     <i class="fas fa-envelope"></i> lukasparaemprender@puertomontt.cl
          </div>
              <br>

            <form action="<?= base_url();?>home/email_consulta" method="post">
                <div class="row">
                   <div class="form-group col-md-6">
                     <label class="sr-only" for="email"></label>
                     <span class="fas fa-user form-control-feedback"></span>
                     <input type="text" class="form-control" id="name" name="name" placeholder="Nombre completo*">
                   </div>
                   <div class="form-group col-md-6">
                     <label class="sr-only" for="email"></label>
                     <span class="fas fa-phone form-control-feedback"></span>
                     <input type="phone" class="form-control" id="phone" name="phone" placeholder="Teléfono">
                   </div>
                </div>
               <div class="form-group">
                  <label class="sr-only" for="email"></label>
                  <span class="fas fa-at form-control-feedback"></span>
                  <input type="email" class="form-control" id="email" name="email" placeholder="Correo electronico*">
               </div>
               <div class="form-group">
                  <label class="sr-only" for="subject"></label>
                  <span class="fas fa-comments form-control-feedback"></span>
                  <input type="text" class="form-control" id="subject" name="subject" placeholder="Asunto*">
               </div>
               <div class="form-group">
                  <textarea class="form-control" id="message" row="4" name="message" placeholder="Escribe tu mensaje*"></textarea>
               </div>
               <input type="submit" class="btn btn-lg btn-primary" value="Enviar mensaje">

               <div class="status">Ingrese sus datos y contactese con nosotros.</div>
            </form>
         </div>
      </div>
   </div>
</section>
<!--
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal">
  Launch demo modal
</button>
-->
<div class="modal" tabindex="-1" role="dialog" id="modal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Correo Enviado</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>
      		Su correo ha sido enviado correctamente. Pronto nos comunicaremos con usted.
        </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">

	var oficina = "<div class=font-weight-bold>Subdireccion de Desarrollo Economico Local<br>Direccion de Desarrollo Comunitario</div>Edificio Consistorial 2</b><br>Presidente Ibañez #600<br>2do piso";

    var locations = [
      [oficina, -41.462331, -72.937423, 4]
    ];

    preventLoadRoboto();

    var map = new google.maps.Map(document.getElementById('fixed_map'), {
      zoom: 17,
      center: new google.maps.LatLng(-41.462331, -72.937423),
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var infowindow = new google.maps.InfoWindow();

    var marker, i;

    for (i = 0; i < locations.length; i++) {

      	marker = new google.maps.Marker({
	        position: new google.maps.LatLng(locations[i][1], locations[i][2]),
	        map: map
	    });

	    google.maps.event.addListener(marker, 'click', (function(marker, i) {
	        return function() {
	           infowindow.setContent(locations[i][0]);
	           infowindow.open(map, marker);
	        }
	    })(marker, i));
	}

	new google.maps.event.trigger( marker, 'click' );

	$('#modal').on('hidden.bs.modal', function (e) {
  		window.location.href = '<?= base_url(); ?>';
	})

</script>
