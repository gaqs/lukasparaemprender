<?php
$qty_char = $this->config->item('qty_char');
$disabled = '';
//Determina cuales key en array tienen valor 0
$array_val = array();
foreach ($user[0] as $key => $value) {
  $pos = strpos( $key, 'val_' );
  if( $pos == 'false' && $value == '0'){
      array_push($array_val, $key);
  }
}
$val = implode(',',$array_val);

/*
if(empty($array_val)){
  $disabled = 'disabled';
}
*/
?>

<div class="container fix_header">

  <div class="row justify-content-center">
    <div class="alert alert-warning card_container" role="alert">
      <h3 class="alert-heading"><b><i class="fas fa-lightbulb"></i> Recordatorio</b></h3>
      <p>La información que se proporcione en el formulario se administrará bajo el concepto de absoluta confidencialidad y será revisado sólo por el equipo autorizado de trabajo de la Ilustre Municipalidad de Puerto Montt y la Universidad de Los Lagos. Se garantiza absoluta reserva y discreción de la información proporcionada. </p>
      <hr>
      <p class="mb-0">Recuerde que al terminar debe presionar el botón <b>GUARDAR CAMBIOS</b>, al final de la página.</p>
    </div>
  </div>

  <form class="needs-validation" novalidate action="user/edit_emprendimiento" method="post" enctype="multipart/form-data">
    <input type="hidden" value="<?= $user[0]['id'] ?>" class="form-control" id="user_id" name="user_id">
    <div class="row justify-content-center">
      <div class="card card_container">
        <div class="card-header bg-secondary text-white">
          <i class="fas fa-pen-square"></i>  Informacion básica
        </div>
        <div class="card-body">
            <div class="form-row">
              <div class="form-group col-md-4">
                <label for="user_rut">RUT</label>
                <span class="fas fa-id-card form-control-feedback"></span>
                <input type="text" value="<?= $user[0]['rut'] ?>" class="form-control" id="rut" name="user_rut" placeholder="RUT" placeholder="" readonly>
                <div class="invalid-feedback">RUT incorrecto.</div>
              </div>

              <div class="form-group col-md-4">
                <label for="user_name">Nombres</label>
                <span class="fas fa-user form-control-feedback"></span>
                <input type="text" value="<?= $user[0]['nombres'] ?>" class="form-control" id="nombres" name="user_name" placeholder="Nombre">
                <div class="invalid-feedback">Ingrese Nombre.</div>
              </div>

              <div class="form-group col-md-4">
                <label for="user_surname">Apellidos</label>
                <span class="fas fa-user form-control-feedback"></span>
                <input type="text" value="<?= $user[0]['apellidos'] ?>" class="form-control" id="apellidos" name="user_surname" placeholder="Apellidos">
                <div class="invalid-feedback">Ingrese Apellidos.</div>
              </div>
            </div>
            <div class="form-group">
              <label for="user_job">Ocupacion</label>
              <span class="fas fa-user-tie form-control-feedback"></span>
              <input type="text" value="<?= $user[0]['ocupacion'] ?>" class="form-control" id="ocupacion" name="user_job" placeholder="Ocupacion/Profesion">
              <div class="invalid-feedback">Ingrese Ocupación.</div>
            </div>
            <div class="form-group">
              <label for="user_address">Dirección</label>
              <span class="fas fa-map-marker-alt form-control-feedback"></span>
              <input type="text" value="<?= $user[0]['direccion'] ?>" class="form-control" id="direccion" name="user_address" placeholder="Departamento, Direccion">
              <div class="invalid-feedback">Ingrese Dirección.</div>
            </div>
            <div class="form-row">
              <div class="form-group col-md-6">
                <label for="user_phone">Telefono</label>
                <span class="fas fa-phone form-control-feedback"></span>
                <input type="text" value="<?= $user[0]['telefono'] ?>" class="form-control" id="telefono" name="user_phone" placeholder="Telefono" size="10"  >
                <div class="invalid-feedback">Ingrese Teléfono.</div>
              </div>
              <div class="form-group col-md-6">
                <label for="user_email">Email</label>
                <span class="fas fa-at form-control-feedback"></span>
                <input type="email" value="<?= $user[0]['email'] ?>" class="form-control" id="email" name="user_email" placeholder="Correo electronico" readonly>
                <div class="invalid-feedback">Ingrese Email.</div>
              </div>
            </div>
        </div>
      </div>
    </div>

    <br>

    <div class="row justify-content-center">
      <div class="card card_container">
        <div class="card-header bg-secondary text-white">
          <i class="fas fa-briefcase"></i>  Informacion relevante al emprendimiento
        </div>
        <div class="card-body">

          <div class="form-row">
            <div class="form-group col-md-6">
              <label for="user_project_name"><b>Nombre del Proyecto</b></label>
              <span class="fas fa-project-diagram form-control-feedback"></span>
              <input type="text" value="<?= $user[0]['nombre'] ?>" class="form-control" id="nombre" name="user_proyect_name" placeholder="Ingrese nombre del proyecto"    >
            </div>
          </div>
          <br>

              <label for="bussiness_explication">Explique de forma breve su negocio y/o emprendimiento.</label>
              <span class="span float-right" id="count"><?= $qty_char ?></span>
              <textarea class="form-control" id="resumen" name="bussiness_explication" rows="4" data-limit=<?= $qty_char ?>   ><?= trim($user[0]['resumen']) ?></textarea>
              <br>
              <label for="bussiness_team">Si cuenta con un equipo emprendedor, haga una breve reseña de ellos.</label>
              <span class="span float-right" id="count"><?= $qty_char ?></span>
              <textarea class="form-control" id="equipo" name="bussiness_team" rows="4" data-limit=<?= $qty_char ?>  ><?= trim($user[0]['equipo']) ?></textarea>
              <br>
              <label for="bussiness_diferences">¿Qué hace su negocio y/o emprendimiento sea relevante o diferente de otros existentes?</label>
              <span class="span float-right" id="count"><?= $qty_char ?></span>
              <textarea class="form-control" id="relevancia" name="bussiness_diferences" rows="4" data-limit=<?= $qty_char ?>  ><?= trim($user[0]['relevancia']) ?></textarea>
              <br>
              <label for="bussiness_finances">¿Qué hará con el financiamiento adjudicado de este Concurso?</label>
              <span class="span float-right" id="count"><?= $qty_char ?></span>
              <textarea class="form-control" id="financiamiento" name="bussiness_finances" rows="4" data-limit=<?= $qty_char ?>  ><?= trim($user[0]['financiamiento']) ?></textarea>
              <br>
              <label for="bussiness_finances">¿Quiénes son sus potenciales clientes? breve descripción.</label>
              <span class="span float-right" id="count"><?= $qty_char ?></span>
              <textarea class="form-control" id="clientes" name="bussiness_clients" rows="4" data-limit=<?= $qty_char ?>  ><?= trim($user[0]['clientes']) ?></textarea>
              <br>
              <label for="bussiness_status">¿Cuál es el estado actual de su producto o servicio? Comente.</label>
              <span class="span float-right" id="count"><?= $qty_char ?></span>
              <textarea class="form-control" id="estado" name="bussiness_status" rows="4" data-limit=<?= $qty_char ?>  ><?= trim($user[0]['estado']) ?></textarea>
              <br>
              <label for="bussiness_complementary">INFORMACIÓN COMPLEMENTARIA Y OTROS ASPECTOS QUE QUIERA USTED DESTACAR</label>
              <div class="row">
                  <div class="col-md-7">
                    <span class="span float-right" id="count"><?= $qty_char ?></span>
                    <textarea class="form-control" id="bussiness_complementary" name="bussiness_complementary" rows="6" data-limit=<?= $qty_char ?> rows="4"><?= trim($user[0]['complemento']) ?></textarea>
                  </div>

                  <div class="col-md-5"  id="documentacion">
                    <span class="span float-left">Si desea subir documentos relacionados.</span>
                    <br><br>
                    <input type="hidden" value="<?= $user[0]['path_complemento1']?>" name="path_complemento1">
                    <?php if(file_exists($user[0]['path_complemento1'])){ ?>

                      <table class="text-center">
                        <tr>
                          <td width="50"><i class="fas fa-check-circle text-success" style="font-size:32px;"></i></td>
                          <td width="250">ARCHIVO COMPLEMENTO 1</td>
                          <td>
                            <div class="btn-group" role="group" aria-label="">
                              <a class="btn btn-success" href="<?= $user[0]['path_complemento1'] ?>" data-tt="tooltip" data-placement="top" title="<em>Descargar archivo</em>" download>
                                <i class="fas fa-file-download"></i>
                              </a>
                              <!--
                              <a id="show_file" class="btn btn-primary text-white" data-toggle="modal" data-target="#ver_archivo" data-tt="tooltip" data-placement="top" title="<em>Ver archivo</em>" data-whatever="<?= $user[0]['path_complemento1']?>">
                                <i class="fas fa-eye"></i>
                              </a>
                            -->
                              <a id="delete_file" class="btn btn-danger text-white" data-toggle="modal" data-target="#eliminar_archivo" data-tt="tooltip" data-placement="top" title="<em>Eliminar archivo</em>" data-whatever="<?= $user[0]['path_complemento1']?>">
                                <i class="fas fa-trash-alt"></i>
                              </a>
                            </div>
                          </td>
                        </tr>
                      </table>

                    <?php }else{ ?>

                    <div class="custom-file w-100">
                      <input type="file" class="custom-file-input" id="complement_file1" name="complemento1" aria-describedby="fileHelpBlock">
                      <label class="custom-file-label" for="user_file_identity">Elegir Archivo...</label>
                    </div>
                  <?php } ?>
                    <br><br>

                    <input type="hidden" value="<?= $user[0]['path_complemento2']?>" name="path_complemento2">
                    <?php if(file_exists($user[0]['path_complemento2'])){ ?>

                      <table class="text-center">
                        <tr>
                          <td width="50"><i class="fas fa-check-circle text-success" style="font-size:32px;"></i></td>
                          <td width="250">ARCHIVO COMPLEMENTO 2</td>
                          <td>
                            <div class="btn-group" role="group" aria-label="">
                              <a class="btn btn-success" href="<?= $user[0]['path_complemento2'] ?>" data-tt="tooltip" data-placement="top" title="<em>Descargar archivo</em>" download>
                                <i class="fas fa-file-download"></i>
                              </a>
                              <!--
                              <a id="show_file" class="btn btn-primary text-white" data-toggle="modal" data-target="#ver_archivo" data-tt="tooltip" data-placement="top" title="<em>Ver archivo</em>" data-whatever="<?= $user[0]['path_complemento2']?>">
                                <i class="fas fa-eye"></i>
                              </a>
                            -->
                              <a id="delete_file" class="btn btn-danger text-white" data-toggle="modal" data-target="#eliminar_archivo" data-tt="tooltip" data-placement="top" title="<em>Eliminar archivo</em>" data-whatever="<?= $user[0]['path_complemento2']?>">
                                <i class="fas fa-trash-alt"></i>
                              </a>
                            </div>
                          </td>
                        </tr>
                      </table>

                    <?php }else{ ?>

                    <div class="custom-file w-100">
                      <input type="file" class="custom-file-input" id="complement_file2" name="complemento2" aria-describedby="fileHelpBlock">
                      <label class="custom-file-label" for="user_file_identity">Elegir Archivo...</label>
                      <small id="fileHelpBlock" class="form-text text-muted">
                        Recuerde que los documentos deben estar en formato .doc, .docx, .pdf, .jpg o .png
                      </small>
                    </div>
                  <?php } ?>
                </div>
              </div>

        </div>
      </div>


    </div>


      <br>


    <div class="row justify-content-center">
      <div class="card card_container">
        <div class="card-header">
          <i class="fas fa-file"></i> Documentacion necesaria
        </div>
        <div class="card-body">
              <label for="user_file_identity"><b>1.- Copia de cédula de identidad por ambos lados del postulante.</b></label>
              <div class="custom-file">
                <input type="hidden" value="<?= $user[0]['path_cedula']?>" name="path_cedula">
                <?php if(file_exists($user[0]['path_cedula'])){ ?>

                  <table class="text-center">
                    <tr>
                      <td width="50"><i class="fas fa-check-circle text-success" style="font-size:32px;"></i></td>
                      <td width="250">CEDULA DE IDENTIDAD</td>
                      <td>
                        <div class="btn-group" role="group" aria-label="">
                          <a class="btn btn-success" href="<?= $user[0]['path_cedula'] ?>" data-tt="tooltip" data-placement="top" title="<em>Descargar archivo</em>" download>
                            <i class="fas fa-file-download"></i>
                          </a>
                          <!--
                          <a id="show_file" class="btn btn-primary text-white" data-toggle="modal" data-target="#ver_archivo" data-tt="tooltip" data-placement="top" title="<em>Ver archivo</em>" data-whatever="<?= $user[0]['path_cedula']?>">
                            <i class="fas fa-eye"></i>
                          </a>
                        -->
                          <a id="delete_file" class="btn btn-danger text-white" data-toggle="modal" data-target="#eliminar_archivo" data-tt="tooltip" data-placement="top" title="<em>Eliminar archivo</em>" data-whatever="<?= $user[0]['path_cedula']?>">
                            <i class="fas fa-trash-alt"></i>
                          </a>
                        </div>
                      </td>
                    </tr>
                  </table>

                <?php }else{ ?>
                <input type="file" class="custom-file-input" id="path_cedula" name="cedula" aria-describedby="fileHelpBlock" >
                <label class="custom-file-label" for="user_file_identity">Elegir Archivo...</label>
                <small id="fileHelpBlock" class="form-text text-muted">
                  Recuerde que los documentos deben estar en formato .doc, .docx, .pdf, .jpg o .png
                </small>
                <div class="invalid-feedback">La copia de la cedula es obligatoria.</div>
                <?php } ?>
              </div>

            <br><br>

            <label for="user_file_residence"><b>2.- Certificado de Residencia del postulante para corroborar domicilio en la comuna de Puerto Montt, que se obtienen en las Juntas de Vecinos y/o en Notaría.</b></label>
            <div class="custom-file">
              <input type="hidden" value="<?= $user[0]['path_residencia']?>" name="path_residencia">
              <?php if(file_exists($user[0]['path_residencia'])){ ?>

                <table border="0" class="text-center">
                  <tr>
                    <td width="50"><i class="fas fa-check-circle text-success" style="font-size:32px;"></i></td>
                    <td width="250">CERTIFICADO DE RESIDENCIA</td>
                    <td>
                      <div class="btn-group" role="group" aria-label="">
                          <a class="btn btn-success" href="<?= $user[0]['path_residencia'] ?>" data-tt="tooltip" data-placement="top" title="<em>Descargar archivo</em>" download>
                            <i class="fas fa-file-download"></i>
                          </a>
                          <!--
                          <a id="show_file" class="btn btn-primary text-white" data-toggle="modal" data-target="#ver_archivo" data-tt="tooltip" data-placement="top" title="<em>Ver archivo</em>" data-whatever="<?= $user[0]['path_residencia']?>">
                            <i class="fas fa-eye"></i>
                          </a>
                        -->
                          <a id="delete_file" class="btn btn-danger text-white" data-toggle="modal" data-target="#eliminar_archivo" data-tt="tooltip" data-placement="top" title="<em>Eliminar archivo</em>" data-whatever="<?= $user[0]['path_residencia']?>">
                            <i class="fas fa-trash-alt"></i>
                          </a>
                      </div>
                    </td>
                  </tr>
                </table>



                <?php }else{ ?>
              <input type="file" class="custom-file-input" id="path_residencia" name="residencia" aria-describedby="fileHelpBlock">
              <label class="custom-file-label" for="user_file_residence">Elegir Archivo...</label>
              <small id="fileHelpBlock" class="form-text text-muted">
                Recuerde que los documentos deben estar en formato .doc, .docx, .pdf, .jpg o .png
              </small>
              <div class="invalid-feedback">El certificado de residencia es obligatorio.</div>
              <?php } ?>
            </div>
        </div>

        <div class="btn-group" role="group">
          <button type="submit" class="btn btn-primary btn-lg btn-xlg" onClick='return editSubmit()'><i class="fas fa-share-square"></i> Guardar cambios</button>
        </div>

      </div>
    </div>


    </form>
</div>


<script>
/*
  $(document).ready(function(){
    var val = '<?= $val ?>';
    console.log(val);
    if ( val == ''){
      setTimeout(function() { $('#recordatorio').modal(); }, 1000);
    }

    array = val.split(',');


    for (var i = 0; i < array.length; i++) {
      var id = array[i].substr(4);
      $('#'+id).prop('readonly', false);
      $('#'+id).prop('disabled', false);
    }


  });
  */
</script>
