<div class="modal fade" id="recordatorio" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title"><b><i class="fas fa-exclamation-triangle"></i> Atención!</b></h4>
      </div>
      <div class="modal-body">
        <p>Recuerde que todos los campos estarán bloqueados hasta que un administrador lo revise. De tener algún error o algo incompleto, se le enviará un correo electrónico a <b><?= $user[0]['email'] ?></b> avisándole con anterioridad explicándole que campos deberá corregir.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="eliminar_archivo" tabindex="-1" role="dialog">
  <form action="<?= base_url();?>user/delete_file" method="post">
  <div class="modal-dialog" role="document">
    <div class="modal-content">

        <div class="modal-header">
          <h4 class="modal-title"><b><i class="fas fa-exclamation-triangle"></i> Atención!</b></h4>
        </div>
        <div class="modal-body">
          <input type="hidden" class="form-control" id="recipient-name" name="path_file">
          <p>¿Está seguro de que quiere eliminar el archivo?</p>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary">Aceptar</button>
          <button class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
        </div>
    </div>
  </div>
</form>
</div>

<div class="modal fade" id="ver_archivo" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Vista previa</h4>
      </div>
        <div class="modal-body">
          <input type="hidden" class="form-control" id="recipient-name" name="path_file">
          <img src="" class="w-100">

        </div>
        <div class="modal-footer">
          <button class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        </div>
    </div>
  </div>
</div>

<script>

$('#eliminar_archivo').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget)
  var recipient = button.data('whatever')
  var modal = $(this)
  modal.find('.modal-body input').val(recipient)
})

$('#ver_archivo').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget)
  var recipient = button.data('whatever')
  var modal = $(this)
  modal.find('.modal-body img').attr("src", recipient)
})
</script>
