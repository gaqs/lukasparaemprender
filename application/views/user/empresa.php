<?php
$qty_char = $this->config->item('qty_char');
$disabled = '';
//Determina cuales key en array tienen valor 0
$array_val = array();
foreach ($user[0] as $key => $value) {
  $pos = strpos( $key, 'val_' );
  if( $pos == 'false' && $value == '0'){
      array_push($array_val, $key);
  }
}
$val = implode(',',$array_val);
/*
if(empty($array_val)){
  $disabled = 'disabled';
}
*/
?>

<div class="container fix_header">

<div class="row justify-content-center">
    <div class="alert alert-warning card_container" role="alert">
      <h3 class="alert-heading"><b><i class="fas fa-lightbulb"></i> Recordatorio</b></h3>
      <p>La información que se proporcione en el formulario se administrará bajo el concepto de absoluta confidencialidad y será revisado sólo por el equipo autorizado de trabajo de la Ilustre Municipalidad de Puerto Montt y la Universidad de Los Lagos. Se garantiza absoluta reserva y discreción de la información proporcionada. </p>
      <hr>
      <p class="mb-0">Recuerde que al terminar debe presionar el botón <b>GUARDAR CAMBIOS</b>, al final de la página.  </p>
    </div>
  </div>

  <form class="needs-validation" novalidate action="user/edit_empresa" method="post" enctype="multipart/form-data">
    <input type="hidden" value="<?= $user[0]['id'] ?>" class="form-control" id="id" name="user_id">
    <div class="row justify-content-center">
      <div class="card card_container">
        <div class="card-header bg-secondary text-white">
          <i class="fas fa-pen-square"></i>  Informacion básica
        </div>
        <div class="card-body">
            <div class="form-row">
              <div class="form-group col-md-4">
                <label for="user_rut">RUT</label>
                <span class="fas fa-id-card form-control-feedback"></span>
                <input type="text" value="<?= $user[0]['rut'] ?>" class="form-control" id="rut" name="user_rut" placeholder="RUT" placeholder=""  oninput="checkRut(this)" readonly>
                <div class="invalid-feedback">RUT incorrecto.</div>
              </div>

              <div class="form-group col-md-4">
                <label for="user_name">Nombres</label>
                <span class="fas fa-user form-control-feedback"></span>
                <input type="text" value="<?= $user[0]['nombres'] ?>" class="form-control" id="nombres" name="user_name" placeholder="Nombre"  >
                <div class="invalid-feedback">Ingrese Nombre.</div>
              </div>

              <div class="form-group col-md-4">
                <label for="user_surname">Apellidos</label>
                <span class="fas fa-user form-control-feedback"></span>
                <input type="text" value="<?= $user[0]['apellidos'] ?>" class="form-control" id="apellidos" name="user_surname" placeholder="Apellidos"  >
                <div class="invalid-feedback">Ingrese Apellidos.</div>
              </div>
            </div>
            <div class="form-group">
              <label for="user_job">Ocupacion</label>
              <span class="fas fa-user-tie form-control-feedback"></span>
              <input type="text" value="<?= $user[0]['ocupacion'] ?>" class="form-control" id="ocupacion" name="user_job" placeholder="Ocupacion/Profesion"  >
              <div class="invalid-feedback">Ingrese Ocupación.</div>
            </div>
            <div class="form-row">
              <div class="form-group col-md-6">
                <label for="user_phone">Empresa</label>
                <span class="fas fa-building form-control-feedback"></span>
                <input type="text" value="<?= $user[0]['empresa'] ?>" class="form-control" id="empresa" name="user_business" placeholder="Nombre de la empresa" size="10"  >
                <div class="invalid-feedback">Ingrese Empresa.</div>
              </div>
              <div class="form-group col-md-6">
                <label for="user_email">Cargo</label>
                <span class="fas fa-user-md form-control-feedback"></span>
                <input type="text" value="<?= $user[0]['cargo'] ?>" class="form-control" id="cargo" name="user_position" placeholder="Cargo dentro de la empresa"  >
                <div class="invalid-feedback">Ingrese cargo dentro de la empresa.</div>
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col-md-5">
                <label for="user_phone">Telefono</label>
                <span class="fas fa-phone form-control-feedback"></span>
                <input type="text" value="<?= $user[0]['telefono'] ?>" class="form-control" id="telefono" name="user_phone" placeholder="Telefono" size="10"  >
                <div class="invalid-feedback">Ingrese Teléfono.</div>
              </div>
              <div class="form-group col-md-7">
                <label for="user_email">Email</label>
                <span class="fas fa-at form-control-feedback"></span>
                <input type="email" value="<?= $user[0]['email'] ?>" class="form-control" id="email" name="user_email" placeholder="Correo electronico" readonly>
                <div class="invalid-feedback">Ingrese Email.</div>
              </div>
            </div>
        </div>
      </div>
    </div>

    <br>

    <div class="row justify-content-center">
      <div class="card card_container">
        <div class="card-header bg-secondary text-white">
          <i class="fas fa-building"></i>  Datos de la Empresa
        </div>
        <div class="card-body">
            <div class="form-row">
              <div class="form-group col-md-4">
                <label for="user_rut">RUT empresa</label>
                <span class="fas fa-id-card form-control-feedback"></span>
                <input type="text" value="<?= $user[0]['rut_empresa'] ?>" class="form-control" id="rut_empresa" name="business_rut" placeholder="RUT" placeholder=""  >
                <div class="invalid-feedback">RUT incorrecto.</div>
              </div>

              <div class="form-group col-md-5">
                <label for="user_name">Razon Social</label>
                <span class="fas fa-file-signature form-control-feedback"></span>
                <input type="text" value="<?= $user[0]['razon_social'] ?>" class="form-control" id="razon_social" name="business_name" placeholder="Nombre"  >
                <div class="invalid-feedback">Ingrese Nombre empresa</div>
              </div>

              <div class="form-group col-md-3">
                <label for="user_surname">Fecha Inicio Actividades</label>
                <span class="fas fa-calendar-alt form-control-feedback"></span>
                <input type="date" value="<?= $user[0]['inicio'] ?>"  class="form-control" id="inicio" name="business_date" placeholder=""  >
                <div class="invalid-feedback">Ingrese fecha inicio actividades.</div>
              </div>
            </div>
            <div class="form-group">
              <label for="user_job">Dirección</label>
              <span class="fas fa-map-marker-alt form-control-feedback"></span>
              <input type="text" value="<?= $user[0]['direccion_empresa'] ?>" class="form-control" id="direccion_empresa" name="business_address" placeholder="Direccion empresa"  >
              <div class="invalid-feedback">Ingrese Direccion.</div>
            </div>
            <div class="form-row">
              <div class="form-group col-md-5">
                <label for="user_phone">Teléfono</label>
                <span class="fas fa-phone form-control-feedback"></span>
                <input type="text" value="<?= $user[0]['telefono_empresa'] ?>" class="form-control" id="telefono_empresa" name="business_phone" placeholder="Telefono empresa" size="10"  >
                <div class="invalid-feedback">Ingrese Telefono.</div>
              </div>
              <div class="form-group col-md-7">
                <label for="user_email">Página web</label>
                <span class="fas fa-desktop form-control-feedback"></span>
                <input type="text" value="<?= $user[0]['web'] ?>" class="form-control" id="web" name="business_web" placeholder="Pagina web" >
                <div class="invalid-feedback"></div>
              </div>
            </div>
        </div>
      </div>
    </div>

    <br>

    <div class="row justify-content-center">
      <div class="card card_container">
        <div class="card-header bg-secondary text-white">
          <i class="fas fa-briefcase"></i>  Información relevante al emprendimiento
        </div>
        <div class="card-body">
          <div class="form-row">
            <div class="form-group col-md-6">
              <label for="user_project_name"><b>Nombre del Proyecto</b></label>
              <span class="fas fa-project-diagram form-control-feedback"></span>
              <input type="text" value="<?= $user[0]['nombre'] ?>" class="form-control" id="nombre" name="user_proyect_name" placeholder="Ingrese nombre del proyecto"  >
            </div>
          </div>
          <br>
              <label for="bussiness_explication">1.- Explique de forma breve a qué se dedica su empresa y cuál es su idea para ella.</label>
              <span class="span float-right" id="count"><?= $qty_char ?></span>
              <textarea class="form-control" id="explicacion" name="bussiness_explication" rows="4" data-limit=<?= $qty_char ?>  ><?= $user[0]['explicacion'] ?></textarea>
              <br>
              <label for="bussiness_team">2.- Si cuenta con un equipo emprendedor, haga una breve reseña de ellos</label>
              <span class="span float-right" id="count"><?= $qty_char ?></span>
              <textarea class="form-control" id="equipo" name="bussiness_team" rows="4" data-limit=<?= $qty_char ?>  ><?= $user[0]['equipo'] ?></textarea>
              <br>
              <label for="bussiness_diferences">3.- ¿Qué hará con el financiamiento adjudicado de este Concurso?</label>
              <span class="span float-right" id="count"><?= $qty_char ?></span>
              <textarea class="form-control" id="financiamiento" name="bussiness_finances" rows="4" data-limit=<?= $qty_char ?>  ><?= $user[0]['financiamiento'] ?></textarea>
              <br>
              <label for="bussiness_finances">4.- ¿Quiénes son sus potenciales clientes? breve descripción</label>
              <span class="span float-right" id="count"><?= $qty_char ?></span>
              <textarea class="form-control" id="clientes" name="bussiness_clients" rows="4" data-limit=<?= $qty_char ?>  ><?= $user[0]['clientes'] ?></textarea>
              <br>
              <label for="bussiness_commercialization">5.- ¿Cuenta con un sistema de comercialización?.</label>
              <span class="span float-right" id="count"><?= $qty_char ?></span>
              <textarea class="form-control" id="clientes" name="bussiness_commercialization" rows="4" data-limit=<?= $qty_char ?>  ><?= $user[0]['comercializacion'] ?></textarea>
              <br>
              <label for="bussiness_status">6.- ¿Quiénes son su competencia?.</label>
              <span class="span float-right" id="count"><?= $qty_char ?></span>
              <textarea class="form-control" id="competencia" name="bussiness_competition" rows="4" data-limit=<?= $qty_char ?>  ><?= $user[0]['competencia'] ?></textarea>
              <br>
              <label for="bussiness_projection">7.- Cuáles son sus Proyecciones de ventas esperadas para el próximo año (2020), en caso de ser ganador. </label>
              <span class="span float-right" id="count"><?= $qty_char ?></span>
              <textarea class="form-control" id="proyecciones" name="bussiness_projection" rows="4" data-limit=<?= $qty_char ?>  ><?= $user[0]['proyecciones'] ?></textarea>
              <br>
              <label for="bussiness_complementary">INFORMACIÓN COMPLEMENTARIA Y OTROS ASPECTOS QUE QUIERA USTED DESTACAR</label>
              <div class="row">
                  <div class="col-md-7">
                    <span class="span float-right" id="count"><?= $qty_char ?></span>
                    <textarea class="form-control" id="bussiness_complementary" name="bussiness_complementary" rows="6" data-limit=<?= $qty_char ?> rows="4"><?= trim($user[0]['complemento']) ?></textarea>
                  </div>

                  <div class="col-md-5"  id="documentacion">
                    <span class="span float-left">Si desea subir documentos relacionados.</span>
                    <br><br>
                    <input type="hidden" value="<?= $user[0]['path_complemento1']?>" name="path_complemento1">
                    <?php if(file_exists($user[0]['path_complemento1'])){ ?>

                      <table class="text-center">
                        <tr>
                          <td width="50"><i class="fas fa-check-circle text-success" style="font-size:32px;"></i></td>
                          <td width="250">ARCHIVO COMPLEMENTO 1</td>
                          <td>
                            <div class="btn-group" role="group" aria-label="">
                              <a class="btn btn-success" href="<?= $user[0]['path_complemento1'] ?>" data-tt="tooltip" data-placement="top" title="<em>Descargar archivo</em>" download>
                                <i class="fas fa-file-download"></i>
                              </a>
                              <!--
                              <a id="show_file" class="btn btn-primary text-white" data-toggle="modal" data-target="#ver_archivo" data-tt="tooltip" data-placement="top" title="<em>Ver archivo</em>" data-whatever="<?= $user[0]['path_complemento1']?>">
                                <i class="fas fa-eye"></i>
                              </a>
                            -->
                              <a id="delete_file" class="btn btn-danger text-white" data-toggle="modal" data-target="#eliminar_archivo" data-tt="tooltip" data-placement="top" title="<em>Eliminar archivo</em>" data-whatever="<?= $user[0]['path_complemento1']?>">
                                <i class="fas fa-trash-alt"></i>
                              </a>
                            </div>
                          </td>
                        </tr>
                      </table>

                    <?php }else{ ?>

                    <div class="custom-file w-100">
                      <input type="file" class="custom-file-input" id="complement_file1" name="complemento1" aria-describedby="fileHelpBlock">
                      <label class="custom-file-label" for="user_file_identity">Elegir Archivo...</label>
                    </div>
                  <?php } ?>
                    <br><br>

                    <input type="hidden" value="<?= $user[0]['path_complemento2']?>" name="path_complemento2">
                    <?php if(file_exists($user[0]['path_complemento2'])){ ?>

                      <table class="text-center">
                        <tr>
                          <td width="50"><i class="fas fa-check-circle text-success" style="font-size:32px;"></i></td>
                          <td width="250">ARCHIVO COMPLEMENTO 2</td>
                          <td>
                            <div class="btn-group" role="group" aria-label="">
                              <a class="btn btn-success" href="<?= $user[0]['path_complemento2'] ?>" data-tt="tooltip" data-placement="top" title="<em>Descargar archivo</em>" download>
                                <i class="fas fa-file-download"></i>
                              </a>
                              <!--
                              <a id="show_file" class="btn btn-primary text-white" data-toggle="modal" data-target="#ver_archivo" data-tt="tooltip" data-placement="top" title="<em>Ver archivo</em>" data-whatever="<?= $user[0]['path_complemento2']?>">
                                <i class="fas fa-eye"></i>
                              </a>
                            -->
                              <a id="delete_file" class="btn btn-danger text-white" data-toggle="modal" data-target="#eliminar_archivo" data-tt="tooltip" data-placement="top" title="<em>Eliminar archivo</em>" data-whatever="<?= $user[0]['path_complemento2']?>">
                                <i class="fas fa-trash-alt"></i>
                              </a>
                            </div>
                          </td>
                        </tr>
                      </table>

                    <?php }else{ ?>

                    <div class="custom-file w-100">
                      <input type="file" class="custom-file-input" id="complement_file2" name="complemento2" aria-describedby="fileHelpBlock">
                      <label class="custom-file-label" for="user_file_identity">Elegir Archivo...</label>
                      <small id="fileHelpBlock" class="form-text text-muted">
                        Recuerde que los documentos deben estar en formato .doc, .docx, .pdf, .jpg o .png
                      </small>
                    </div>
                  <?php } ?>
                </div>
              </div>


        </div>
      </div>


    </div>


      <br>


    <div class="row justify-content-center">
      <div class="card card_container">
        <div class="card-header bg-secondary text-white">
          <i class="fas fa-file"></i> Documentación necesaria
        </div>
        <div class="card-body">
              <label for="user_file_identity"><b>1.- Copia de cédula de identidad por ambos lados del postulante.</b></label>
              <div class="custom-file">
                <input type="hidden" value="<?= $user[0]['path_cedula']?>" name="path_cedula">
                <?php if(file_exists($user[0]['path_cedula'])){ ?>

                  <table class="text-center">
                    <tr>
                      <td width="50"><i class="fas fa-check-circle text-success" style="font-size:32px;"></i></td>
                      <td width="250">CEDULA DE IDENTIDAD</td>
                      <td>
                        <div class="btn-group" role="group" aria-label="">
                          <a class="btn btn-success" href="<?= $user[0]['path_cedula'] ?>" data-tt="tooltip" data-placement="top" title="<em>Descargar archivo</em>" download>
                            <i class="fas fa-file-download"></i>
                          </a>
                          <!--
                          <a id="show_file" class="btn btn-primary text-white" data-toggle="modal" data-target="#ver_archivo" data-tt="tooltip" data-placement="top" title="<em>Ver archivo</em>" data-whatever="<?= $user[0]['path_cedula']?>">
                            <i class="fas fa-eye"></i>
                          </a>
                        -->
                          <a id="delete_file" class="btn btn-danger text-white" data-toggle="modal" data-target="#eliminar_archivo" data-tt="tooltip" data-placement="top" title="<em>Eliminar archivo</em>" data-whatever="<?= $user[0]['path_cedula']?>">
                            <i class="fas fa-trash-alt"></i>
                          </a>
                        </div>
                      </td>
                    </tr>
                  </table>

                <?php }else{ ?>
                <input type="file" class="custom-file-input" id="path_cedula" name="cedula" aria-describedby="fileHelpBlock" >
                <label class="custom-file-label" for="user_file_identity">Elegir Archivo...</label>
                <small id="fileHelpBlock" class="form-text text-muted">
                  Recuerde que los documentos deben estar en formato .doc, .docx, .pdf, .jpg o .png
                </small>
                <div class="invalid-feedback">La copia de la cedula es obligatoria.</div>
                <?php } ?>
              </div>

            <br><br>

            <label for="user_file_residence"><b>2.- Copia Rol Único Tributario.</b></label><br>
            <div class="custom-file">
              <input type="hidden" value="<?= $user[0]['path_rol']?>" name="path_rol">
              <?php if(file_exists($user[0]['path_rol'])){ ?>

                <table class="text-center">
                  <tr>
                    <td width="50"><i class="fas fa-check-circle text-success" style="font-size:32px;"></i></td>
                    <td width="250">ROL UNICO TRIBUTARIO</td>
                    <td>
                      <div class="btn-group" role="group" aria-label="">
                        <a class="btn btn-success" href="<?= $user[0]['path_rol'] ?>" data-tt="tooltip" data-placement="top" title="<em>Descargar archivo</em>" download>
                          <i class="fas fa-file-download"></i>
                        </a>
                        <!--
                        <a id="show_file" class="btn btn-primary text-white" data-toggle="modal" data-target="#ver_archivo" data-tt="tooltip" data-placement="top" title="<em>Ver archivo</em>" data-whatever="<?= $user[0]['path_rol']?>">
                          <i class="fas fa-eye"></i>
                        </a>
                      -->
                        <a id="delete_file" class="btn btn-danger text-white" data-toggle="modal" data-target="#eliminar_archivo" data-tt="tooltip" data-placement="top" title="<em>Eliminar archivo</em>" data-whatever="<?= $user[0]['path_rol']?>">
                          <i class="fas fa-trash-alt"></i>
                        </a>
                      </div>
                    </td>
                  </tr>
                </table>

              <?php }else{ ?>
              <input type="file" class="custom-file-input" id="path_rol" name="rol" aria-describedby="fileHelpBlock" >
              <label class="custom-file-label" for="user_file_identity">Elegir Archivo...</label>
              <small id="fileHelpBlock" class="form-text text-muted">
                Recuerde que los documentos deben estar en formato .doc, .docx, .pdf, .jpg o .png
              </small>
              <div class="invalid-feedback">La copia de la cedula es obligatoria.</div>
              <?php } ?>
            </div>

            <br><br>
            <label for="user_file_identity"><b>3.- Copia Declaración de Impuesto a la Renta 2018 según atañe.</b></label>
            <div class="custom-file">
              <input type="hidden" value="<?= $user[0]['path_renta']?>" name="path_renta">
              <?php if(file_exists($user[0]['path_renta'])){ ?>

                <table class="text-center">
                  <tr>
                    <td width="50"><i class="fas fa-check-circle text-success" style="font-size:32px;"></i></td>
                    <td width="250">DECLARACION DE IMPUESTO</td>
                    <td>
                      <div class="btn-group" role="group" aria-label="">
                        <a class="btn btn-success" href="<?= $user[0]['path_renta'] ?>" data-tt="tooltip" data-placement="top" title="<em>Descargar archivo</em>" download>
                          <i class="fas fa-file-download"></i>
                        </a>
                        <!--
                        <a id="show_file" class="btn btn-primary text-white" data-toggle="modal" data-target="#ver_archivo" data-tt="tooltip" data-placement="top" title="<em>Ver archivo</em>" data-whatever="<?= $user[0]['path_renta']?>">
                          <i class="fas fa-eye"></i>
                        </a>
                      -->
                        <a id="delete_file" class="btn btn-danger text-white" data-toggle="modal" data-target="#eliminar_archivo" data-tt="tooltip" data-placement="top" title="<em>Eliminar archivo</em>" data-whatever="<?= $user[0]['path_renta']?>">
                          <i class="fas fa-trash-alt"></i>
                        </a>
                      </div>
                    </td>
                  </tr>
                </table>

              <?php }else{ ?>
              <input type="file" class="custom-file-input" id="path_renta" name="renta" aria-describedby="fileHelpBlock" >
              <label class="custom-file-label" for="user_file_identity">Elegir Archivo...</label>
              <small id="fileHelpBlock" class="form-text text-muted">
                Recuerde que los documentos deben estar en formato .doc, .docx, .pdf, .jpg o .png
              </small>
              <div class="invalid-feedback">La copia de la cedula es obligatoria.</div>
              <?php } ?>
            </div>

          <br><br>
          <label for="user_file_identity"><b>4.- Copia Patente Municipal del último semestre.</b></label><br>
          <div class="custom-file">
            <input type="hidden" value="<?= $user[0]['path_patente']?>" name="path_patente">
            <?php if(file_exists($user[0]['path_patente'])){ ?>

              <table class="text-center">
                <tr>
                  <td width="50"><i class="fas fa-check-circle text-success" style="font-size:32px;"></i></td>
                  <td width="250">PATENTE MUNICIPAL</td>
                  <td>
                    <div class="btn-group" role="group" aria-label="">
                      <a class="btn btn-success" href="<?= $user[0]['path_patente'] ?>" data-tt="tooltip" data-placement="top" title="<em>Descargar archivo</em>" download>
                        <i class="fas fa-file-download"></i>
                      </a>
                      <!--
                      <a id="show_file" class="btn btn-primary text-white" data-toggle="modal" data-target="#ver_archivo" data-tt="tooltip" data-placement="top" title="<em>Ver archivo</em>" data-whatever="<?= $user[0]['path_patente']?>">
                        <i class="fas fa-eye"></i>
                      </a>
                    -->
                      <a id="delete_file" class="btn btn-danger text-white" data-toggle="modal" data-target="#eliminar_archivo" data-tt="tooltip" data-placement="top" title="<em>Eliminar archivo</em>" data-whatever="<?= $user[0]['path_patente']?>">
                        <i class="fas fa-trash-alt"></i>
                      </a>
                    </div>
                  </td>
                </tr>
              </table>

            <?php }else{ ?>
            <input type="file" class="custom-file-input" id="path_patente" name="patente" aria-describedby="fileHelpBlock" >
            <label class="custom-file-label" for="user_file_identity">Elegir Archivo...</label>
            <small id="fileHelpBlock" class="form-text text-muted">
              Recuerde que los documentos deben estar en formato .doc, .docx, .pdf, .jpg o .png
            </small>
            <div class="invalid-feedback">La copia de la cedula es obligatoria.</div>
            <?php } ?>
          </div>

        <br><br>
        <label for="user_file_identity"><b>5.- Copia otros permisos de operación del negocio de acuerdo a su naturaleza (ejemplo resolución sanitaria, permisos sectoriales, etc.)</b></label>
        <div class="custom-file">
          <input type="hidden" value="<?= $user[0]['path_otrospermisos']?>" name="path_otrospermisos">
          <?php if(file_exists($user[0]['path_otrospermisos'])){ ?>

            <table class="text-center">
              <tr>
                <td width="50"><i class="fas fa-check-circle text-success" style="font-size:32px;"></i></td>
                <td width="250">OTROS PERMISOS DE OPERACION</td>
                <td>
                  <div class="btn-group" role="group" aria-label="">
                    <a class="btn btn-success" href="<?= $user[0]['path_otrospermisos'] ?>" data-tt="tooltip" data-placement="top" title="<em>Descargar archivo</em>" download>
                      <i class="fas fa-file-download"></i>
                    </a>
                    <!--
                    <a id="show_file" class="btn btn-primary text-white" data-toggle="modal" data-target="#ver_archivo" data-tt="tooltip" data-placement="top" title="<em>Ver archivo</em>" data-whatever="<?= $user[0]['path_otrospermisos']?>">
                      <i class="fas fa-eye"></i>
                    </a>
                  -->
                    <a id="delete_file" class="btn btn-danger text-white" data-toggle="modal" data-target="#eliminar_archivo" data-tt="tooltip" data-placement="top" title="<em>Eliminar archivo</em>" data-whatever="<?= $user[0]['path_otrospermisos']?>">
                      <i class="fas fa-trash-alt"></i>
                    </a>
                  </div>
                </td>
              </tr>
            </table>

          <?php }else{ ?>
          <input type="file" class="custom-file-input" id="path_otrospermisos" name="otrospermisos" aria-describedby="fileHelpBlock" >
          <label class="custom-file-label" for="user_file_identity">Elegir Archivo...</label>
          <small id="fileHelpBlock" class="form-text text-muted">
            Recuerde que los documentos deben estar en formato .doc, .docx, .pdf, .jpg o .png
          </small>
          <div class="invalid-feedback">La copia de la cedula es obligatoria.</div>
          <?php } ?>
        </div>

      <br><br>
      <label for="user_file_identity"><b>6.- Boleta de Agua, Luz o Gas ORIGINAL (una de las tres) que esté a nombre del POSTULANTE con una antigüedad máxima de tres meses, donde se identifique el domicilio particular o donde se desarrollará el emprendimiento. </b></label>
      <div class="custom-file">
        <input type="hidden" value="<?= $user[0]['path_boleta']?>" name="path_boleta">
        <?php if(file_exists($user[0]['path_boleta'])){ ?>

          <table class="text-center">
            <tr>
              <td width="50"><i class="fas fa-check-circle text-success" style="font-size:32px;"></i></td>
              <td width="250">BOLETA ORIGINAL</td>
              <td>
                <div class="btn-group" role="group" aria-label="">
                  <a class="btn btn-success" href="<?= $user[0]['path_boleta'] ?>" data-tt="tooltip" data-placement="top" title="<em>Descargar archivo</em>" download>
                    <i class="fas fa-file-download"></i>
                  </a>
                  <!--
                  <a id="show_file" class="btn btn-primary text-white" data-toggle="modal" data-target="#ver_archivo" data-tt="tooltip" data-placement="top" title="<em>Ver archivo</em>" data-whatever="<?= $user[0]['path_boleta']?>">
                    <i class="fas fa-eye"></i>
                  </a>
                -->
                  <a id="delete_file" class="btn btn-danger text-white" data-toggle="modal" data-target="#eliminar_archivo" data-tt="tooltip" data-placement="top" title="<em>Eliminar archivo</em>" data-whatever="<?= $user[0]['path_boleta']?>">
                    <i class="fas fa-trash-alt"></i>
                  </a>
                </div>
              </td>
            </tr>
          </table>

        <?php }else{ ?>
        <input type="file" class="custom-file-input" id="path_boleta" name="boleta" aria-describedby="fileHelpBlock" >
        <label class="custom-file-label" for="user_file_identity">Elegir Archivo...</label>
        <small id="fileHelpBlock" class="form-text text-muted">
          Recuerde que los documentos deben estar en formato .doc, .docx, .pdf, .jpg o .png
        </small>
        <div class="invalid-feedback">La copia de la cedula es obligatoria.</div>
        <?php } ?>
      </div>

    <br><br>

        </div>

        <div class="btn-group" role="group">
          <button type="submit" class="btn btn-primary btn-lg btn-xlg" onClick='return editSubmit()'><i class="fas fa-share-square"></i> Guardar cambios</button>
        </div>


      </div>
    </div>


    </form>
</div>


<script>
/*
  $(document).ready(function(){
    var val = '<?= $val ?>';
    if ( val == ''){
      setTimeout(function() { $('#recordatorio').modal(); }, 1000);
    }

    array = val.split(',');

    for (var i = 0; i < array.length; i++) {
      var id = array[i].substr(4);
      $('#'+id).prop('readonly', false).addClass('border-primary');
      $('#'+id).prop('disabled', false);
    }
  });
  */
</script>
