<?php $this->load->helper('cookie'); ?>
<div class="container fix_header">
  <div class="row justify-content-center">
    <div class="col-md-5">
      <div class="card">
        <div class="card-body">
          <form class="needs-validation" action="<?= base_url();?>login/user_access" method="post" style="width:100%;"  novalidate >
            <h1 class="h3 mb-3">Iniciar sesión</h1>
            <hr>

            <div class="form-group">
              <label for="user_email">Correo electrónico</label>
              <span class="fas fa-at form-control-feedback"></span>
              <input type="email" class="form-control" id="registered_email" placeholder="nombre@dominio.com" name="email" required value="<?php if (get_cookie('uemail')) { echo get_cookie('uemail'); } ?>">
              <div class="invalid-feedback">Ingrese correo electrónico</div>
            </div>
            <div class="form-group">
              <label for="user_email">Contraseña</label>
              <span class="fas fa-key form-control-feedback"></span>
              <input type="password" class="form-control" id="user_pass" placeholder="Contraseña" name="pass" required value="<?php if (get_cookie('upassword')) { echo get_cookie('upassword'); } ?>">
              <div class="invalid-feedback">Ingrese contraseña</div>
            </div>
            <div class="custom-control custom-checkbox mb-3">
                <input type="checkbox" class="custom-control-input" id="user_remember" name="remember" <?php if (get_cookie('uremember')) { echo 'checked'; } ?>>
                <label class="custom-control-label" for="user_remember">Recordarme</label>
            </div>
            <button type="submit" class="btn btn-primary w-100">Iniciar sesión <i class="fas fa-sign-in-alt"></i></button>
            <br><br>
            <small>
              <center>
                <p>¿No tienes una cuenta aún? <a href="<?= base_url();?>register">Regístrate aquí</a>.</p>
                <p>¿Olvidaste tu contraseña? <a href="<?= base_url();?>register/forgot">Click aquí</a>.</p>
                <p class="mt-5 mb-3 text-muted">Municipalidad de Puerto Montt - 2019</p>
              </center>
            </small>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
  $(document).ready(function(){ $('#login_select1').click(); });
</script>
