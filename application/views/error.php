<div class="container text-align fix_header">
  <div class="row justify-content-center">
    <div class="col-md-8">

      <?php
      $nerror = $this->input->get('error');
      if( $nerror == '01' ){ ?>

        <div class="card w-100 text-center error_card">
          <h5 class="card-header bg-danger"><i class="far fa-times-circle"></i></h5>
          <div class="card-body">
            <h5 class="card-title">ERROR!</h5>
            <p class="card-text">El usuario ya existe. Pruebe iniciando sesión con los datos que enviamos a su correo electrónico.</p>
            <a href="<?= base_url(); ?>" class="btn btn-danger">Volver al Home</a>
          </div>
        </div>

      <?php }else if( $nerror == '02'){ ?>

        <div class="card w-100 text-center error_card">
          <h5 class="card-header bg-danger"><i class="fas fa-thumbs-down"></i></h5>
          <div class="card-body">
            <h5 class="card-title">ERROR CAMBIO DE CONTRASEÑA</h5>
            <p class="card-text">Puede que la contraseña temporal este mal ingresada o que haya vencido el tiempo para el cambio.</p>
            <a href="#" class="btn btn-danger">Volver atrás</a>
            <a href="<?= base_url(); ?>login" class="btn btn-danger">Volver a enviar correo</a>
          </div>
        </div>

      <?php }else if( $nerror == '03'){ ?>

        <div class="card w-100 text-center error_card">
          <h5 class="card-header bg-danger"><i class="fas fa-thumbs-down"></i></h5>
          <div class="card-body">
            <h5 class="card-title">ERROR DE VALIDACIÓN!</h5>
            <p class="card-text">El hash de validación no existe. Le enviamos un correo con para realizar el proceso.</p>
            <a href="<?= base_url(); ?>" class="btn btn-danger">Volver al Home</a>
          </div>
        </div>

      <?php }else if( $nerror == '04'){ ?>

        <div class="card w-100 text-center error_card">
          <h5 class="card-header bg-danger"><i class="far fa-times-circle"></i></h5>
          <div class="card-body">
            <h5 class="card-title">ERROR!</h5>
            <p class="card-text">El usuario y/o contraseña son incorrectos. Vuelva a intentar.</p>
            <a href="<?= base_url(); ?>login/user" class="btn btn-danger">Volver a Logearse</a>
          </div>
        </div>

      <?php }else if( $nerror == '05'){ ?>

        <div class="card w-100 text-center error_card">
          <h5 class="card-header bg-danger"><i class="far fa-times-circle"></i></h5>
          <div class="card-body">
            <h5 class="card-title">ERROR ADMINISTRADOR!</h5>
            <p class="card-text">El usuario y/o contraseña son incorrectos. Vuelva a intentar.</p>
            <a href="<?= base_url(); ?>login" class="btn btn-danger">Volver a Logearse</a>
          </div>
        </div>

      <?php }else if( $nerror == '06'){ ?>

        <div class="card w-100 text-center error_card">
          <h5 class="card-header bg-danger"><i class="fas fa-thumbs-down"></i></h5>
          <div class="card-body">
            <h5 class="card-title">AUN NO VALIDA SU CORREO</h5>
            <p class="card-text">Su correo electrónico aun no ha sido validado, por favor, revise su casilla de correo.</p>
            <a href="<?= base_url(); ?>register/reenviar" class="btn btn-danger">Reenviar correo de validación</a>
            <a href="<?= base_url(); ?>" class="btn btn-danger">Volver al Home</a>
          </div>
        </div>

      <?php }else if( $nerror == '07'){ ?>

        <div class="card w-100 text-center error_card">
          <h5 class="card-header bg-danger"><i class="fas fa-thumbs-down"></i></h5>
          <div class="card-body">
            <h5 class="card-title">USUARIO YA VALIDADO!</h5>
            <p class="card-text">Usuario previamente validado, por favor, pruebe iniciando sesión.</p>
            <a href="<?= base_url(); ?>" class="btn btn-danger">Volver al Home</a>
            <a href="<?= base_url(); ?>login/user" class="btn btn-danger">Iniciar sesión</a>
          </div>
        </div>



    <?php }else{ ?>

      <div class="card w-100 text-center error_card">
        <h5 class="card-header bg-danger"><i class="far fa-times-circle"></i></h5>
        <div class="card-body">
          <h5 class="card-title">ERROR!</h5>
          <p class="card-text">Ha ocurrido un error inesperado. Vuelva a intentar o consulte al Administrador.</p>
          <a href="<?= base_url(); ?>" class="btn btn-danger">Volver al Home</a>
        </div>
      </div>

    <?php } ?>


    </div>
  </div>

</div>
