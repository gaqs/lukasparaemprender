<div class="container text-align fix_header">
  <div class="row justify-content-center">
    <div class="col-md-9">

      <?php
      $exito = $this->input->get('exito');
      if( $exito == '01' ){ ?>

        <div class="card w-100 text-center success_card">
          <h5 class="card-header bg-success"><i class="far fa-check-circle"></i></h5>
          <div class="card-body">
            <h5 class="card-title">MUCHAS GRACIAS POR REGISTRARSE!</h5>
            <p class="card-text">Aun falta un paso!<br> Para concluir con el registro, debe validar su correo electronico, solo siga las instrucciones del correo que le hemos enviado.</p>
            <a href="<?= base_url(); ?>" class="btn btn-success">Volver a la página principal</a>
          </div>
        </div>

      <?php }else if( $exito == '02'){ ?>

        <div class="card w-100 text-center success_card">
          <h5 class="card-header bg-success"><i class="far fa-check-circle"></i></h5>
          <div class="card-body">
            <h5 class="card-title">EXITO!</h5>
            <p class="card-text">Usuario eliminado correctamente.</p>
            <a href="<?= base_url(); ?>admin" class="btn btn-success">Volver a la página de Administrador</a>
          </div>
        </div>

      <?php }else if( $exito == '03'){ ?>

        <div class="card w-100 text-center success_card">
          <h5 class="card-header bg-success"><i class="far fa-check-circle"></i></h5>
          <div class="card-body">
            <h5 class="card-title">EXITO!</h5>
            <p class="card-text">Usuario editado correctamente.</p>
            <a href="<?= base_url(); ?>admin" class="btn btn-success">Volver a la página de Administrador</a>
          </div>
        </div>

      <?php }else if( $exito == '04'){ ?>

        <div class="card w-100 text-center success_card">
          <h5 class="card-header bg-success"><i class="far fa-check-circle"></i></h5>
          <div class="card-body">
            <h5 class="card-title">EXITO!</h5>
            <p class="card-text">Su formulario ha sido editado exitosamente. Si existe algún error, nuestros administradores se comunicarán con ud.</p>
            <a href="<?= base_url(); ?>user" class="btn btn-success">Volver al formulario</a>
          </div>
        </div>

      <?php }else if( $exito == '05'){ ?>

        <div class="card w-100 text-center success_card">
          <h5 class="card-header bg-success"><i class="far fa-check-circle"></i></h5>
          <div class="card-body">
            <h5 class="card-title">CORREO ENVIADO CORRECTAMENTE!</h5>
            <p class="card-text">Nuestro equipo le responderá lo mas pronto posible.</p>
            <a href="<?= base_url(); ?>" class="btn btn-success">Volver a la página principal</a>
          </div>
        </div>

      <?php }else if( $exito == '06'){ ?>

        <div class="card w-100 text-center success_card">
          <h5 class="card-header bg-success"><i class="far fa-check-circle"></i></h5>
          <div class="card-body">
            <h5 class="card-title">EXITO!</h5>
            <p class="card-text">Usuario informado correctamente. Informacion editada correctamente.</p>
            <a href="<?= base_url(); ?>admin" class="btn btn-success">Volver a la página principal</a>
          </div>
        </div>

      <?php }else if( $exito == '07'){ ?>

        <div class="card w-100 text-center success_card">
          <h5 class="card-header bg-success"><i class="far fa-check-circle"></i></h5>
          <div class="card-body">
            <h5 class="card-title">MUCHAS GRACIAS POR PARTICIPAR!</h5>
            <p class="card-text">Formulario completado correctamente. Hemos enviado un correo con información de su cuenta. Pronto será revisado por nuestros administradores.</p>
            <a href="<?= base_url(); ?>innovacion" class="btn btn-success">Volver a la página principal</a>
          </div>
        </div>

      <?php }else if( $exito == '08'){ ?>

        <div class="card w-100 text-center success_card">
          <h5 class="card-header bg-success"><i class="far fa-check-circle"></i></h5>
          <div class="card-body">
            <h5 class="card-title">EXITO!</h5>
            <p class="card-text">Su formulario ha sido editado exitosamente. Si existe algún error, nuestros administradores se comunicarán con ud.</p>
            <a href="<?= base_url(); ?>user/innovacion" class="btn btn-success">Volver a la página principal</a>
      <?php }else if( $exito == '10'){ ?>

        <div class="card w-100 text-center success_card">
          <h5 class="card-header bg-success"><i class="fas fa-thumbs-up"></i></h5>
          <div class="card-body">
            <h5 class="card-title">CORREO DE RECUPERACION ENVIADO</h5>
            <p class="card-text">Hemos enviado un correo para reestablecer tu contraseña.</p>
            <a href="<?= base_url(); ?>login/user" class="btn btn-success">Iniciar sesión</a>
          </div>
        </div>

      <?php }else if( $exito == '11'){ ?>

        <div class="card w-100 text-center success_card">
          <h5 class="card-header bg-success"><i class="fas fa-thumbs-up"></i></h5>
          <div class="card-body">
            <h5 class="card-title">CONTRASEÑA ACTUALIZADA</h5>
            <p class="card-text">Su contraseña ha sido editada correctamente. </p>
            <a href="<?= base_url(); ?>admin" class="btn btn-success">Volver a la página principal</a>
            <a href="<?= base_url(); ?>login/user" class="btn btn-success">Iniciar sesión</a>
          </div>
        </div>

      <?php }else if( $exito == '12'){ ?>

        <div class="card w-100 text-center success_card">
          <h5 class="card-header bg-success"><i class="fas fa-thumbs-up"></i></h5>
          <div class="card-body">
            <h5 class="card-title">USUARIO VALIDADO CORRECTAMENTE</h5>
            <p class="card-text">Ya estas registrado y validado, puedes comenzar a llenar tu formulario iniciando sesión.</p>
            <a href="<?= base_url(); ?>admin" class="btn btn-success">Volver a la página principal</a>
            <a href="<?= base_url(); ?>login/user" class="btn btn-success">Iniciar sesión</a>
          </div>
        </div>


      <?php }else{  ?>
        <div class="card w-100 text-center success_card">
          <h5 class="card-header bg-success"><i class="far fa-check-circle"></i></h5>
          <div class="card-body">
            <h5 class="card-title">EXITO!</h5>
            <p class="card-text">Lo que haya realizado, fue... correcto!</p>
            <a href="<?= base_url(); ?>" class="btn btn-success">Volver a la página principal</a>
          </div>
        </div>

      <?php }?>



    </div>
  </div>

</div>
