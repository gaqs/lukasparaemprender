<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tests extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

   public function create_random_password(){
   	$pass = password_generate(6);
	$encrypted_pass = md5($pass);
	$encrypted_pass = hash('sha256',$encrypted_pass);

	echo 'contraseña: '.$pass.'<br>contraseña cifrada: '.$encrypted_pass;

   }

   public function pass(){
     $this->load->view('test');
   }
   public function cy_pass(){
     $pass = $this->input->post('pass');
     $cy_pass = md5($pass);
     $cy_pass = hash('sha256',$cy_pass);

     echo $cy_pass;
   }

}//End CI_Controller
?>
