<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Backup extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	 public function index(){
	 	date_default_timezone_set("Chile/Continental");
	 	$now = time(); //unix time

		$this->load->dbutil();
        $prefs = array(
                'format'      => 'txt'
              );
        $backup = $this->dbutil->backup($prefs);

        $path = './sql_file/'.$now.'_db_backup.sql';

       	$this->load->helper('file');
				write_file($path, $backup);

  	}

		public function innovacion(){
 	 	date_default_timezone_set("Chile/Continental");
 	 	$now = time(); //unix time
		$this->load->dbutil();
		$prefs = array(
        'tables'        => array('inn_encuesta', 'inn_mentor', 'inn_usuarios'),
        'ignore'        => array('administradores', 'empresa', 'encuesta_emprendimiento','encuesta_empresa','notas_encuesta','usuarios'),
        'format'        => 'txt',                       // gzip, zip, txt
        'add_drop'      => TRUE,                        // Whether to add DROP TABLE statements to backup file
        'add_insert'    => TRUE,                        // Whether to add INSERT data to backup file
        'newline'       => "\n"                         // Newline character used in backup file
		);

		$backup = $this->dbutil->backup($prefs);

		$path = './sql_file/inn_'.$now.'_db_backup.sql';
    $this->load->helper('file');
 		write_file($path, $backup);

   	}


}
