/*##################
  ### DEPRECATED ###
  ##################*/
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Empresa extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

   public function index()
   {
     $this->load->view('header');
     $this->load->view('empresa');
     $this->load->view('footer');
   }

   public function insert(){
    date_default_timezone_set("Chile/Continental");
    //Informacion basica
    $user_rut = $this->input->post('user_rut');
    //verificar si rut existe en la base de datos
    $query = $this->db->get_where('usuarios', array('rut'=>$user_rut, 'flag'=>1));
    $result = $query->result();

    if( empty($result) ){
      //usuario
      $user_name 					= ucwords($this->input->post('user_name'));
      $user_surname 			= ucwords($this->input->post('user_surname'));
      $user_job 					= $this->input->post('user_job');
      $user_business 			= $this->input->post('user_business');
      $user_position      = $this->input->post('user_position');
      $user_phone 				= $this->input->post('user_phone');
      $user_email 				= $this->input->post('user_email');
      //empresa
      $business_rut     = $this->input->post('business_rut');
      $business_name    = $this->input->post('business_name');
      $business_date    = $this->input->post('business_date');
      $business_address = $this->input->post('business_address');
      $business_phone   = $this->input->post('business_phone');
      $business_web     = $this->input->post('business_web');

      //informacion de empresa
      $user_proyect_name 				= $this->input->post('user_proyect_name');

      $bussiness_explication 		= $this->input->post('bussiness_explication');
      $bussiness_team 					= $this->input->post('bussiness_team');
      $bussiness_finances 			= $this->input->post('bussiness_finances');
      $bussiness_clients 			  = $this->input->post('bussiness_clients');
      $bussiness_commercialization = $this->input->post('bussiness_commercialization');
      $bussiness_competition 		= $this->input->post('bussiness_competition');
      $bussiness_projection	    = $this->input->post('bussiness_projection');
      $bussiness_jobs           = $this->input->post('bussiness_jobs');
      $bussiness_complementary	= $this->input->post('bussiness_complementary');


      //cedula de identidad
      $file_path = "./uploaded_files/" . $user_rut .'/';
      $path_complementario = $file_path.'Complementario/';
      if (!file_exists($file_path)) { mkdir($file_path, 0777, true); }
      if (!file_exists($path_complementario)){ mkdir($path_complementario, 0777, true); }

      //archivos complementarios
      $path_complemento1 = '';
      $path_complemento2 = '';
      $complemento1 = $_FILES['complemento1'];
      $complemento2 = $_FILES['complemento2'];

      if( !empty($complemento1['tmp_name'] ) ){
        $path_complemento1 = fileManager($path_complementario, 'comp01_', $complemento1);
      }
      if( !empty($complemento2['tmp_name'] ) ){
        $path_complemento2 = fileManager($path_complementario, 'comp02_', $complemento2);
      }

      $cedula         = $_FILES['cedula'];
      $rol            = $_FILES['rol'];
      $renta          = $_FILES['renta'];
      $patente        = $_FILES['patente'];
      $otrospermisos  = $_FILES['otrospermisos'];
      $boleta         = $_FILES['boleta'];

      $path_cedula        = fileManager($file_path, 'cedula', $cedula);
      $path_rol           = fileManager($file_path, 'rol', $rol);
      $path_renta         = fileManager($file_path, 'renta', $renta);
      $path_patente       = fileManager($file_path, 'patente', $patente);
      $path_otrospermisos = fileManager($file_path, 'otrospermisos', $otrospermisos);
      $path_boleta        = fileManager($file_path, 'boleta', $boleta);

      $pass           = password_generate(6);
      $encrypted_pass = md5($pass);
      $encrypted_pass = hash('sha256', $encrypted_pass);
      // Guardado en base de datos
      $data = array(
        'tipo_usuario'=> 1,
        'rut'	 				=> $user_rut,
        'contrasena' 	=> $encrypted_pass, //crear password
        'nombres'			=> $user_name,
        'apellidos' 	=> $user_surname,
        'ocupacion' 	=> $user_job,
        'telefono' 		=> $user_phone,
        'email' 			=> $user_email,
        'empresa' 		=> $user_business,
        'cargo' 			=> $user_position,
        'insert_date' => date('y-m-d H:i:s'),
        'edit_date' 	=> date('y-m-d H:i:s')
        );

        $this->db->insert('usuarios', $data);
        $user_id = $this->db->insert_id();

        $datac = array(
          'id_usuario' 			=> $user_id,
          'nombre' 					=> $user_proyect_name,
          'explicacion' 		=> $bussiness_explication,
          'equipo' 					=> $bussiness_team,
          'financiamiento' 	=> $bussiness_finances,
          'clientes'        => $bussiness_clients,
          'comercializacion'=> $bussiness_commercialization,
          'competencia' 		=> $bussiness_competition,
          'proyecciones' 		=> $bussiness_projection,
          'trabajos'        => $bussiness_jobs,
          'complemento'     => $bussiness_complementary,
          'path_complemento1' => $path_complemento1,
          'path_complemento2' => $path_complemento2,
          'path_cedula' 		=> $path_cedula,
          'path_rol'        => $path_rol,
          'path_renta'      => $path_renta,
          'path_patente'     =>$path_patente,
          'path_otrospermisos' => $path_otrospermisos,
          'path_boleta'     => $path_boleta,
          'insert_date' 		=> date('y-m-d H:i:s'),
          'edit_date' 			=> date('y-m-d H:i:s')
        );

        $this->db->insert('encuesta_empresa', $datac);
        $encuesta_id = $this->db->insert_id();

        $datab = array(
          'id_usuario'    => $user_id,
          'id_encuesta'  => $encuesta_id,
          'rut_empresa'  => $business_rut,
          'razon_social' => $business_name,
          'inicio'       => $business_date,
          'direccion_empresa' => $business_address,
          'telefono_empresa'  => $business_phone,
          'web'          => $business_web,
          'insert_date'  => date('y-m-d H:i:s'),
          'edit_date'    => date('y-m-d H:i:s')
        );

        $this->db->insert('empresa', $datab);

        //clonacion del insert en otra dabatase clone
        $this->db->db_select('lukaspar_lukasparaemprender_clone');
        $this->db->insert('usuarios', $data);
        $this->db->insert('empresa', $datab);
        $this->db->insert('encuesta_empresa', $datac);



        //Si todo sale correcto, envia correo y va a ventana de exito.
        $this->sendcorrectmail($user_name, $user_surname, $user_rut, $user_proyect_name, $user_email, $pass);
        header('Location: '.base_url().'home/exito?exito=01'); //ingresado correctamente
    }//end if verify
    else{
      header('Location: '.base_url().'home/error?error=01');
    }

  }//end public function insert

  public function sendcorrectmail($user_name, $user_surname, $user_rut, $user_proyect_name, $user_email, $pass){
    $config = Array(
        'protocol'  => 'smtp',
        'smtp_host' => 'mail.lukasparaemprender.com',
        'smtp_port' => '587',
        'smtp_user' => 'postmaster@lukasparaemprender.com',
        'smtp_pass' => 'g3@N1Phrd=JL',
        'mailtype'  => 'html',
        'charset'   => 'iso-8859-1',
        'newline'   => "\r\n"
    );
    $this->load->library('email', $config);

    $data['info'] = array(
      'name' => $user_name,
      'surname' => $user_surname,
      'rut' => $user_rut,
      'email' => $user_email,
      'pass' => $pass
    );
    $message = utf8_decode($this->load->view('email/email',$data, TRUE));

    $this->email->from('postmaster@lukasparaemprender.com', 'Luka$ para Emprender');
    $this->email->to($user_email);
    $this->email->bcc('lukasparaemprender@gmail.com');

    $this->email->subject('Ingresado correctamente');
    $this->email->message($message);

    $send = $this->email->send();

    if (!$send){
       $this->email->print_debugger();
    }


  }

    public function send_test(){

    $user_name = 'gustavo';
    $user_surname = 'quilodran';
    $user_rut = '17513256-2';
    $user_proyect_name = 'prueba';
    $user_email = 'gaqs.02@gmail.com';
    $pass = 'UINHG';

    $this->sendcorrectmail($user_name, $user_surname, $user_rut, $user_proyect_name, $user_email, $pass);

  }








} // end CI_Controller
?>
