<?php
defined('BASEPATH') OR exit('No direct script access allowed');

date_default_timezone_set("Chile/Continental");

class Register extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function index(){
		$this->load->view('header');
	    $this->load->view('register/index');
	    $this->load->view('footer');
	}

	public function insert(){

 		//Informacion basica
 		$user_rut 	= $this->input->post('user_rut');
   	$user_email = strtolower( $this->input->post('user_email') );
 		//verificar si rut existe en la base de datos
 		$query = $this->db->get_where('usuarios', array('rut'=>$user_rut, 'email'=>$user_email ,'flag'=>1));
 		$result = $query->result();

 		//testing
 		if($user_rut == '17513256-2' && $user_email == 'gaqs.02@gmail.com'){ $result = ''; }

 		if( empty($result) ){

 			$user_type 		= $this->input->post('user_type');
 			$user_name 		= ucwords($this->input->post('user_name'));
 			$user_surname = ucwords($this->input->post('user_surname'));
 			$user_pass		= $this->input->post('user_pass');

			//cifra contraseña
 			$encrypted_pass = encrypted($user_pass);

			//genera y cifra un hash de validacion
 			$hash = hash_generate(16);
 			$encrypted_hash = encrypted($hash);

 			$data = array(
        'tipo_usuario'	=> $user_type,
 				'rut'	 					=> $user_rut,
 				'nombres'				=> $user_name,
 				'apellidos' 		=> $user_surname,
 				'email' 				=> $user_email,
 				'contrasena' 		=> $encrypted_pass,
 				'insert_date' 	=> date('y-m-d H:i:s'),
 				'edit_date' 		=> date('y-m-d H:i:s')
 				);

			$this->db->insert('usuarios', $data);
			$user_id = $this->db->insert_id();

			$datab = array(
				'id_user' 		=> $user_id,
				'hash_email' 	=> $encrypted_hash,
				'date_email'	=> date('y-m-d H:i:s'),
				'validado'		=> 0
			);

			$this->db->insert('hash_validation', $datab);

			$this->send_register_email($user_type,$user_rut,$user_name,$user_surname,$user_email,$encrypted_hash);

			header('Location: '.base_url().'home/exito?exito=01'); //enviado correo registro correctamente

 		}else{

 			header('Location: '.base_url().'home/error?error=01'); //el usuario ya existe
 		}

	}//end public function insert

 	public function send_register_email($user_type,$user_rut,$user_name,$user_surname,$user_email,$encrypted_hash){

	    $config = $this->config->item('config_email');
	    $this->load->library('email', $config);

	    $data['info'] = array(
	      'name' 		=> $user_name,
	      'surname' => $user_surname,
	      'rut' 		=> $user_rut,
	      'email' 	=> $user_email,
	      'link' 		=> base_url().'register/validate?hash='.$encrypted_hash
	    );

	    $message = utf8_decode($this->load->view('email/email_register',$data, TRUE));

	    $this->email->from('postmaster@lukasparaemprender.com', 'Luka$ para Emprender');
	    $this->email->to($user_email);
	    $this->email->bcc('lukasparaemprender@gmail.com');

	    $this->email->subject('Ingresado correctamente');
	    $this->email->message($message);

	    $send = $this->email->send();

	    if (!$send){
	       $this->email->print_debugger();
	    }

 	}//end public function send_register_email


 	public function validate(){

 		$hash = $this->input->get('hash');
 		$result = $this->querys->validateAndSelectUser($hash);

 		$user_id = $result[0]['id'];
 		$user_rut = $result[0]['rut'];
 		$user_type = $result[0]['tipo_usuario'];
 		$user_validate = $result[0]['validado'];

 		if( empty($result) ){
 			header('Location: '.base_url().'home/error?error=03'); //hash no existe, por ende usuario no existe
 		}else{

 			if( $user_validate == 1 ){

 				header('Location: '.base_url().'home/error?error=07'); //usuario previamente validado

	 		}else{

	 			$this->db->where('id_user', $user_id);
	 			$this->db->update('hash_validation', array( 'validado' => 1 ) );

	 			//crea carpeta vacia para el usuario
				$file_path = "./uploaded_files/" . $user_rut .'/';
				if (!file_exists($file_path)) { mkdir($file_path, 0777, true); }

		 		if( $user_type == 0 ){


		 			$data = array(
						'id_usuario' 	=> $user_id,
						'insert_date' 	=> date('y-m-d H:i:s'),
						'edit_date' 	=> date('y-m-d H:i:s')
		 			);

		 			$this->db->insert('encuesta_emprendimiento', $data);


		 		}else{

		 			$data = array(
						'id_usuario' 	=> $user_id,
						'insert_date' 	=> date('y-m-d H:i:s'),
						'edit_date' 	=> date('y-m-d H:i:s')
		 			);

		 			$this->db->insert('encuesta_empresa', $data);
		 			$encuesta_id = $this->db->insert_id();

			        $datab = array(
			          'id_usuario'   => $user_id,
			          'id_encuesta'  => $encuesta_id,
			          'insert_date'  => date('y-m-d H:i:s'),
			          'edit_date'    => date('y-m-d H:i:s')
			        );

			        $this->db->insert('empresa', $datab);

		 		}

		 		header('Location: '.base_url().'home/exito?exito=10'); //enviado correo registro correctamente

	 		}//id validate == 0

 		}//if user existe

 	}

 	public function forgot(){
 		$this->load->view('header');
	    $this->load->view('register/forgot');
	    $this->load->view('footer');
 	}

 	public function confirm_pass(){
 			$this->load->view('header');
	    $this->load->view('register/pass');
	    $this->load->view('footer');
 	}

 	public function send_recover_pass(){

 		$config = $this->config->item('config_email');
 		$this->load->library('email', $config);

 		$email = $this->input->post('email');

 		$hash_pass 	= hash_generate(16);
 		$encrypted_hash = encrypted($hash_pass);

 		$pass 		= pass_generate(6);
 		$encrypted_pass = encrypted($pass);


 		$query = $this->db->select('id')->where('email', $email)->get('usuarios');
 		$res = $query->result();

 		$data = array(
 				'hash_pass' => $encrypted_hash,
 				'pass'			=> $encrypted_pass,
 				'date_pass'	=> date('y-m-d H:i:s')
 			);

 		$this->db->where('id_user', $res[0]->id );
	 	$this->db->update('hash_validation', $data );

	 	//send email
	 	$datab['data'] = array( 'pass' => $pass, 'link' => base_url().'register/confirm_pass?hash='.$encrypted_hash );

 		$message = utf8_decode($this->load->view('email/email_resetpass',$datab, TRUE));

    $this->email->from('postmaster@lukasparaemprender.com', 'Luka$ para Emprender');
    $this->email->to($email);
    //$this->email->bcc('lukasparaemprender@gmail.com');
    $this->email->subject('Recuperar contraseña');
    $this->email->message($message);

    $send = $this->email->send();

    if (!$send){
       $this->email->print_debugger();
    }else{
    	header('Location: '.base_url().'home/exito?exito=10'); //enviado correo reset pass correctamente
    }

 	}

 	public function email(){
 		$this->load->view('email/email_resetpass');
 	}

 	public function reset_pass(){

 		$old_pass = $this->input->post('old_pass');
 		$new_pass = $this->input->post('new_pass');
 		$pass_hash = $this->input->post('hash');

		$encrypted_old_pass = encrypted($old_pass);
		$encrypted_new_pass = encrypted($new_pass);

 		$result = $this->querys->selectUserIdByHash($encrypted_old_pass, $pass_hash);

		if( empty($result) ){
			header('Location: '.base_url().'home/exito?error=02'); //old pass incorrecta o hash no existe
		}else{
			var_dump($result);
			$user_id = $result[0]['id_user'];

	 		$this->db->where('id', $user_id);
		 	$this->db->update('usuarios', array( 'contrasena' => $encrypted_new_pass ) );

		 	header('Location: '.base_url().'home/exito?exito=11'); //contraseña actualizada
		}



 	}




} //End CI_Controller

//9KQNTE coffee.puertom@gmail.com
?>
