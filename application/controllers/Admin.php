<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

   public function header(){
  		$name = $this->session->userdata('name');

  		if( isset($name ) ){
  			$info['name'] = $name;
  			$info['surname'] = $this->session->userdata('surname');
  			$info['email'] = $this->session->userdata('email');

  			$this->load->view('admin/header',$info);
  		}else{
  			header('Location: '.base_url().'login');
  		}
  	}

   public function index()
   {
     $this->header();
 		 $data['usuarios'] = $this->querys->selectInfoUser();
     $this->load->view('admin/index', $data);
     $this->load->view('admin/footer');
   }

   public function innovacion()
   {
     $this->header();
 		 $data['inn_usuarios'] = $this->querys->selectAllInnUser();
     $this->load->view('admin/innovacion', $data);
     $this->load->view('admin/footer');
   }

   public function modal_edit_empren(){
  		$id = $this->input->post('id');
  		$data['user'] = $this->querys->selectUserById($id);
      $data['user'][0]['id'] = $id;
      $data['notas'] = $this->db->get_where('notas_encuesta', array('id_usuario'=>$id))->result();
  		$this->load->view('admin/modals/modal_editempren',$data);
  	}

    public function modal_edit_empresa(){
   		$id = $this->input->post('id');
   		$data['user'] = $this->querys->selectUserByIdEmpresa($id);
      $data['user'][0]['id'] = $id;

      $data['notas'] = $this->db->get_where('notas_encuesta', array('id_usuario'=>$id))->result();
   		$this->load->view('admin/modals/modal_editempresa',$data);
   	}

    public function modal_edit_inn(){
   		$group_id = $this->input->post('group_id');

      for ($r=1; $r < 6; $r++) {
        $data['inn_user'.$r] = $this->querys->selectUserByGroupAndRol($group_id, $r);
      }
      $data['inn_all'] = $this->querys->selectAllByGroup($group_id);

   		$this->load->view('admin/modals/modal_editinnovacion',$data);
   	}


    public function edit_user()
    {
      date_default_timezone_set("Chile/Continental");
      $send = '0';

      $action             = $this->input->post('action');
      $user_id 					  = $this->input->post('user_id');
      $user_rut 					= $this->input->post('user_rut');
      $user_email 				= $this->input->post('user_email');
      $user_name 					= ucwords($this->input->post('user_name'));
 			$user_surname 			= ucwords($this->input->post('user_surname'));
 			$user_job 					= $this->input->post('user_job');
 			$user_address 			= $this->input->post('user_address');
 			$user_phone 				= $this->input->post('user_phone');

 			//informacion de emprendimiento
 			$user_proyect_name 				= $this->input->post('user_proyect_name');
 			$bussiness_explication 		= $this->input->post('bussiness_explication');
 			$bussiness_diferences 		= $this->input->post('bussiness_diferences');
 			$bussiness_finances 			= $this->input->post('bussiness_finances');
      $bussiness_clients 			  = $this->input->post('bussiness_clients');
 			$bussiness_status 				= $this->input->post('bussiness_status');
 			$bussiness_complementary	= $this->input->post('bussiness_complementary');
 			$bussiness_team 					= $this->input->post('bussiness_team');

      $message 					        = $this->input->post('mensaje_error');

      $descalificado            = $this->input->post('des_emprendimiento');
      $causa                    = '';

      if( $descalificado == 'on' ){
        $descalificado = 1;
        $causa = $this->input->post('des_textarea');
      }else{
        $descalificado = 0;
      }


      if( $action == 'savensend' && !empty($message)){

      $full_message = 'Estimado '.$user_name.' '.$user_surname.'<br>Hemos encontrado errores en el formulario de inscripción para participar en el concurso de <b>"Luka$ para Emprender"</b>.<br>Se adjunta el listado a continuación.<br><br><b>'.$message.'</b><br><br>Si desea arregla los errores, por favor visite el siguiente link:<br>'.base_url().'login/user<br>Inicie sesión con los datos previamente enviados a su correo y verifique los campos que le permitan editar.<br><br>Cualquier información estamos en el siguiente contacto:<br>lukasparaemprender@puertomontt.com<br>(65) 2 261315<br><br>Muchas Gracias.<br>Saluda atentamente a Ud.<br><br>Subdepartamento de Economía y Desarrollo.<br>Dirección de Desarrollo Comunitario.<br><b>CONCURSO LUKAS PARA EMPRENDER</b>';

        $message = $this->input->post('mensaje_error');
        $this->senderroremail($full_message, $user_email);
        $send = '1';
      }

      $data = array(
 				'nombres'			=> $user_name,
 				'apellidos' 	=> $user_surname,
 				'ocupacion' 	=> $user_job,
 				'direccion' 	=> $user_address,
 				'telefono' 		=> $user_phone,
        'descalificado' => $descalificado,
        'descalificado_causa' => $causa,
 				'edit_date' 	=> date('y-m-d H:i:s')
 				);

        $this->db->where('id', $user_id);
 				$this->db->update('usuarios', $data);

        $datab = array(
          'nombre' 					=> $user_proyect_name,
          'resumen' 				=> $bussiness_explication,
          'equipo' 					=> $bussiness_team,
          'relevancia'	 		=> $bussiness_diferences,
          'financiamiento' 	=> $bussiness_finances,
          'clientes' 	      => $bussiness_clients,
          'estado' 					=> $bussiness_status,
          'complemento' 		=> $bussiness_complementary,
          'edit_date' 			=> date('y-m-d H:i:s')
        );

        $this->db->where('id_usuario', $user_id);
 				$this->db->update('encuesta_emprendimiento', $datab);

        //vaL todos los chec elejidos en db a 0 en usuario
        $array = $this->input->post('checks[]');
        if( !empty($array) ){
          $data = array_fill_keys($array, 0);
          $this->db->where('id', $user_id);
          $this->db->update('usuarios', $data);
        }
        //vaL todos los chec elejidos en db a 0 en encuesta emprendimiento
        $b_array = $this->input->post('b_checks[]');

        if( !empty($b_array) ){
          $b_data = array_fill_keys($b_array, 0);
          var_dump($b_data);
          $this->db->where('id_usuario', $user_id);
          $this->db->update('encuesta_emprendimiento', $b_data);
        }

        if( !empty($b_array) ){
          //elimina archivos si val esta en array del check
          if( in_array('val_path_cedula', $b_array) ){
            //eliminar archivo
            $path = $this->input->post('path_cedula');
            unlink($path);
          }
          if( in_array('val_path_residencia', $b_array) ){
            //eliminar archivo
            $path = $this->input->post('path_residencia');
            unlink($path);
          }

        }

        if( $send == '1'){
          header('Location: '.base_url().'home/exito?exito=06');
        }else{
          header('Location: '.base_url().'home/exito?exito=03');
        }

    }

    public function delete_user(){
      date_default_timezone_set("Chile/Continental");
      $id = $this->input->post('user_id');
      $query = $this->querys->selectEspecificUser($id);
      $tipo = $query[0]['tipo_usuario'];

      if( $tipo == 0 ){
        $user = $this->querys->selectUserById($id);
        $path = $user[0]['path_cedula'];
        $file = explode('/', $path);
        $carpeta = './'.$file[1].'/'.$file[2].'/';
        rename($carpeta, './'.$file[1].'/flaged'.date('ymdHis').'_'.$file[2].'/');

        $this->db->where('id_usuario', $id)->update('encuesta_emprendimiento', array('flag'=>0));
        $this->db->where('id', $id)->update('usuarios', array('flag'=>0));

      }else if( $tipo == 1){
        $user = $this->querys->selectUserByIdEmpresa($id);
        $path = $user[0]['path_cedula'];
        $file = explode('/', $path);
        $carpeta = './'.$file[1].'/'.$file[2].'/';
        rename($carpeta, './'.$file[1].'/flaged'.date('ymdHis').'_'.$file[2].'/');

        $this->db->where('id_usuario', $id)->update('encuesta_empresa', array('flag'=>0));
        $this->db->where('id_usuario', $id)->update('empresa', array('flag'=>0));
        $this->db->where('id', $id)->update('usuarios', array('flag'=>0));

      }
      header('Location: '.base_url().'home/exito?exito=02');

    }

    public function edit_empresa(){
      date_default_timezone_set("Chile/Continental");
      $send = '0';

      $action             = $this->input->post('action');
      $user_id 					  = $this->input->post('user_id');
      $user_rut 					= $this->input->post('user_rut');
      $user_name 					= ucwords($this->input->post('user_name'));
      $user_surname 			= ucwords($this->input->post('user_surname'));
      $user_job 					= $this->input->post('user_job');
      $user_business 			= $this->input->post('user_business');
      $user_position      = $this->input->post('user_position');
      $user_phone 				= $this->input->post('user_phone');
      $user_email 				= $this->input->post('user_email');
      //empresa
      $business_rut     = $this->input->post('business_rut');
      $business_name    = $this->input->post('business_name');
      $business_date    = $this->input->post('business_date');
      $business_address = $this->input->post('business_address');
      $business_phone   = $this->input->post('business_phone');
      $business_web     = $this->input->post('business_web');

      //informacion de empresa
      $user_proyect_name 				= $this->input->post('user_proyect_name');

      $bussiness_explication 		= $this->input->post('bussiness_explication');
      $bussiness_team 					= $this->input->post('bussiness_team');
      $bussiness_finances 			= $this->input->post('bussiness_finances');
      $bussiness_clients 			  = $this->input->post('bussiness_clients');
      $bussiness_commercialization = $this->input->post('bussiness_commercialization');
      $bussiness_competition 		= $this->input->post('bussiness_competition');
      $bussiness_projection	    = $this->input->post('bussiness_projection');
      $bussiness_jobs           = $this->input->post('bussiness_jobs');
      $bussiness_complementary	= $this->input->post('bussiness_complementary');

      $message 					        = $this->input->post('mensaje_error');

      $descalificado            = $this->input->post('des_empresa');
      $causa                    = '';

      if( $descalificado == 'on' ){
        $descalificado = 1;
        $causa = $this->input->post('des_textarea');
      }else{
        $descalificado = 0;
      }


      if( $action == 'savensend' && !empty($message)){

      $full_message = 'Estimado '.$user_name.' '.$user_surname.'<br>Hemos encontrado errores en el formulario de inscripción para participar en el concurso de <b>"Luka$ para Emprender"</b>.<br>Se adjunta el listado a continuación.<br><br><b>'.$message.'</b><br><br>Si desea arregla los errores, por favor visite el siguiente link:<br>'.base_url().'login/user<br>Inicie sesión con los datos previamente enviados a su correo y verifique los campos que le permitan editar.<br><br>Cualquier información estamos en el siguiente contacto:<br>lukasparaemprender@puertomontt.com<br>(65) 2 261315<br><br>Muchas Gracias.<br>Saluda atentamente a Ud.<br><br>Subdepartamento de Economía y Desarrollo.<br>Dirección de Desarrollo Comunitario.<br><b>CONCURSO LUKAS PARA EMPRENDER</b>';

        $message = $this->input->post('mensaje_error');
        $this->senderroremail($full_message, $user_email);
        $send = '1';
      }

      $data = array(
        'rut'	 				=> $user_rut,
        'nombres'			=> $user_name,
        'apellidos' 	=> $user_surname,
        'ocupacion' 	=> $user_job,
        'telefono' 		=> $user_phone,
        'email' 			=> $user_email,
        'empresa' 		=> $user_business,
        'cargo' 			=> $user_position,
        'descalificado' => $descalificado,
        'descalificado_causa' => $causa,
        'edit_date' 	=> date('y-m-d H:i:s')
        );

        $this->db->where('id', $user_id);
        $this->db->update('usuarios', $data);

        $datab = array(
          'rut_empresa'      => $business_rut,
          'razon_social'     => $business_name,
          'inicio'           => $business_date,
          'direccion_empresa'=> $business_address,
          'telefono_empresa' => $business_phone,
          'web'              => $business_web,
          'edit_date' 	      => date('y-m-d H:i:s')
        );

        $this->db->where('id_usuario', $user_id);
        $this->db->update('empresa', $datab);

        $datac = array(
          'nombre' 					=> $user_proyect_name,
          'explicacion' 		=> $bussiness_explication,
          'equipo' 					=> $bussiness_team,
          'financiamiento' 	=> $bussiness_finances,
          'clientes'        => $bussiness_clients,
          'comercializacion'=> $bussiness_commercialization,
          'competencia' 		=> $bussiness_competition,
          'proyecciones' 		=> $bussiness_projection,
          'trabajos'        => $bussiness_jobs,
          'complemento'     => $bussiness_complementary,
          'edit_date' 			=> date('y-m-d H:i:s')
        );

        $this->db->where('id_usuario', $user_id);
        $this->db->update('encuesta_empresa', $datac);

        //vaL todos los check elejidos en db a 0 en usuario
        $array = $this->input->post('checks[]');
        if( !empty($array) ){
          $data = array_fill_keys($array, 0);
          $this->db->where('id', $user_id);
          $this->db->update('usuarios', $data);
        }
        //vaL todos los check elejidos en db a 0 en empresa
        $c_array = $this->input->post('c_checks[]');
        if( !empty($c_array) ){
          $c_data = array_fill_keys($c_array, 0);
          $this->db->where('id_usuario', $user_id);
          $this->db->update('empresa', $c_data);
        }
        //vaL todos los check elejidos en db a 0 en encuesta empresa
        $b_array = $this->input->post('b_checks[]');
        if( !empty($b_array) ){
          $b_data = array_fill_keys($b_array, 0);
          $this->db->where('id_usuario', $user_id);
          $this->db->update('encuesta_empresa', $b_data);
        }

        //elimina archivos si val esta en array del check
        if( in_array('val_path_cedula', $b_array) ){ $path = $this->input->post('path_cedula'); unlink($path); }
        if( in_array('val_path_rol', $b_array) ){ $path = $this->input->post('path_rol'); unlink($path); }
        if( in_array('val_path_renta', $b_array) ){ $path = $this->input->post('path_renta'); unlink($path); }
        if( in_array('val_path_patente', $b_array) ){ $path = $this->input->post('path_patente'); unlink($path); }
        if( in_array('val_path_otrospermisos', $b_array) ){ $path = $this->input->post('path_otrospermisos'); unlink($path); }
        if( in_array('val_path_boleta', $b_array) ){ $path = $this->input->post('path_boleta'); unlink($path); }

        if( $send == '1'){
          header('Location: '.base_url().'home/exito?exito=06');
        }else{
          header('Location: '.base_url().'home/exito?exito=03');
        }

    }

    public function senderroremail($message, $user_email){
     $config = Array(
         'protocol'  => 'smtp',
         'smtp_host' => 'mail.lukasparaemprender.com',
         'smtp_port' => '587',
         'smtp_user' => 'postmaster@lukasparaemprender.com',
         'smtp_pass' => 'g3@N1Phrd=JL',
         'mailtype'  => 'html',
         'charset'   => 'iso-8859-1',
         'newline'	  => "\r\n"
     );
     $this->load->library('email', $config);
     $message = utf8_decode($message);

     $this->email->from('postmaster@lukasparaemprender.com', 'Luka$ para Emprender');
     $this->email->to($user_email);

     $this->email->subject('Ingresado correctamente');
     $this->email->message($message);

     $this->email->send();
    }

    public function verify_rut(){
      $rut = $this->input->post('rut');
      $validar = validRut($rut);
      if( $validar ){
        $query = $this->db->get_where('usuarios', array('rut'=>$rut ,'flag'=>1));
        $result = $query->result();

        if( empty($result) ){
          echo 'success';
        }else{
          echo 'error';
        }

      }else{
        echo 'error_validacion';
      }

    }

    public function verify_email(){
      $email = $this->input->post('email');

      if( !empty($email)){
        $query = $this->db->get_where('usuarios', array('email'=>$email ,'flag'=>1));
        $result = $query->result();

        if( empty($result) ){
          echo 'success';
        }else{
          echo 'error';
        }
      }else{
        echo 'error';
      }


    }

    public function save_notas(){

        $id = $this->input->post('id_usuario');
        $pregunta = $this->input->post('npregunta');
        $criterio = $this->input->post('ncriterio');
        $nota = $this->input->post('nota');

      $data = array(
        'id_usuario'  => $id,
        'pregunta'    => $pregunta,
        'criterio'    => $criterio,
        'nota'        => $nota
      );

      $query = $this->db->get_where('notas_encuesta', array('id_usuario'=>$id ,'pregunta'=>$pregunta, 'criterio'=>$criterio) );
      $result = $query->result();

      if( empty($result) ){
        $this->db->insert('notas_encuesta', $data);
      }else{
        $this->db->where( array('id_usuario'=>$id ,'pregunta'=>$pregunta, 'criterio'=>$criterio) );
       $this->db->update('notas_encuesta', $data);
      }

      $error = $this->db->error();
      echo $error['message'];

    }

    public function inn_verify_rut(){
      $rut = $this->input->post('rut');
      $validar = validRut($rut);
      if( $validar ){
        $query = $this->db->get_where('inn_usuarios', array('rut'=>$rut));
        $result = $query->result();

        if( empty($result) ){
          echo 'success';
        }else{
          echo 'error';
        }

      }else{
        echo 'error_validacion';
      }

    }

    public function inn_verify_email(){
      $email = $this->input->post('email');

      if( !empty($email)){
        $query = $this->db->get_where('inn_usuarios', array('email'=>$email));
        $result = $query->result();

        if( empty($result) ){
          echo 'success';
        }else{
          echo 'error';
        }
      }else{
        echo 'error';
      }


    }



 }//end CI_Controller
?>
