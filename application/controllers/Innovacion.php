<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Innovacion extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function index()
	{
		$this->load->view('header');
		$this->load->view('innovacion/index');
		$this->load->view('innovacion/footer');
	}

  public function form(){
    $this->load->view('header');
		$this->load->view('innovacion/formulario');
		$this->load->view('innovacion/footer');

  }

	public function insert(){
		date_default_timezone_set("Chile/Continental");
		/*   Mentor   */
		$men_user_rut					= $this->input->post('men_user_rut');
		$men_user_name				= $this->input->post('men_user_name');
		$men_user_surname			= $this->input->post('men_user_surname');
		$men_user_phone				= $this->input->post('men_user_phone');
		$men_user_email				= $this->input->post('men_user_email');
		$men_user_description = $this->input->post('men_user_description');

		$solution_challenge   = $this->input->post('solution_challenge');
		$solution_deepen      = $this->input->post('solution_deepen');
		$solution_recipient   = $this->input->post('solution_recipient');

		/*   Solucion   */
		$solution_proposal		= $this->input->post('solution_proposal');
		$solution_community		= $this->input->post('solution_community');
		$solution_prototipe		= $this->input->post('solution_prototipe');
		$solution_susten			= $this->input->post('solution_susten');
		$solution_test				= $this->input->post('solution_test');
		$solution_budget			= $this->input->post('solution_budget');

		/*  Archivos   */
		$patrocinio 					= $_FILES['patrocinio'];
		$mentor_academico 		= $_FILES['mentor_academico'];
		$reunion 							= $_FILES['reunion'];
		$instrumentos 				= $_FILES['instrumentos'];


		$data1 = array(
			'rut'					=> $men_user_rut,
			'nombres'			=> $men_user_name,
			'apellidos'		=> $men_user_surname,
			'telefono'		=> $men_user_phone,
			'email'				=> $men_user_email,
			'descripcion' => $men_user_description,
			'insert_date' => date('y-m-d H:i:s'),
			'edit_date' 	=> date('y-m-d H:i:s')
		);

		$this->db->insert('inn_mentor', $data1);
		$group_id = $this->db->insert_id();

		//cedula de identidad

		$file_path = "./uploaded_files/" . $men_user_rut .'/';
		if (!file_exists($file_path)) { mkdir($file_path, 0777, true); }

		$path_patrocinio = fileManager($file_path, 'patrocinio', $patrocinio);
		$path_mentor_academico = fileManager($file_path, 'mentor_academico', $mentor_academico);
		$path_reunion = fileManager($file_path, 'reunion', $reunion);
		$path_instrumentos = fileManager($file_path, 'instrumentos', $instrumentos);

		$data2 = array(
			'id_grupo'							=> $group_id,
			'desafio' 							=> $solution_challenge,
			'profundizar' 					=> $solution_deepen,
			'beneficiarios' 				=> $solution_recipient,
			'propuesta_solucion' 		=> $solution_proposal,
			'comunidad_implicada' 	=> $solution_community,
			'prototipo' 						=> $solution_prototipe,
			'sustentabilidad' 			=> $solution_susten,
			'testear' 							=> $solution_test,
			'presupuesto' 					=> $solution_budget,
			'path_patrocinio'				=> $path_patrocinio,
			'path_mentor_academico'	=> $path_mentor_academico,
			'path_reunion'					=> $path_reunion,
			'path_instrumentos'			=> $path_instrumentos,
			'insert_date' 					=> date('y-m-d H:i:s'),
			'edit_date' 						=> date('y-m-d H:i:s')
		);

		$this->db->insert('inn_encuesta', $data2);

		/*   Integrantes   */
		for ($i=1; $i < 6; $i++) {

			if( $i == 4 && $this->input->post('user_rut4') == '' ){ continue; }
			else if( $i == 5 && $this->input->post('user_rut5') == '' ){ continue; }

			$user_rut 					= $this->input->post('user_rut'.$i);
			$user_name 					= $this->input->post('user_name'.$i);
			$user_surname 			= $this->input->post('user_surname'.$i);
			$user_address 			= $this->input->post('user_address'.$i);
			$user_phone 				= $this->input->post('user_phone'.$i);
			$user_email					=	$this->input->post('user_email'.$i);
			$user_rol						= $this->input->post('user_rol'.$i);
			$user_description		= $this->input->post('user_description'.$i);
			/*  Archivos  */
			$alumno 							= $_FILES['alumno_regular'.$i];
			$cedula 							= $_FILES['cedula_identidad'.$i];

			$administrador 				= 0;

			if( $i == 1 ){ $administrador = 1; }

			$pass = password_generate(6);
			$encrypted_pass = md5($pass);
			$encrypted_pass = hash('sha256',$encrypted_pass);

			//cedula de identidad
			$file_path = "./uploaded_files/" . $user_rut .'/';
			if (!file_exists($file_path)) { mkdir($file_path, 0777, true); }

			$path_alumno = fileManager($file_path, 'alumno', $alumno);
			$path_cedula = fileManager($file_path, 'cedula', $cedula);

			$data3 = array(
				'id_grupo'			=> $group_id,
				'administrador' => $administrador,
				'rut'	 					=> $user_rut,
				'nombres'				=> $user_name,
				'apellidos' 		=> $user_surname,
				'direccion' 		=> $user_address,
				'telefono' 			=> $user_phone,
				'email' 				=> $user_email,
				'rol'						=> $user_rol,
				'descripcion'		=> $user_description,
				'contrasena' 		=> $encrypted_pass, //crear password
				'path_alumno' 	=> $path_alumno,
				'path_cedula'		=> $path_cedula,
				'insert_date' 	=> date('y-m-d H:i:s'),
				'edit_date' 		=> date('y-m-d H:i:s')
			);

			$this->db->insert('inn_usuarios', $data3);

			if( $administrador == 1 ){
				$this->sendcorrectmail($user_name, $user_surname, $user_rut, $user_email, $pass);
			}
		}
		//Si todo sale correcto, envia correo y va a ventana de exito.
		header('Location: '.base_url().'home/exito?exito=07'); //ingresado correctamente

	}

	//Actualizar servidor de correos.
	public function sendcorrectmail($user_name, $user_surname, $user_rut, $user_email, $pass){
	 $config = Array(
			 'protocol'  => 'smtp',
			 'smtp_host' => 'mail.lukasparaemprender.com',
			 'smtp_port' => '587',
			 'smtp_user' => 'postmaster@lukasparaemprender.com',
			 'smtp_pass' => 'g3@N1Phrd=JL',
			 'mailtype'  => 'html',
			 'charset'   => 'iso-8859-1',
			 'newline'	  => "\r\n"
	 );
	 $this->load->library('email', $config);

	 $data['info'] = array(
		 'name' => $user_name,
		 'surname' => $user_surname,
		 'rut' => $user_rut,
		 'email' => $user_email,
		 'pass' => $pass
	 );
	 $message = utf8_decode($this->load->view('email/inn_email',$data, TRUE));

	 $this->email->from('postmaster@lukasparaemprender.com', 'Luka$ para Emprender');
	 $this->email->to($user_email);
	 $this->email->bcc('lukasparaemprender@gmail.com');

	 $this->email->subject('Ingresado correctamente');
	 $this->email->message($message);

	 $send = $this->email->send();

	 if (!$send){
			$this->email->print_debugger();
	 }
	}



	public function email_test(){
		error_reporting(0);

 		 $user_name = 'Gustavo';
 		 $user_surname = 'Quilodran';
 		 $user_rut = '17513256-2';
 		 $user_email = 'gaqs.02@gmail.com';
 		 $pass = 'ASDASD0201';

			$this->sendcorrectmail($user_name, $user_surname, $user_rut, $user_email, $pass);
	}


}//end CI_Controller
