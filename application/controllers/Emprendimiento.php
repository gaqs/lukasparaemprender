/*##################
  ### DEPRECATED ###
  ##################*/

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Emprendimiento extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

   public function index()
   {
     $this->load->view('header');
     $this->load->view('emprendimiento');
     $this->load->view('footer');
   }

   public function insert(){
 		date_default_timezone_set("Chile/Continental");

 		//Informacion basica
 		$user_rut = $this->input->post('user_rut');
    $user_email = $this->input->post('user_email');
 		//verificar si rut existe en la base de datos
 		$query = $this->db->get_where('usuarios', array('rut'=>$user_rut, 'email'=>$user_email ,'flag'=>1));
 		$result = $query->result();

 		if( empty($result) ){

 			$user_name 					= ucwords($this->input->post('user_name'));
 			$user_surname 			= ucwords($this->input->post('user_surname'));
 			$user_job 					= $this->input->post('user_job');
 			$user_address 			= $this->input->post('user_address');
 			$user_phone 				= $this->input->post('user_phone');
 			//informacion de emprendimiento
 			$user_proyect_name 				= $this->input->post('user_proyect_name');

 			$bussiness_explication 		= $this->input->post('bussiness_explication');
 			$bussiness_diferences 		= $this->input->post('bussiness_diferences');
 			$bussiness_finances 			= $this->input->post('bussiness_finances');
      $bussiness_clients 			  = $this->input->post('bussiness_clients');
 			$bussiness_status 				= $this->input->post('bussiness_status');
 			$bussiness_complementary	= $this->input->post('bussiness_complementary');
 			$bussiness_team 					= $this->input->post('bussiness_team');

      //cedula de identidad
      $file_path = "./uploaded_files/" . $user_rut .'/';
      $path_complementario = $file_path.'Complementario/';

      if (!file_exists($file_path)) { mkdir($file_path, 0777, true); }
      if (!file_exists($path_complementario)){ mkdir($path_complementario, 0777, true); }

      //archivos complementarios
      $path_complemento1 = '';
      $path_complemento2 = '';
      $complemento1 = $_FILES['complemento1'];
      $complemento2 = $_FILES['complemento2'];

      if( !empty($complemento1['tmp_name']) ){
        $path_complemento1 = fileManager($path_complementario, 'comp01_', $complemento1);
      }
      if( !empty($complemento2['tmp_name']) ){
        $path_complemento2 = fileManager($path_complementario, 'comp02_', $complemento2);
      }

      $cedula = $_FILES['cedula'];
      $residencia = $_FILES['residencia'];
      $path_cedula = fileManager($file_path, 'cedula', $cedula);
      $path_residencia = fileManager($file_path, 'residencia', $residencia);

 			$pass = password_generate(6);
 			$encrypted_pass = md5($pass);
 			$encrypted_pass = hash('sha256',$encrypted_pass);
 			// Guardado en base de datos
 			$data = array(
        'tipo_usuario'=> 0,
 				'rut'	 				=> $user_rut,
 				'nombres'			=> $user_name,
 				'apellidos' 	=> $user_surname,
 				'ocupacion' 	=> $user_job,
 				'direccion' 	=> $user_address,
 				'telefono' 		=> $user_phone,
 				'email' 			=> $user_email,
 				'contrasena' 	=> $encrypted_pass, //crear password
 				'insert_date' => date('y-m-d H:i:s'),
 				'edit_date' 	=> date('y-m-d H:i:s')
 				);

 				$this->db->insert('usuarios', $data);
 				$user_id = $this->db->insert_id();

 				$datab = array(
          'id_usuario' 			=> $user_id,
 					'nombre' 					=> $user_proyect_name,
 					'resumen' 				=> $bussiness_explication,
 					'equipo' 					=> $bussiness_team,
 					'relevancia'	 		=> $bussiness_diferences,
 					'financiamiento' 	=> $bussiness_finances,
          'clientes'        => $bussiness_clients,
 					'estado' 					=> $bussiness_status,
 					'complemento' 		=> $bussiness_complementary,
          'path_complemento1' => $path_complemento1,
          'path_complemento2' => $path_complemento2,
 					'path_cedula' 		=> $path_cedula,
 					'path_residencia' => $path_residencia,
 					'insert_date' 		=> date('y-m-d H:i:s'),
 					'edit_date' 			=> date('y-m-d H:i:s')
 				);

 				$this->db->insert('encuesta_emprendimiento', $datab);

        //clonacion del insert en otra dabatase clone
        $this->db->db_select('lukaspar_lukasparaemprender_clone');
        $this->db->insert('usuarios', $data);
        $this->db->insert('encuesta_emprendimiento', $datab);



 				//Si todo sale correcto, envia correo y va a ventana de exito.
        $this->sendcorrectmail($user_name, $user_surname, $user_rut, $user_proyect_name, $user_email, $pass);
 				header('Location: '.base_url().'home/exito?exito=01'); //ingresado correctamente
 		}//end if verify
 		else{
 			header('Location: '.base_url().'home/error?error=01'); //el usuario ya existe
 		}

 	}//end public function insert

 	//Actualizar servidor de correos.
 	public function sendcorrectmail($user_name, $user_surname, $user_rut, $user_proyect_name, $user_email, $pass){
    $config = Array(
        'protocol'  => 'smtp',
        'smtp_host' => 'mail.lukasparaemprender.com',
        'smtp_port' => '587',
        'smtp_user' => 'postmaster@lukasparaemprender.com',
        'smtp_pass' => 'g3@N1Phrd=JL',
        'mailtype'  => 'html',
        'charset'   => 'iso-8859-1',
        'newline'	  => "\r\n"
    );
    $this->load->library('email', $config);

    $data['info'] = array(
      'name' => $user_name,
      'surname' => $user_surname,
      'rut' => $user_rut,
      'email' => $user_email,
      'pass' => $pass
    );
    $message = utf8_decode($this->load->view('email/email',$data, TRUE));

    $this->email->from('postmaster@lukasparaemprender.com', 'Luka$ para Emprender');
    $this->email->to($user_email);
    $this->email->bcc('lukasparaemprender@gmail.com');

    $this->email->subject('Ingresado correctamente');
    $this->email->message($message);

    $send = $this->email->send();

    if (!$send){
       $this->email->print_debugger();
    }


 	}




} // end CI_Controller
?>
