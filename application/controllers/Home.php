<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function terminate(){
		date_default_timezone_set("Chile/Continental");
		$now = time();
		$finish = 1557359999;

		if( $now == $finish ){
			$this->load->view('finish');
		}
	}

	public function index()
	{
		$this->load->view('header');
		$this->terminate();
		$this->load->view('index');
		$this->load->view('footer');
	}
	public function login()
	{
		$this->load->view('header');
		$this->load->view('login');
		$this->load->view('footer');
	}
	public function selection(){
		$selection = $this->input->post('select_type');
		if( $selection == 1 ){
				header('Location: '.base_url().'emprendimiento');
		}else if( $selection == 2){
				header('Location: '.base_url().'empresa');
		}else{
				header('Location: '.base_url().'home/error');
		}
	}
	public function contacto()
	{
		$this->load->view('header');
		$this->load->view('contacto');
		$this->load->view('footer');
	}


	public function exito()
	{
		$this->load->view('header');
		$this->load->view('exito');
		$this->load->view('footer');
	}
	public function error()
	{
		$this->load->view('header');
		$nerror = $this->input->post('error');
		$this->load->view('error', $nerror);
		$this->load->view('footer');
	}

	public function email_consulta(){
		$name = $this->input->post('name');
		$phone = $this->input->post('phone');
		$email = $this->input->post('email');
		$subject = $this->input->post('subject');
		$message = $this->input->post('message');

		$config = Array(
				'protocol'  => 'smtp',
				'smtp_host' => 'mail.lukasparaemprender.com',
				'smtp_port' => '587',
				'smtp_user' => 'postmaster@lukasparaemprender.com',
				'smtp_pass' => 'g3@N1Phrd=JL',
				'mailtype'  => 'html',
				'charset'   => 'iso-8859-1',
				'newline'	  => "\r\n"
		);
		$this->load->library('email', $config);

		$mensaje = '<b>Nombre:&nbsp;</b>'.$name.'<br><b>Teléfono:&nbsp;</b>'.$phone.'<br><b>Correo:&nbsp;</b>'.$email.'<br><b>Asunto:&nbsp;</b>'.$subject.'<br><b>Mensaje:&nbsp;</b>'.$message.'.';
		$mensaje = utf8_decode($mensaje);

		$this->email->from('postmaster@lukasparaemprender.com', 'Consulta WEB Lukas para Emprender');
		$this->email->to('lukasparaemprender@puertomontt.cl');

		$this->email->subject($subject);
		$this->email->message($mensaje);

		$this->email->send();
		header('Location: '.base_url().'home/exito?exito=05'); //ingresado correctamente

	}


}//end CI_Controller
