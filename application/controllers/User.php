<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

   public function index(){
     $this->load->view('header');
     $type_user = $this->session->userdata('tipo_usuario');
     $user_id = $this->session->userdata('user_id');
     if( isset($type_user) ){

       if( $type_user == 0 ){
         $data['user'] = $this->querys->selectUserById($user_id);
         $data['user'][0]['id'] = $user_id;
         $this->load->view('user/emprendimiento',$data);


       }else if( $type_user == 1 ){
         $data['user'] = $this->querys->selectUserByIdEmpresa($user_id);
         $data['user'][0]['id'] = $user_id;
         $this->load->view('user/empresa',$data);
       }

     }else{
       header('Location: '.base_url().'login/user');
     }
    $this->load->view('user/footer');
    $this->load->view('footer');


   }

   public function innovacion(){
    $this->load->view('header');
    $group_id = $this->session->userdata('inn_idgrupo');

    //$data['inn_user'] = $this->querys->selectUserByGroup($group_id);
    /* Separar usuarios por rol en distintos arreglos */
    if( isset($group_id)){
      for ($r=1; $r < 6; $r++) {
        $data['inn_user'.$r] = $this->querys->selectUserByGroupAndRol($group_id, $r);
      }
      $data['inn_all'] = $this->querys->selectAllByGroup($group_id);
      $this->load->view('user/innovacion',$data);

      $this->load->view('innovacion/footer');
    }else{
      header('Location: '.base_url().'innovacion');
    }


   }

   public function edit_emprendimiento(){
     date_default_timezone_set("Chile/Continental");
     //Informacion basica
     $user_id           = $this->input->post('user_id');
     $user_rut          = $this->input->post('user_rut');
     $user_name 				= ucwords($this->input->post('user_name'));
     $user_surname 			= ucwords($this->input->post('user_surname'));
     $user_job 					= $this->input->post('user_job');
     $user_address 			= $this->input->post('user_address');
     $user_phone 				= $this->input->post('user_phone');
     $user_email 				= $this->input->post('user_email');
     //informacion de emprendimiento
     $user_proyect_name 			= $this->input->post('user_proyect_name');

     $bussiness_explication 	= $this->input->post('bussiness_explication');
     $bussiness_diferences 		= $this->input->post('bussiness_diferences');
     $bussiness_finances 			= $this->input->post('bussiness_finances');
     $bussiness_clients 			= $this->input->post('bussiness_clients');
     $bussiness_status 				= $this->input->post('bussiness_status');
     $bussiness_complementary	= $this->input->post('bussiness_complementary');
     $bussiness_team 					= $this->input->post('bussiness_team');

     $path_cedula             = $this->input->post('path_cedula');
     $path_residencia         = $this->input->post('path_residencia');

     $path_complemento1       = $this->input->post('path_complemento1');
     $path_complemento2       = $this->input->post('path_complemento2');


     //cedula de identidad
     $file_path = "./uploaded_files/" . $user_rut .'/';
      if (!file_exists($file_path)) { mkdir($file_path, 0777, true); }

      //manejo de archivos
      if( isset($_FILES['cedula']['tmp_name']) ){
        $file_cedula = $_FILES['cedula'];
        if( $path_cedula != '' ){ $path_cedula = EditfileManager($path_cedula, 'cedula', $file_cedula); }
        else{ $path_cedula = fileManager($file_path, 'cedula', $file_cedula); }
      }

      if( isset($_FILES['residencia']['tmp_name']) ){
        $file_residencia = $_FILES['residencia'];
        if( $path_residencia != '' ){ $path_residencia = EditfileManager($path_residencia, 'residencia', $file_residencia); }
        else{ $path_residencia = fileManager($file_path, 'residencia', $file_residencia); }
      }


      //archivos complementarios
      if( isset($_FILES['complemento1']['tmp_name']) ){
        $file_complemento1 = $_FILES['complemento1'];
        if( $path_complemento1 != '' ){ $path_complemento1 = EditfileManager($path_complemento1, 'comp01_', $file_complemento1); }
        else{ $path_complemento1 = fileManager($file_path, 'comp01_', $file_complemento1); }
      }
      if( isset($_FILES['complemento2']['tmp_name']) ){
        $file_complemento2 = $_FILES['complemento2'];
        if( $path_complemento2 != '' ){ $path_complemento2 = EditfileManager($path_complemento2, 'comp02_', $file_complemento2); }
        else{ $path_complemento2 = fileManager($file_path, 'comp02_', $file_complemento2); }
      }


     $data = array(
       'rut'	 				=> $user_rut,
       'val_rut'  => 1,
       'nombres'			=> $user_name,
       'val_nombres' => 1,
       'apellidos' 	=> $user_surname,
       'val_apellidos' => 1,
       'ocupacion' 	=> $user_job,
       'val_ocupacion' => 1,
       'direccion' 	=> $user_address,
       'val_direccion' => 1,
       'telefono' 		=> $user_phone,
       'val_telefono' => 1,
       'email' 			=> $user_email,
       'val_email' => 1,
       'edit_date' 	=> date('y-m-d H:i:s')
       );

       $this->db->where('id', $user_id);
       $this->db->update('usuarios', $data);

       $datab = array(
         'nombre' 					=> $user_proyect_name,
         'val_nombre'     => 1,
         'resumen' 				=> $bussiness_explication,
         'val_resumen'  => 1,
         'equipo' 					=> $bussiness_team,
         'val_equipo' => 1,
         'relevancia'	 		=> $bussiness_diferences,
         'val_relevancia' => 1,
         'financiamiento' 	=> $bussiness_finances,
         'val_financiamiento' => 1,
         'clientes' 	=> $bussiness_clients,
         'val_clientes' => 1,
         'estado' 					=> $bussiness_status,
         'val_estado' => 1,
         'complemento' 		=> $bussiness_complementary,
         'val_complemento' => 1,
         'path_complemento1' 		=> $path_complemento1,
         'val_path_complemento1' => 1,
         'path_complemento2' => $path_complemento2,
         'val_path_complemento2' => 1,
         'path_cedula' 		=> $path_cedula,
         'val_path_cedula' => 1,
         'path_residencia' => $path_residencia,
         'val_path_residencia' => 1,
         'edit_date' 			=> date('y-m-d H:i:s')
       );

       $this->db->where('id_usuario', $user_id);
       $this->db->update('encuesta_emprendimiento', $datab);

       //Si todo sale correcto, ventana de exito.
       header('Location: '.base_url().'home/exito?exito=04');

   }//end public function edit

   public function delete_file(){
     $path = $this->input->post('path_file');
     unlink($path);
     header('Location: '.base_url().'login/user#documentacion ');

   }

   public function edit_empresa(){
     date_default_timezone_set("Chile/Continental");

     $user_id 					  = $this->input->post('user_id');
     $user_rut 					= $this->input->post('user_rut');
     $user_name 					= ucwords($this->input->post('user_name'));
     $user_surname 			= ucwords($this->input->post('user_surname'));
     $user_job 					= $this->input->post('user_job');
     $user_business 			= $this->input->post('user_business');
     $user_position      = $this->input->post('user_position');
     $user_phone 				= $this->input->post('user_phone');
     $user_email 				= $this->input->post('user_email');
     //empresa
     $business_rut     = $this->input->post('business_rut');
     $business_name    = $this->input->post('business_name');
     $business_date    = $this->input->post('business_date');
     $business_address = $this->input->post('business_address');
     $business_phone   = $this->input->post('business_phone');
     $business_web     = $this->input->post('business_web');

     //informacion de empresa
     $user_proyect_name 				= $this->input->post('user_proyect_name');

     $bussiness_explication 		= $this->input->post('bussiness_explication');
     $bussiness_team 					= $this->input->post('bussiness_team');
     $bussiness_finances 			= $this->input->post('bussiness_finances');
     $bussiness_clients 			  = $this->input->post('bussiness_clients');
     $bussiness_commercialization = $this->input->post('bussiness_commercialization');
     $bussiness_competition 		= $this->input->post('bussiness_competition');
     $bussiness_projection	    = $this->input->post('bussiness_projection');
     $bussiness_jobs           = $this->input->post('bussiness_jobs');
     $bussiness_complementary	= $this->input->post('bussiness_complementary');

     $path_complemento1       = $this->input->post('path_complemento1');
     $path_complemento2       = $this->input->post('path_complemento2');

      $path_cedula         = $this->input->post('path_cedula');
      $path_rol            = $this->input->post('path_rol');
      $path_renta          = $this->input->post('path_renta');
      $path_patente        = $this->input->post('path_patente');
      $path_otrospermisos  = $this->input->post('path_otrospermisos');
      $path_boleta         = $this->input->post('path_boleta');

      //cedula de identidad
      $file_path = "./uploaded_files/" . $user_rut .'/';
       if (!file_exists($file_path)) { mkdir($file_path, 0777, true); }

       //manejo de archivos
       if( isset($_FILES['cedula']['tmp_name']) ){
         $file_cedula = $_FILES['cedula'];
         if( $path_cedula != '' ){ $path_cedula = EditfileManager($path_cedula, 'cedula', $file_cedula); }
         else{ $path_cedula = fileManager($file_path, 'cedula', $file_cedula); }
       }
       if( isset($_FILES['rol']['tmp_name']) ){
         $file_rol = $_FILES['rol'];
         if( $path_residencia != '' ){ $path_rol = EditfileManager($path_rol, 'rol', $file_rol); }
         else{ $path_rol = fileManager($file_path, 'rol', $file_rol); }
       }
       if( isset($_FILES['renta']['tmp_name']) ){
         $file_renta = $_FILES['renta'];
         if( $path_renta != '' ){ $path_renta = EditfileManager($path_renta, 'renta', $file_renta); }
         else{ $path_renta = fileManager($file_path, 'residencia', $file_renta); }
       }
       if( isset($_FILES['patente']['tmp_name']) ){
         $file_patente = $_FILES['patente'];
         if( $path_patente != '' ){ $path_patente = EditfileManager($path_patente, 'patente', $file_patente); }
         else{ $path_patente = fileManager($file_path, 'residencia', $file_patente); }
       }
       if( isset($_FILES['otrospermisos']['tmp_name']) ){
         $file_otrospermisos = $_FILES['otrospermisos'];
         if( $path_otrospermisos != '' ){ $path_otrospermisos = EditfileManager($path_otrospermisos, 'otrospermisos', $file_otrospermisos); }
         else{ $path_otrospermisos = fileManager($file_path, 'otrospermisos', $file_otrospermisos); }
       }
       if( isset($_FILES['boleta']['tmp_name']) ){
         $file_boleta = $_FILES['boleta'];
         if( $path_boleta != '' ){ $path_boleta = EditfileManager($path_boleta, 'residencia', $file_boleta); }
         else{ $path_boleta = fileManager($file_path, 'boleta', $file_boleta); }
       }


       //archivos complementarios
       if( isset($_FILES['complemento1']['tmp_name']) ){
         $file_complemento1 = $_FILES['complemento1'];
         if( $path_complemento1 != '' ){ $path_complemento1 = EditfileManager($path_complemento1, 'comp01_', $file_complemento1); }
         else{ $path_complemento1 = fileManager($file_path, 'comp01_', $file_complemento1); }
       }
       if( isset($_FILES['complemento2']['tmp_name']) ){
         $file_complemento2 = $_FILES['complemento2'];
         if( $path_complemento2 != '' ){ $path_complemento2 = EditfileManager($path_complemento2, 'comp02_', $file_complemento2); }
         else{ $path_complemento2 = fileManager($file_path, 'comp02_', $file_complemento2); }
       }


     $data = array(
       'rut'	 				=> $user_rut,
       'val_rut' => 1,
       'nombres'			=> $user_name,
       'val_nombres' => 1,
       'apellidos' 	  => $user_surname,
       'val_apellidos' => 1,
       'ocupacion' 	  => $user_job,
       'val_ocupacion' => 1,
       'telefono' 		=> $user_phone,
       'val_telefono' => 1,
       'email' 			=> $user_email,
       'val_email' => 1,
       'empresa' 		=> $user_business,
       'val_empresa' => 1,
       'cargo' 			=> $user_position,
       'val_cargo' => 1,
       'edit_date' 	=> date('y-m-d H:i:s')
       );

       $this->db->where('id', $user_id);
       $this->db->update('usuarios', $data);

       $datab = array(
         'rut_empresa'      => $business_rut,
         'val_rut_empresa' => 1,
         'razon_social'     => $business_name,
         'val_razon_social' => 1,
         'inicio'           => $business_date,
         'val_inicio' => 1,
         'direccion_empresa'=> $business_address,
         'val_direccion_empresa' => 1,
         'telefono_empresa' => $business_phone,
         'val_telefono_empresa' => 1,
         'web'              => $business_web,
         'val_web' => 1,
         'edit_date' 	      => date('y-m-d H:i:s')
       );

       $this->db->where('id_usuario', $user_id);
       $this->db->update('empresa', $datab);

       $datac = array(
         'nombre' 					   => $user_proyect_name,
         'val_nombre'          => 1,
         'explicacion' 		     => $bussiness_explication,
         'val_explicacion'     => 1,
         'equipo' 					   => $bussiness_team,
         'val_equipo'          => 1,
         'financiamiento' 	   => $bussiness_finances,
         'val_financiamiento'  => 1,
         'clientes'        => $bussiness_clients,
         'val_clientes' => 1,
         'comercializacion'=> $bussiness_commercialization,
         'val_comercializacion' => 1,
         'competencia' 		=> $bussiness_competition,
         'val_competencia' => 1,
         'proyecciones' 		=> $bussiness_projection,
         'val_proyecciones' => 1,
         'trabajos'        => $bussiness_jobs,
         'val_trabajos'   => 1,
         'complemento'     => $bussiness_complementary,
         'val_complemento' => 1,
         'path_complemento1' 		=> $path_complemento1,
         'val_path_complemento1' => 1,
         'path_complemento2' => $path_complemento2,
         'val_path_complemento2' => 1,
         'path_cedula' => $path_cedula,
         'val_path_cedula' => 1,
         'path_rol' => $path_rol,
         'val_path_rol' => 1,
         'path_renta' => $path_renta,
         'val_path_renta' => 1,
         'path_patente' => $path_patente,
         'val_path_patente' => 1,
         'path_otrospermisos' => $path_otrospermisos,
         'val_path_otrospermisos' => 1,
         'path_boleta' => $path_boleta,
         'val_path_boleta' => 1,
         'edit_date' 			=> date('y-m-d H:i:s')
       );

       $this->db->where('id_usuario', $user_id);
       $this->db->update('encuesta_empresa', $datac);

       header('Location: '.base_url().'home/exito?exito=04');
     }


     public function edit_innovacion(){
   		date_default_timezone_set("Chile/Continental");
   		/*   Mentor   */
      $men_user_id					= $this->input->post('men_user_id');
   		$men_user_rut					= $this->input->post('men_user_rut');
   		$men_user_name				= $this->input->post('men_user_name');
   		$men_user_surname			= $this->input->post('men_user_surname');
   		$men_user_phone				= $this->input->post('men_user_phone');
   		$men_user_email				= $this->input->post('men_user_email');
   		$men_user_description = $this->input->post('men_user_description');

   		/*   Solucion   */
      $solution_challenge   = $this->input->post('solution_challenge');
      $solution_deepen      = $this->input->post('solution_deepen');
      $solution_recipient   = $this->input->post('solution_recipient');

   		$solution_proposal		= $this->input->post('solution_proposal');
   		$solution_community		= $this->input->post('solution_community');
   		$solution_prototipe		= $this->input->post('solution_prototipe');
   		$solution_susten			= $this->input->post('solution_susten');
   		$solution_test				= $this->input->post('solution_test');
      $solution_budget      = $this->input->post('solution_budget');

   		/*  Archivos   */
   		$path_patrocinio 			 = $this->input->post('patrocinio');
   		$path_mentor_academico = $this->input->post('mentor_academico');
   		$path_reunion 				 = $this->input->post('reunion');
   		$path_instrumentos 	 	 = $this->input->post('instrumentos');

      // actualizacion de path si archivo no existe, que es el mismo que se permitio editar por administrador.
      if( !file_exists($path_patrocinio) ){$path_patrocinio = EditfileManager($path_patrocinio, 'patrocinio', $_FILES['patrocinio']); }
      if( !file_exists($path_mentor_academico) ){$path_mentor_academico = EditfileManager($path_mentor_academico, 'mentor_academico', $_FILES['mentor_academico']); }
      if( !file_exists($path_reunion) ){$path_reunion = EditfileManager($path_reunion, 'reunion', $_FILES['reunion']); }
      if( !file_exists($path_instrumentos) ){$path_instrumentos = EditfileManager($path_instrumentos, 'instrumentos', $_FILES['instrumentos']); }


   		$data1 = array(
   			'rut'					=> $men_user_rut,
   			'nombres'			=> $men_user_name,
   			'apellidos'		=> $men_user_surname,
   			'telefono'		=> $men_user_phone,
   			'email'				=> $men_user_email,
   			'descripcion' => $men_user_description,
   			'edit_date' 	=> date('y-m-d H:i:s')
   		);

      $this->db->where('id_mentor', $men_user_id);
      $this->db->update('inn_mentor', $data1);

   		$data2 = array(
        'desafio'               => $solution_challenge,
        'profundizar'           => $solution_deepen,
        'beneficiarios'         => $solution_recipient,
   			'propuesta_solucion' 	  => $solution_proposal,
   			'comunidad_implicada'   => $solution_community,
   			'prototipo' 					  => $solution_prototipe,
   			'sustentabilidad' 		  => $solution_susten,
   			'testear' 						  => $solution_test,
        'presupuesto'           => $solution_budget,
   			'path_patrocinio'				=> $path_patrocinio,
   			'path_mentor_academico'	=> $path_mentor_academico,
   			'path_reunion'					=> $path_reunion,
   			'path_instrumentos'			=> $path_instrumentos,
   			'edit_date' 					  => date('y-m-d H:i:s')
   		);


      $this->db->where('id_grupo', $men_user_id);
      $this->db->update('inn_encuesta', $data2);

   		for ($i=1; $i < 6; $i++) {

        if( $i == 4 && $this->input->post('user_rut4') == '' ){ continue; }
  			else if( $i == 5 && $this->input->post('user_rut5') == '' ){ continue; }

        /*   Integrantes   */
        $user_id					  = $this->input->post('user_id'.$i);
        $user_rut 					= $this->input->post('user_rut'.$i);
        $user_name 					= $this->input->post('user_name'.$i);
        $user_surname 			= $this->input->post('user_surname'.$i);
        $user_address 			= $this->input->post('user_address'.$i);
        $user_phone 				= $this->input->post('user_phone'.$i);
        $user_email					=	$this->input->post('user_email'.$i);
        $user_rol						= $this->input->post('user_rol'.$i);
        $user_description		= $this->input->post('user_description'.$i);
        /*  Archivos  */
        $path_alumno 				  = $this->input->post('alumno_regular'.$i);
        $path_cedula 				  = $this->input->post('cedula_identidad'.$i);

        if( !file_exists($path_alumno) ){$path_alumno = EditfileManager($path_alumno, 'alumno', $_FILES['alumno_regular'.$i]); }
        if( !file_exists($path_cedula) ){$path_cedula = EditfileManager($path_cedula, 'cedula', $_FILES['cedula_identidad'.$i]); }

   			$data3 = array(
   				'rut'	 				   => $user_rut,
   				'nombres'		  	 => $user_name,
   				'apellidos' 	   => $user_surname,
   				'direccion'  	   => $user_address,
   				'telefono' 		   => $user_phone,
   				'email' 			   => $user_email,
   				'rol'					   => $user_rol,
   				'descripcion'	   => $user_description,
   				'path_alumno'    => $path_alumno,
   				'path_cedula'	   => $path_cedula,
   				'edit_date' 	   => date('y-m-d H:i:s')
   			);

        $this->db->where('id', $user_id);
        $this->db->update('inn_usuarios', $data3);


   		}
   		//Si todo sale correcto, envia correo y va a ventana de exito.
   		header('Location: '.base_url().'home/exito?exito=08'); //ingresado correctamente

   	}




   }//End CI_Controller
   ?>
