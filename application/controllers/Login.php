<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

   public function index(){
 		$this->load->view('admin/header');
 		$name = $this->session->userdata('name');

 		if( isset($name) ){
 			header('Location: '.base_url().'admin');
 		}else{
 			$this->load->view('admin/login');
 		}
 		$this->load->view('admin/footer');
 	}

 	public function access(){
    $this->load->helper('cookie');
 		$this->load->view('admin/header');

 		$aemail = $this->input->post('admin_email');
    /*
 		$pass = md5($this->input->post('pass'));
 		$pass = hash('sha256',$pass);
    */
    $apass = $this->input->post('admin_pass');
    $aremember = $this->input->post('admin_remember');

    //cookie remember user and pass
 if( $aremember ){
    $this->input->set_cookie('aemail', $aemail, 86500);
    $this->input->set_cookie('apassword', $apass, 86500);
    $this->input->set_cookie('aremember', $aremember, 86500);
 }else{
    delete_cookie('aemail');
    delete_cookie('apassword');
    delete_cookie('aremember');
 }

 		$result = $this->querys->checkAdmin($aemail,$apass);

 		if( count($result) != 0 ){

 			$datauser = array(
 						'name' => $result[0]['nombres'],
 						'surname' => $result[0]['apellidos'],
 						'email' => $result[0]['correo'],
            'contrasena' => $result[0]['contrasena']
 					);

 			$this->session->set_userdata($datauser);
 			header('Location: '.base_url().'admin');

 		}else{
 		   header('Location: '.base_url().'home/error?error=05');
 		}
 		$this->load->view('admin/footer');

 	}

  public function destroy(){
    $items = array('name','surname','email','contrasena');
    $this->session->unset_userdata($items);

    header('Location: '.base_url().'login');
  }



//Login normal normal user
public function user(){
 $this->load->view('header');
 $name = $this->session->userdata('user_name');

 if( isset($name) ){
   header('Location: '.base_url().'user');
 }else{
   $this->load->view('login');
 }
 $this->load->view('footer');
}




/*LOGIN USERS*/
public function user_access(){
  $this->load->helper('cookie');
 $this->load->view('header');

 $email = $this->input->post('email');
 $pass = $this->input->post('pass');
 $select = $this->input->post('login_select');
 $remember = $this->input->post('remember');

 if( $remember ){
    $this->input->set_cookie('uemail', $email, 86500);
    $this->input->set_cookie('upassword', $pass, 86500);
    $this->input->set_cookie('uremember', $remember, 86500);
 }else{
    delete_cookie('uemail');
    delete_cookie('upassword');
    delete_cookie('uremember');
 }


 if( $select == 'concurso' ){

   $cy_pass = encrypted($pass);
   $result = $this->querys->checkUser($email,$cy_pass);

   if( count($result) != 0 ){

     $query = $this->db->get_where('hash_validation', array('id_user'=>$result[0]['id']));
     $val = $query->result();

     if( $val[0]->validado == 0 ){
       header('Location: '.base_url().'home/error?error=06'); //correo aun no validado

    }else{

      $datauser = array(
            'user_id' => $result[0]['id'],
            'user_name' => $result[0]['nombres'],
            'user_surname' => $result[0]['apellidos'],
            'user_email' => $result[0]['correo'],
            'tipo_usuario' => $result[0]['tipo_usuario']
          );

      $this->session->set_userdata($datauser);
      header('Location: '.base_url().'user');

    }

  }else{
      header('Location: '.base_url().'home/error?error=04');
  }

 }else if( $select == 'innovacion' ){

   $cy_pass = md5($pass);
   $cy_pass = hash('sha256',$cy_pass);

   $result = $this->querys->checkInnUser($email,$cy_pass);

   if( count($result) != 0 ){

     $inn_datauser = array(
           'inn_id' => $result[0]['id'],
           'inn_name' => $result[0]['nombres'],
           'inn_surname' => $result[0]['apellidos'],
           'inn_email' => $result[0]['email'],
           'inn_idgrupo' => $result[0]['id_grupo']
         );

     $this->session->set_userdata($inn_datauser);
     header('Location: '.base_url().'user/innovacion');

   }else{
     header('Location: '.base_url().'home/error?error=04');
   }

 }


}

  /* LOGOUT */
  public function user_destroy(){

    $items = array('user_id','user_name','user_surname','user_email','tipo_usuario');
    $this->session->unset_userdata($items);

    header('Location: '.base_url().'home');
  }

  public function inn_destroy(){
    $items = array('inn_id','inn_name','inn_surname','inn_email','inn_idgrupo');
    $this->session->unset_userdata($items);

    header('Location: '.base_url().'innovacion');
  }
  /*--------*/



  //Login normal innovacion user
  public function innovacion(){
    $this->load->view('header');
    $name = $this->session->userdata('inn_name');

    if( isset($name) ){
      header('Location: '.base_url().'user/innovacion');
    }else{
      $this->load->view('innovacion/login');
    }
    $this->load->view('innovacion/footer');
  }





}// end CI_Controller
?>
