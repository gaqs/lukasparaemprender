<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Export extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function header(){
  		$name = $this->session->userdata('name');

  		if( isset($name ) ){
  			$info['name'] = $name;
  			$info['surname'] = $this->session->userdata('surname');
  			$info['email'] = $this->session->userdata('email');

  			$this->load->view('admin/header',$info);
  		}else{
  			header('Location: '.base_url().'login');
  		}
  	}


	public function export_user(){
		// Include the main TCPDF library (search for installation path).
		require_once('TCPDF/tcpdf.php');

		// create new PDF document
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

		$id = $this->input->get('id');
		$data['user'] = $this->querys->selectUserById($id);

		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Subdirección de Desarrollo Local<br>Dirección de Desarrollo Comunitario');
		$pdf->SetTitle('Formulario'.$data['user'][0]['nombres']);
		$pdf->SetSubject('');
		$pdf->SetKeywords('');

		// set default header data
		$pdf->SetHeaderData('../image/escudopuertomontt.png', 20, 'Formulario Inscripción '.$data['user'][0]['nombres'].' '.$data['user'][0]['apellidos'], "Subdirección de Desarrollo Económico local\nDirección de Desarrollo Comunitario", array(0,64,255), array(0,64,128));
		$pdf->setFooterData(array(0,64,0), array(0,64,128));

		// set header and footer fonts
		$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		// set margins
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

		// set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		// set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		// set some language-dependent strings (optional)
		if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
			require_once(dirname(__FILE__).'/lang/eng.php');
			$pdf->setLanguageArray($l);
		}

		// ---------------------------------------------------------

		// set default font subsetting mode
		$pdf->setFontSubsetting(true);

		$pdf->SetFont('dejavusans', '', 9, '', true);

		$pdf->AddPage();

		$html = $this->load->view('export/emprendimiento', $data, true);

		// Print text using writeHTMLCell()
		$pdf->writeHTML($html, true, false, false, false, '');

		// ---------------------------------------------------------

		// Close and output PDF document
		// This method has several options, check the source code documentation for more information.
		$pdf->Output('Informe_001.pdf', 'I');

	}


	public function export_empresa(){
		// Include the main TCPDF library (search for installation path).
		require_once('TCPDF/tcpdf.php');

		// create new PDF document
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

		$id = $this->input->get('id');
		$data['user'] = $this->querys->selectUserByIdEmpresa($id);

		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Subdirección de Desarrollo Local<br>Dirección de Desarrollo Comunitario');
		$pdf->SetTitle('Formulario'.$data['user'][0]['nombres']);
		$pdf->SetSubject('');
		$pdf->SetKeywords('');

		// set default header data
		$pdf->SetHeaderData('../image/escudopuertomontt.png', 20, 'Formulario Inscripción '.$data['user'][0]['nombres'].' '.$data['user'][0]['apellidos'], "Subdirección de Desarrollo Económico local\nDirección de Desarrollo Comunitario", array(0,64,255), array(0,64,128));
		$pdf->setFooterData(array(0,64,0), array(0,64,128));

		// set header and footer fonts
		$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		// set margins
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

		// set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		// set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		// set some language-dependent strings (optional)
		if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
			require_once(dirname(__FILE__).'/lang/eng.php');
			$pdf->setLanguageArray($l);
		}

		// ---------------------------------------------------------

		// set default font subsetting mode
		$pdf->setFontSubsetting(true);

		$pdf->SetFont('dejavusans', '', 9, '', true);

		$pdf->AddPage();


		$html = $this->load->view('export/empresa', $data, true);


		// Print text using writeHTMLCell()
		$pdf->writeHTML($html, true, false, false, false, '');

		// ---------------------------------------------------------

		// Close and output PDF document
		// This method has several options, check the source code documentation for more information.
		$pdf->Output('Informe_001.pdf', 'I');

	}

	public function notas_emprendimiento(){
		$this->header();
		$data['user'] = $this->querys->selectAllUsers();
		$data['notas'] = $this->querys->selectAllNotas();
		$this->load->view('export/notas_emprendimiento',$data);
		$this->load->view('admin/footer');

	}

	public function notas_empresa(){
		$this->header();
		$data['user'] = $this->querys->selectAllEmpresa();
		$data['notas'] = $this->querys->selectAllNotas();
		$this->load->view('export/notas_empresa',$data);
		$this->load->view('admin/footer');
	}

	public function export_notas_emprendimiento(){
		// Include the main TCPDF library (search for installation path).
		require_once('TCPDF/tcpdf.php');

		// create new PDF document
		$pdf = new TCPDF('L', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Gustavo Quilodrán');
		$pdf->SetTitle('Notas Lukas para Emprender');
		$pdf->SetSubject('');
		$pdf->SetKeywords('');
		// remove default header/footer
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);
		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		// set margins
		$pdf->SetMargins(30, PDF_MARGIN_TOP, 10);
		// set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		// set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
		// set some language-dependent strings (optional)
		if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
			require_once(dirname(__FILE__).'/lang/eng.php');
			$pdf->setLanguageArray($l);
		}
		// ---------------------------------------------------------

		$pdf->SetDisplayMode('fullpage', 'SinglePage', 'UseNone');

		// set default font subsetting mode
		$pdf->setFontSubsetting(true);

		$pdf->SetFont('dejavusans', '', 18, '', true);

		$pdf->AddPage('L','A0');

		$data['user'] = $this->querys->selectAllUsers();
		$data['notas'] = $this->querys->selectAllNotas();
		$html = $this->load->view('export/notas_emprendimiento', $data, TRUE);

		// Print text using writeHTMLCell()
		$pdf->writeHTML($html, true, false, false, false, '');

		// ---------------------------------------------------------

		// Close and output PDF document
		// This method has several options, check the source code documentation for more information.
		$pdf->Output('Notas_001.pdf', 'I');

	}

	public function export_notas_empresa(){
		// Include the main TCPDF library (search for installation path).
		require_once('TCPDF/tcpdf.php');

		// create new PDF document
		$pdf = new TCPDF('L', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Gustavo Quilodrán');
		$pdf->SetTitle('Notas Lukas para Emprender');
		$pdf->SetSubject('');
		$pdf->SetKeywords('');
		// remove default header/footer
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);
		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		// set margins
		$pdf->SetMargins(30, PDF_MARGIN_TOP, 10);
		// set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		// set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
		// set some language-dependent strings (optional)
		if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
			require_once(dirname(__FILE__).'/lang/eng.php');
			$pdf->setLanguageArray($l);
		}
		// ---------------------------------------------------------

		$pdf->SetDisplayMode('fullpage', 'SinglePage', 'UseNone');

		// set default font subsetting mode
		$pdf->setFontSubsetting(true);

		$pdf->SetFont('dejavusans', '', 18, '', true);

		$pdf->AddPage('L','A0');

		$data['user'] = $this->querys->selectAllEmpresa();
		$data['notas'] = $this->querys->selectAllNotas();
		$html = $this->load->view('export/notas_empresa', $data, TRUE);

		// Print text using writeHTMLCell()
		$pdf->writeHTML($html, true, false, false, false, '');

		// ---------------------------------------------------------

		// Close and output PDF document
		// This method has several options, check the source code documentation for more information.
		$pdf->Output('Notas_001.pdf', 'I');

	}

}
