###################
LuKa$ para Emprender
###################

Es  un  financiamiento  no  reembolsable,  que  se  adjudica  al  postulante  por  medio  de  un  proceso concursable para iniciar nuevos negocios y/o fortalecer los actuales. Por  lo  tanto,  no  se  trata  de  un  crédito,  ya  que  el  dinero  entregado  a  los(as)  ganadores  no  debe  ser devuelto, pero sí, debe ser rendido íntegramente. Dicho  financiamiento  está  destinado  a  fortalecer  diferentes  ámbitos  de  gestión  de  la  microempresa,  así como  al  ingreso  de  nuevos  mercados  y/o  consolidación  en  los  actuales  mercados,  que  presenten  reales oportunidades de negocios a microempresas de la Comuna de Puerto Montt.

*******************
Frameworks / Librerias utilizados
*******************
Bootstrap 4.1 with JS bs-custom-file-input.js
Codeigniter 3.1.10

***************
Actualizaciones
***************

Facilitar el ingreso de informacion a usuarios que deseen participar en el concurso.

*******************
Server Requirements
*******************

PHP version 5.6 o mas actualizado es recomendado.
Ciertas funciones utilizadas en la web no funcionan con versiones mas antigual de PHP.

************
Responsables
************

Gustavo Quilodran Sanhueza
Programador - Coordinador de Tecnologias
Telecentros Comunitarios
Municipalidad de Puerto Montt.

Subdirección  de  Desarrollo Económico Local
Municipalidad de Puerto Montt.
